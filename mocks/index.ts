/* eslint-disable @typescript-eslint/no-var-requires */

if (typeof window === 'undefined') {
  const { server } = require('./servers');
  server.listen();
} else {
  const { worker } = require('./browser');
  worker.start();
}

export {};
