import { PengmasFilterValues } from '@/components/container/Kegiatan/Pengmas';
import resultsPage1 from '@/constants/mock-response/pengmas/getPengmas1.json';
import resultsPage2 from '@/constants/mock-response/pengmas/getPengmas2.json';
import detailPengmas from '@/constants/mock-response/pengmas/getDetailPengmas.json';

const mockPengmas = {
  getAll: (params: PengmasFilterValues) => {
    const { search, page, lab, status_usulan, tipe_pendanaan, tahun } = params;
    let response: ListResponse<IHibah> = {
      next: page === 2 ? null : page + 1,
      previous: page === 1 ? null : page - 1,
      count: 15,
      results: page == 1 ? resultsPage1 : resultsPage2,
    };

    if (search) {
      const searchFilter = response.results.filter(
        (item) =>
          item.judul_penelitian.toLowerCase().includes(search.toLowerCase()) ||
          item.peneliti_utama.includes(search.toLowerCase()),
      );

      response = {
        ...response,
        results: searchFilter,
        count: searchFilter.length,
      };
    }

    if (lab) {
      const searchFilter = response.results.filter(
        (item) => item.lab?.name.toLowerCase().replaceAll(' ', '+') === lab,
      );

      response = {
        ...response,
        results: searchFilter,
        count: searchFilter.length,
      };
    }

    if (status_usulan) {
      const searchFilter = response.results.filter((item) => item.status_usulan === status_usulan);

      response = {
        ...response,
        results: searchFilter,
        count: searchFilter.length,
      };
    }

    if (tipe_pendanaan) {
      const searchFilter = response.results.filter(
        (item) => item.pemberi_dana?.tipe_pendanaan === tipe_pendanaan,
      );

      response = {
        ...response,
        results: searchFilter,
        count: searchFilter.length,
      };
    }

    if (tahun) {
      const searchFilter = response.results.filter((item) => tahun.includes(item.tahun.toString()));

      response = {
        ...response,
        results: searchFilter,
        count: searchFilter.length,
      };
    }

    return response;
  },

  getYears: () => {
    const allPengmas = [...resultsPage1, ...resultsPage2];
    const listYearSets = new Set(allPengmas.map((item) => item.tahun));
    return Array.from(listYearSets);
  },
  getLabs: () => {
    const allPengmas = [...resultsPage1, ...resultsPage2];
    const listLabsSets = new Set(allPengmas.map((item) => item.pemberi_dana?.nama));
    return Array.from(listLabsSets);
  },
  getDetail: () => {
    return detailPengmas;
  },
};

export default mockPengmas;
