import { PatenFilterValues } from '@/components/container/Produk/Paten';
import resultsPage1 from '@/constants/mock-response/paten/getPatenPage1.json';
import resultsPage2 from '@/constants/mock-response/paten/getPatenPage2.json';
import detailPaten from '@/constants/mock-response/hki/getDetailHki.json';

const mockPaten = {
  getAll: (params: PatenFilterValues) => {
    const { search, page, jenis_paten, keterangan, tahun } = params;
    let response: ListResponse<IPaten> = {
      next: page === 2 ? null : page + 1,
      previous: page === 1 ? null : page - 1,
      count: 15,
      results: page == 1 ? resultsPage1 : resultsPage2,
    };

    if (search) {
      const searchFilter = response.results.filter(
        (item) =>
          item.judul_ciptaan.toLowerCase().includes(search.toLowerCase()) ||
          item.pencipta.includes(search.toLowerCase()),
      );

      response = {
        ...response,
        results: searchFilter,
        count: searchFilter.length,
      };
    }

    if (jenis_paten) {
      const searchFilter = response.results.filter(
        (item) => item.jenis_paten.toLowerCase().replaceAll(' ', '+') === jenis_paten,
      );

      response = {
        ...response,
        results: searchFilter,
        count: searchFilter.length,
      };
    }

    if (keterangan) {
      const searchFilter = response.results.filter((item) => item.keterangan === keterangan);

      response = {
        ...response,
        results: searchFilter,
        count: searchFilter.length,
      };
    }

    if (tahun) {
      const searchFilter = response.results.filter((item) => tahun.includes(item.tahun));

      response = {
        ...response,
        results: searchFilter,
        count: searchFilter.length,
      };
    }

    return response;
  },

  getYears: () => {
    const allPatens = [...resultsPage1, ...resultsPage2];
    const listYearSets = new Set(allPatens.map((item) => item.tahun));
    return Array.from(listYearSets);
  },
  getDetail: () => {
    return detailPaten;
  },
};

export default mockPaten;
