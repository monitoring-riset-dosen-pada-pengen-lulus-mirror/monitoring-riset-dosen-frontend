import infoHibahPage1 from '@/constants/mock-response/infoHibah/getInfoHibah.json';
import infoHibahPage2 from '@/constants/mock-response/infoHibah/getInfoHibahPage2.json';

export interface GetInfoHibahParams {
  page: number;
  page_size: number;
  search: string;
  ordering: string;
}

const mockInfoHibah = {
  getAll: (params: GetInfoHibahParams) => {
    const { ordering, page_size, search, page } = params;
    let response = {
      next: page === 2 ? null : page + 1,
      previous: page === 1 ? null : page - 1,
      count: 20,
      results: page == 1 ? infoHibahPage1 : infoHibahPage2,
    };

    if (search) {
      const searchFilter = response.results.filter((item) => item.judul.toLowerCase().includes(search));

      response = {
        ...response,
        results: searchFilter,
        count: searchFilter.length,
      };
    }
    if (ordering) {
      let orderingFilter = response.results;
      if (ordering === 'tanggal_ubah') {
        orderingFilter = response.results.reverse();
      }
      response = {
        ...response,
        results: orderingFilter,
        count: orderingFilter.length,
      };
    }
    if (page_size) {
      const pageSizeFilter = response.results.slice(0, page_size);
      response = {
        ...response,
        results: pageSizeFilter,
        count: pageSizeFilter.length,
      };
    }
    return response;
  },
};

export default mockInfoHibah;
