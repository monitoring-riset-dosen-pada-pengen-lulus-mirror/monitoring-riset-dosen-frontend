import dashboardData from '@/constants/mock-response/dashboard/dashboard.json';

const mockDashboard = {
  getAll: () => {
    return {
      isError: false,
      isLoading: true,
      result: dashboardData,
    };
  },
};

export default mockDashboard;
