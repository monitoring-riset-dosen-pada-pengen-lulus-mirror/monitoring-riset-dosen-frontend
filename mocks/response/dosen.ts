import daftarDosenPage1 from '@/constants/mock-response/dosen/getDaftarDosen1.json';
import daftarDosenPage2 from '@/constants/mock-response/dosen/getDaftarDosen2.json';
import detailDosen from '@/constants/mock-response/dosen/getDetailDosen.json';
import listRiset from '@/constants/mock-response/riset/getRiset1.json';
import listPengmas from '@/constants/mock-response/pengmas/getPengmas1.json';
import listHki from '@/constants/mock-response/hki/getHKIPage1.json';
import listPaten from '@/constants/mock-response/paten/getPatenPage1.json';
export interface GetDaftarDosenParams {
  page: number;
  page_size: number;
  search: string;
}

const mockDaftarDosen = {
  getAll: (params: GetDaftarDosenParams) => {
    const { page_size, search, page } = params;
    let response = {
      next: page === 2 ? null : page + 1,
      previous: page === 1 ? null : page - 1,
      count: 20,
      results: page == 1 ? daftarDosenPage1 : daftarDosenPage2,
    };

    if (search) {
      const searchFilter = response.results.filter((item) =>
        item.nama_lengkap.toLowerCase().includes(search),
      );

      response = {
        ...response,
        results: searchFilter,
        count: searchFilter.length,
      };
    }
    if (page_size) {
      const pageSizeFilter = response.results.slice(0, page_size);
      response = {
        ...response,
        results: pageSizeFilter,
        count: pageSizeFilter.length,
      };
    }
    return response;
  },
  getDetail: () => {
    return detailDosen;
  },
  getRisetList: () => {
    return {
      next: null,
      previous: null,
      count: listRiset.length,
      results: listRiset,
    };
  },
  getPengmasList: () => {
    return {
      next: null,
      previous: null,
      count: listPengmas.length,
      results: listPengmas,
    };
  },
  getHkiList: () => {
    return {
      next: null,
      previous: null,
      count: listHki.length,
      results: listHki,
    };
  },
  getPatenList: () => {
    return {
      next: null,
      previous: null,
      count: listPaten.length,
      results: listPaten,
    };
  },
};

export default mockDaftarDosen;
