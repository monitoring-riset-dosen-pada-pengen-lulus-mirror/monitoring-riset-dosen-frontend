import { HibahFilterValues } from '@/components/container/Kegiatan/Hibah';
import resultsPage1 from '@/constants/mock-response/hibah/getHibah1.json';
import resultsPage2 from '@/constants/mock-response/hibah/getHibah2.json';
import detailHibah from '@/constants/mock-response/hibah/getDetailHibah.json';

const mockHibah = {
  getAll: (params: HibahFilterValues) => {
    const { search, page, lab, status_usulan, tipe_pendanaan, tahun } = params;
    let response: ListResponse<IHibah> = {
      next: page === 2 ? null : page + 1,
      previous: page === 1 ? null : page - 1,
      count: 15,
      results: page == 1 ? resultsPage1 : resultsPage2,
    };

    if (search) {
      const searchFilter = response.results.filter(
        (item) =>
          item.judul_penelitian.toLowerCase().includes(search.toLowerCase()) ||
          item.peneliti_utama.includes(search.toLowerCase()),
      );

      response = {
        ...response,
        results: searchFilter,
        count: searchFilter.length,
      };
    }

    if (lab) {
      const searchFilter = response.results.filter(
        (item) => item.lab?.name.toLowerCase().replaceAll(' ', '+') === lab,
      );

      response = {
        ...response,
        results: searchFilter,
        count: searchFilter.length,
      };
    }

    if (status_usulan) {
      const searchFilter = response.results.filter((item) => item.status_usulan === status_usulan);

      response = {
        ...response,
        results: searchFilter,
        count: searchFilter.length,
      };
    }

    if (tipe_pendanaan) {
      const searchFilter = response.results.filter(
        (item) => item.pemberi_dana?.tipe_pendanaan === tipe_pendanaan,
      );

      response = {
        ...response,
        results: searchFilter,
        count: searchFilter.length,
      };
    }

    if (tahun) {
      const searchFilter = response.results.filter((item) => tahun.includes(item.tahun.toString()));

      response = {
        ...response,
        results: searchFilter,
        count: searchFilter.length,
      };
    }

    return response;
  },
  getYears: () => {
    const allHibah = [...resultsPage1, ...resultsPage2];
    const listYearSets = new Set(allHibah.map((item) => item.tahun));
    return Array.from(listYearSets);
  },
  getLabs: () => {
    const allHibah = [...resultsPage1, ...resultsPage2];
    const listLabsSets = new Set(allHibah.map((item) => item.pemberi_dana?.nama));
    return Array.from(listLabsSets);
  },
  getDetail: () => {
    return detailHibah;
  },
};

export default mockHibah;
