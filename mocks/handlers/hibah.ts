import paths from '@/api/paths';
import { BASE_API } from '@/api/services';
import { HibahFilterValues } from '@/components/container/Kegiatan/Hibah';
import { getAllUrlParams } from '@/utils/api/url';
import { rest } from 'msw';
import mockHibah from '../response/hibah';

export const getListHibah = rest.get(`${BASE_API}${paths.listKegiatan('hibah')}`, (req, res, ctx) => {
  const params = getAllUrlParams(req.url.toString());

  return res(ctx.json(mockHibah.getAll(params as HibahFilterValues)));
});

export const getListHibahError = rest.get(`${BASE_API}${paths.listKegiatan('hibah')}`, (req, res, ctx) => {
  return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
});

export const getHibahYears = rest.get(`${BASE_API}${paths.listOpsiTahun('hibah')}`, (req, res, ctx) => {
  return res(ctx.json(mockHibah.getYears()));
});

export const getHibahYearsError = rest.get(
  `${BASE_API}${paths.listOpsiTahun('hibah')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
  },
);

export const getHibahLabs = rest.get(`${BASE_API}${paths.listLaboratorium}`, (req, res, ctx) => {
  return res(ctx.json(mockHibah.getLabs()));
});

export const getHibahLabsError = rest.get(`${BASE_API}${paths.listLaboratorium}`, (req, res, ctx) => {
  return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
});

export const getHibahDetail = rest.get(
  `${BASE_API}${paths.detailKegiatan('hibah', '1')}`,
  (req, res, ctx) => {
    return res(ctx.json(mockHibah.getDetail()));
  },
);

export const getHibahDetailError = rest.get(
  `${BASE_API}${paths.detailKegiatan('hibah', '1')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
  },
);
