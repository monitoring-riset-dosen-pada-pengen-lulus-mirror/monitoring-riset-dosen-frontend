import paths from '@/api/paths';
import { BASE_API } from '@/api/services';
import { HKIFilterValues } from '@/components/container/Produk/HKI';
import { getAllUrlParams } from '@/utils/api/url';
import { rest } from 'msw';
import mockHKI from '../response/hki';

export const getHKI = rest.get(`${BASE_API}${paths.listProduk('hki')}`, (req, res, ctx) => {
  const params = getAllUrlParams(req.url.toString());

  return res(ctx.json(mockHKI.getAll(params as HKIFilterValues)));
});

export const getHKIError = rest.get(`${BASE_API}${paths.listProduk('hki')}`, (req, res, ctx) => {
  return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
});

export const getHKIYears = rest.get(`${BASE_API}${paths.listOpsiTahun('hki')}`, (req, res, ctx) => {
  return res(ctx.json(mockHKI.getYears()));
});

export const getHKIYearsError = rest.get(`${BASE_API}${paths.listOpsiTahun('hki')}`, (req, res, ctx) => {
  return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
});

export const getHKIDetail = rest.get(`${BASE_API}${paths.detailProduk('hki', '1')}`, (req, res, ctx) => {
  return res(ctx.json(mockHKI.getDetail()));
});

export const getHKIDetailError = rest.get(
  `${BASE_API}${paths.detailProduk('hki', '1')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
  },
);

export const deleteHki = rest.delete(`${BASE_API}${paths.detailProduk('hki', '1')}`, (req, res, ctx) => {
  return res.once(ctx.status(200), ctx.json({ message: 'success delete' }));
});
export const deleteHkiError = rest.delete(
  `${BASE_API}${paths.detailProduk('hki', '1')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
  },
);
