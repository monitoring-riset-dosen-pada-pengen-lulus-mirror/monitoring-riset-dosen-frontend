import paths from '@/api/paths';
import { BASE_API } from '@/api/services';
import { PatenFilterValues } from '@/components/container/Produk/Paten';
import { getAllUrlParams } from '@/utils/api/url';
import { rest } from 'msw';
import mockPaten from '../response/paten';

export const getPaten = rest.get(`${BASE_API}${paths.listProduk('paten')}`, (req, res, ctx) => {
  const params = getAllUrlParams(req.url.toString());

  return res(ctx.json(mockPaten.getAll(params as PatenFilterValues)));
});

export const getPatenError = rest.get(`${BASE_API}${paths.listProduk('paten')}`, (req, res, ctx) => {
  return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
});

export const getPatenYears = rest.get(`${BASE_API}${paths.listOpsiTahun('paten')}`, (req, res, ctx) => {
  return res(ctx.json(mockPaten.getYears()));
});

export const getPatenYearsError = rest.get(
  `${BASE_API}${paths.listOpsiTahun('paten')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
  },
);

export const getPatenDetail = rest.get(
  `${BASE_API}${paths.detailProduk('paten', '1')}`,
  (req, res, ctx) => {
    return res(ctx.json(mockPaten.getDetail()));
  },
);

export const getPatenDetailError = rest.get(
  `${BASE_API}${paths.detailProduk('paten', '1')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
  },
);

export const deletePaten = rest.delete(
  `${BASE_API}${paths.detailProduk('paten', '1')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(200), ctx.json({ message: 'success delete' }));
  },
);
export const deletePatenError = rest.delete(
  `${BASE_API}${paths.detailProduk('paten', '1')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
  },
);
