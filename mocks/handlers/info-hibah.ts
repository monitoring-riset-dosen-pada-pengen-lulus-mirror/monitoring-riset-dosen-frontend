import paths from '@/api/paths';
import { BASE_API } from '@/api/services';
import { getAllUrlParams } from '@/utils/api/url';
import { rest } from 'msw';
import mockInfoHibah, { GetInfoHibahParams } from '../response/info-hibah';

export const getInfoHibah = rest.get(`${BASE_API}${paths.listInfoHibah}`, (req, res, ctx) => {
  const params = getAllUrlParams(req.url.toString());

  return res(ctx.delay(50), ctx.json(mockInfoHibah.getAll(params as GetInfoHibahParams)));
});

export const getInfoHibahError = rest.get(`${BASE_API}${paths.listInfoHibah}`, (req, res, ctx) => {
  return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
});
