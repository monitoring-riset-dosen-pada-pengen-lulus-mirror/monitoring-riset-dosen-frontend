import { deleteHki, getHKI, getHKIDetail, getHKIYears } from './hki';
import { getInfoHibah } from './info-hibah';

import {
  getListPengmas,
  getPengmasLabs,
  getPengmasYears,
  getPengmasDetail,
  deletePengmas,
} from './pengmas';
import { deletePaten, getPaten, getPatenDetail, getPatenYears } from './paten';
import { getListRiset, getRisetLabs, getRisetYears, getRisetDetail, deleteRiset } from './riset';
import { getHibahLabs, getHibahYears, getListHibah, getHibahDetail } from './hibah';
import {
  getDaftarDosen,
  getDetailDosen,
  getHkiDosen,
  getPengmasDosen,
  getRisetDosen,
  getPatenDosen,
} from '@/mocks/handlers/dosen';
import { getDashboard } from '@/mocks/handlers/dashboard';

export const handlers = [
  getPatenDetail,
  getHKIDetail,
  getPaten,
  getInfoHibah,
  getPatenYears,
  getHKI,
  getHKIYears,
  getListPengmas,
  getPengmasYears,
  getPengmasLabs,
  getListRiset,
  getRisetYears,
  getRisetLabs,
  getHibahLabs,
  getHibahYears,
  getListHibah,
  getHibahDetail,
  getPengmasDetail,
  getDaftarDosen,
  getRisetDetail,
  deletePengmas,
  getDetailDosen,
  getHkiDosen,
  getPengmasDosen,
  getRisetDosen,
  getPatenDosen,
  deletePaten,
  deleteHki,
  getDashboard,
  deleteRiset,
];
