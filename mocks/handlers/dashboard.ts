import { BASE_API } from '@/api/services';
import paths from '@/api/paths';
import { rest } from 'msw';
import mockDashboard from '@/mocks/response/dashboard';

export const getDashboard = rest.get(`${BASE_API}${paths.listDashboard}`, (req, res, ctx) => {
  return res(ctx.json(mockDashboard.getAll()));
});

export const getDashboardError = rest.get(`${BASE_API}${paths.listDashboard}`, (req, res, ctx) => {
  return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
});
