import paths from '@/api/paths';
import { BASE_API } from '@/api/services';
import { PengmasFilterValues } from '@/components/container/Kegiatan/Pengmas';
import { getAllUrlParams } from '@/utils/api/url';
import { rest } from 'msw';
import mockPengmas from '../response/pengmas';

export const getListPengmas = rest.get(
  `${BASE_API}${paths.listKegiatan('pengabdian-masyarakat')}`,
  (req, res, ctx) => {
    const params = getAllUrlParams(req.url.toString());

    return res(ctx.json(mockPengmas.getAll(params as PengmasFilterValues)));
  },
);

export const getListPengmasError = rest.get(
  `${BASE_API}${paths.listKegiatan('pengabdian-masyarakat')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
  },
);

export const getPengmasYears = rest.get(`${BASE_API}${paths.listOpsiTahun('hibah')}`, (req, res, ctx) => {
  return res(ctx.json(mockPengmas.getYears()));
});

export const getPengmasYearsError = rest.get(
  `${BASE_API}${paths.listOpsiTahun('hibah')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
  },
);

export const getPengmasLabs = rest.get(`${BASE_API}${paths.listLaboratorium}`, (req, res, ctx) => {
  return res(ctx.json(mockPengmas.getLabs()));
});

export const getPengmasLabsError = rest.get(`${BASE_API}${paths.listLaboratorium}`, (req, res, ctx) => {
  return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
});

export const getPengmasDetail = rest.get(
  `${BASE_API}${paths.detailKegiatan('pengabdian-masyarakat', '1')}`,
  (req, res, ctx) => {
    return res(ctx.json(mockPengmas.getDetail()));
  },
);

export const getPengmasDetailError = rest.get(
  `${BASE_API}${paths.detailKegiatan('pengabdian-masyarakat', '1')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
  },
);

export const deletePengmas = rest.delete(
  `${BASE_API}${paths.detailKegiatan('pengabdian-masyarakat', '1')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(200), ctx.json({ message: 'success delete' }));
  },
);

export const deletePengmasError = rest.delete(
  `${BASE_API}${paths.detailKegiatan('pengabdian-masyarakat', '1')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
  },
);
