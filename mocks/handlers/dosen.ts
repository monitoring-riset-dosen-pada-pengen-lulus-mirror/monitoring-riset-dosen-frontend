import { rest } from 'msw';
import { BASE_API } from '@/api/services';
import { getAllUrlParams } from '@/utils/api/url';
import mockDaftarDosen, { GetDaftarDosenParams } from '@/mocks/response/dosen';
import paths from '@/api/paths';

export const getDaftarDosen = rest.get(`${BASE_API}${paths.listPembuat}`, (req, res, ctx) => {
  const params = getAllUrlParams(req.url.toString());

  return res(ctx.delay(50), ctx.json(mockDaftarDosen.getAll(params as GetDaftarDosenParams)));
});

export const getDaftarDosenError = rest.get(`${BASE_API}${paths.listPembuat}`, (req, res, ctx) => {
  return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
});

export const getDetailDosen = rest.get(`${BASE_API}${paths.detailDosen('1')}`, (req, res, ctx) => {
  return res(ctx.delay(50), ctx.json(mockDaftarDosen.getDetail()));
});

export const getDetailDosenError = rest.get(`${BASE_API}${paths.detailDosen('1')}`, (req, res, ctx) => {
  return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
});

export const getRisetDosen = rest.get(`${BASE_API}${paths.detailDosenRiset('1')}`, (req, res, ctx) => {
  return res(ctx.delay(50), ctx.json(mockDaftarDosen.getRisetList()));
});

export const getRisetDosenError = rest.get(`${BASE_API}${paths.detailDosenRiset('1')}`, (req, res, ctx) => {
  return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
});

export const getPengmasDosen = rest.get(`${BASE_API}${paths.detailDosenPengmas('1')}`, (req, res, ctx) => {
  return res(ctx.delay(50), ctx.json(mockDaftarDosen.getPengmasList()));
});

export const getPengmasDosenError = rest.get(
  `${BASE_API}${paths.detailDosenPengmas('1')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
  },
);

export const getHkiDosen = rest.get(`${BASE_API}${paths.detailDosenHki('1')}`, (req, res, ctx) => {
  return res(ctx.delay(50), ctx.json(mockDaftarDosen.getHkiList()));
});

export const getHkiDosenError = rest.get(`${BASE_API}${paths.detailDosenHki('1')}`, (req, res, ctx) => {
  return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
});

export const getPatenDosen = rest.get(`${BASE_API}${paths.detailDosenPaten('1')}`, (req, res, ctx) => {
  return res(ctx.delay(50), ctx.json(mockDaftarDosen.getPatenList()));
});

export const getPatenDosenError = rest.get(`${BASE_API}${paths.detailDosenPaten('1')}`, (req, res, ctx) => {
  return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
});
