import paths from '@/api/paths';
import { BASE_API } from '@/api/services';
import { RisetFilterValues } from '@/components/container/Kegiatan/Riset';
import { getAllUrlParams } from '@/utils/api/url';
import { rest } from 'msw';
import mockRiset from '../response/riset';

export const getListRiset = rest.get(`${BASE_API}${paths.listKegiatan('riset')}`, (req, res, ctx) => {
  const params = getAllUrlParams(req.url.toString());

  return res(ctx.json(mockRiset.getAll(params as RisetFilterValues)));
});

export const getListRisetError = rest.get(`${BASE_API}${paths.listKegiatan('riset')}`, (req, res, ctx) => {
  return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
});

export const getRisetYears = rest.get(`${BASE_API}${paths.listOpsiTahun('hibah')}`, (req, res, ctx) => {
  return res(ctx.json(mockRiset.getYears()));
});

export const getRisetYearsError = rest.get(
  `${BASE_API}${paths.listOpsiTahun('hibah')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
  },
);

export const getRisetLabs = rest.get(`${BASE_API}${paths.listLaboratorium}`, (req, res, ctx) => {
  return res(ctx.json(mockRiset.getLabs()));
});

export const getRisetLabsError = rest.get(`${BASE_API}${paths.listLaboratorium}`, (req, res, ctx) => {
  return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
});

export const getRisetDetail = rest.get(
  `${BASE_API}${paths.detailKegiatan('riset', '1')}`,
  (req, res, ctx) => {
    return res(ctx.json(mockRiset.getDetail()));
  },
);

export const getRisetDetailError = rest.get(
  `${BASE_API}${paths.detailKegiatan('riset', '1')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
  },
);

export const deleteRiset = rest.delete(
  `${BASE_API}${paths.detailKegiatan('riset', '1')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(200), ctx.json({ message: 'success delete' }));
  },
);

export const deleteRisetError = rest.delete(
  `${BASE_API}${paths.detailKegiatan('riset', '1')}`,
  (req, res, ctx) => {
    return res.once(ctx.status(500), ctx.json({ message: 'server error' }));
  },
);
