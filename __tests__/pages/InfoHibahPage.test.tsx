import { screen } from '@testing-library/react';
import { act } from 'react-test-renderer';
import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import InfoHibahPage from '@/pages/info-hibah';
import { act as actUtils } from 'react-dom/test-utils';

jest.mock('next/dist/client/router', () => require('next-router-mock'));

describe('Info Hibah Page', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });

    act(() => {
      renderWithTheme(<InfoHibahPage />);
    });
  });

  it('renders a Website Title', async () => {
    await actUtils(() => sleep(50));

    const title = screen.queryAllByText('Informasi Hibah');
    screen.getByRole('heading', { name: 'Informasi Hibah' });

    expect(title).toBeVisible;
  });
});
