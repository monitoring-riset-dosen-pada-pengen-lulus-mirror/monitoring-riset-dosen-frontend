import NotFound from '@/pages/404';
import { screen } from '@testing-library/react';
import { renderWithTheme } from '@/utils/test-utils/wrapper';

describe('404 Page', () => {
  it('renders a 404 Page', () => {
    renderWithTheme(<NotFound />);

    const notFoundText = screen.getByText('Halaman Tidak Ditemukan');

    expect(notFoundText).toBeVisible;
  });
});
