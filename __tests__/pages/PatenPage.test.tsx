import { screen } from '@testing-library/react';
import { act } from 'react-test-renderer';
import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import PatenPage from '@/pages/produk/paten';
import { act as actUtils } from 'react-dom/test-utils';

jest.mock('next/dist/client/router', () => require('next-router-mock'));

describe('Paten Page', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });

    // to fully reset the state between tests, clear the storage
    localStorage.clear();
    // and reset all mocks
    jest.clearAllMocks();

    localStorage.setItem('token', 'dummytoken');
    act(() => {
      renderWithTheme(<PatenPage />);
    });
  });

  it('renders a Header', async () => {
    await actUtils(() => sleep(50));

    const title = screen.getByRole('heading', { name: 'Daftar Paten' });

    expect(title).toBeVisible;
  });
});
