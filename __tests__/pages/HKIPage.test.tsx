//

import { screen } from '@testing-library/react';
import { act } from 'react-test-renderer';
import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import HKIPage from '@/pages/produk/hki';
import { act as actUtils } from 'react-dom/test-utils';

jest.mock('next/dist/client/router', () => require('next-router-mock'));

describe('HKI Page', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });

    // to fully reset the state between tests, clear the storage
    localStorage.clear();
    // and reset all mocks
    jest.clearAllMocks();

    localStorage.setItem('token', 'dummytoken');
    act(() => {
      renderWithTheme(<HKIPage />);
    });
  });

  it('renders a Header', async () => {
    await actUtils(() => sleep(50));

    const title = screen.getByRole('heading', { name: 'Daftar HKI' });

    expect(title).toBeVisible;
  });
});
