import DetailPengmas from '@/pages/kegiatan/pengabdian-masyarakat/[id]';
import { screen } from '@testing-library/react';
import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import mockRouter from 'next-router-mock';
import 'next-router-mock/dynamic-routes';
import { act } from 'react-test-renderer';
import { act as actUtils } from 'react-dom/test-utils';

jest.mock('next/dist/client/router', () => require('next-router-mock'));
mockRouter.registerPaths(['/produk/hki/[id]']);
describe('Pengmas Detail', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });
    mockRouter.setCurrentUrl('/produk/hki/1');
    act(() => {
      renderWithTheme(<DetailPengmas />);
    });
  });

  it('renders a back button', async () => {
    await actUtils(() => sleep(100));

    expect(screen.getByTestId('back-btn')).toBeVisible();
    expect(screen.getByTestId('back-btn')).toHaveTextContent('Kembali');
  });

  it('renders a detail pengmas text', async () => {
    await actUtils(() => sleep(100));

    expect(screen.getByTestId('detail-pengmas-text')).toBeVisible();
    expect(screen.getByTestId('detail-pengmas-text')).toHaveTextContent('Kegiatan Pengabdian Masyarakat');
  });
});
