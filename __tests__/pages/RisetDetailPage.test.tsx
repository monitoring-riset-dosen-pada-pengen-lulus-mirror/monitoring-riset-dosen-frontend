import DetailRiset from '@/pages/kegiatan/riset/[id]';
import { screen } from '@testing-library/react';
import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import mockRouter from 'next-router-mock';
import 'next-router-mock/dynamic-routes';
import { act } from 'react-test-renderer';
import { act as actUtils } from 'react-dom/test-utils';

jest.mock('next/dist/client/router', () => require('next-router-mock'));
mockRouter.registerPaths(['/kegiatan/riset/[id]']);
describe('Riset Detail', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });
    mockRouter.setCurrentUrl('/kegiatan/riset/1');
    act(() => {
      renderWithTheme(<DetailRiset />);
    });
  });

  it('renders a back button', async () => {
    await actUtils(() => sleep(100));

    expect(screen.getByTestId('back-btn')).toBeVisible();
    expect(screen.getByTestId('back-btn')).toHaveTextContent('Kembali');
  });

  it('renders a detail riset text', async () => {
    await actUtils(() => sleep(100));

    expect(screen.getByTestId('detail-riset-text')).toBeVisible();
    expect(screen.getByTestId('detail-riset-text')).toHaveTextContent('Kegiatan Riset');
  });
});
