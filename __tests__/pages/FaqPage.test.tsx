import React from 'react';
import { screen } from '@testing-library/react';
import { renderWithTheme } from '@/utils/test-utils/wrapper';
import FaqPage from '@/pages/faq';

describe('FAQ Page', () => {
  beforeEach(() => {
    renderWithTheme(<FaqPage />);
  });

  it('renders FAQ page', () => {
    const faqText = screen.queryByText('Pertanyaan yang Sering Ditanyakan Seputar');

    expect(faqText).toBeVisible;
  });
});
