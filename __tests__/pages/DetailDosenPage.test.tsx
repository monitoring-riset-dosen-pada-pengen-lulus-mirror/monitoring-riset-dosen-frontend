import DetailDosenPage from '@/pages/dosen/[id]';
import { screen } from '@testing-library/react';
import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import mockRouter from 'next-router-mock';
import 'next-router-mock/dynamic-routes';
import { act } from 'react-test-renderer';
import { act as actUtils } from 'react-dom/test-utils';

jest.mock('next/dist/client/router', () => require('next-router-mock'));
mockRouter.registerPaths(['/dosen/[id]']);
describe('Dosen Detail', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });
    // to fully reset the state between tests, clear the storage
    localStorage.clear();
    // and reset all mocks
    jest.clearAllMocks();

    localStorage.setItem('token', 'dummytoken');

    mockRouter.setCurrentUrl('/dosen/1');
    act(() => {
      renderWithTheme(<DetailDosenPage />);
    });
  });

  it('renders a Overview text', async () => {
    await actUtils(() => sleep(100));
    await actUtils(() => sleep(100));

    expect(screen.getByText('Overview')).toBeVisible();
  });
});
