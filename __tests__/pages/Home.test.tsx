import React from 'react';
import { screen } from '@testing-library/react';
import Home from '@/pages/index';
import { renderWithTheme } from '@/utils/test-utils/wrapper';

describe('Home Page', () => {
  beforeEach(() => {
    renderWithTheme(<Home />);
  });

  it('renders a Header Banner', () => {
    const banner = screen.getByText('Faculty of Computer Science Universitas Indonesia');

    expect(banner).toBeVisible;
  });
});
