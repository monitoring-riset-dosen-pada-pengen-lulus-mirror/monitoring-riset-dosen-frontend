import React from 'react';
import { screen } from '@testing-library/react';
import { renderWithTheme } from '@/utils/test-utils/wrapper';
import DosenPage from '@/pages/dosen';
import { act } from 'react-test-renderer';

jest.mock('next/dist/client/router', () => require('next-router-mock'));

describe('Daftar Dosen Page', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });

    // to fully reset the state between tests, clear the storage
    localStorage.clear();
    // and reset all mocks
    jest.clearAllMocks();

    localStorage.setItem('token', 'dummytoken');
    act(() => {
      renderWithTheme(<DosenPage />);
    });
  });

  it('renders the page header banner', () => {
    const banner = screen.getByText('Daftar Dosen');
    expect(banner).toBeVisible;
  });
});
