import { screen } from '@testing-library/react';
import { renderWithTheme } from '@/utils/test-utils/wrapper';
import RedirectAuth from '@/pages/auth/redirect';
import mockRouter from 'next-router-mock';
import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';
import { mockPayload, mockResponse } from '@/redux/slices/auth/__tests__/thunk.test';

jest.mock('next/dist/client/router', () => require('next-router-mock'));

describe('RedirectAuth Page', () => {
  beforeEach(() => {
    renderWithTheme(<RedirectAuth />);
  });

  it('renders a Loading state', () => {
    const loading = screen.getByText('Loading...');
    expect(loading).toBeVisible;
  });

  it('success process sso login', async () => {
    const mock = new MockAdapter(axios);
    mock.onPost('/account/login', mockPayload).reply(200, mockResponse);

    mockRouter.setCurrentUrl('/auth/redirect/?ticket=dummyticket');
    const SuccessToast = screen.getByText('Login Berhasil');

    expect(SuccessToast).toBeVisible;
  });
});
