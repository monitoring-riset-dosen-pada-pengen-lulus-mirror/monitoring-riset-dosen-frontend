const base = {
  HOST_URL: (): string => {
    if (typeof window !== 'undefined') return window.location.origin;

    return '';
  },
  SSO_UI_URL: 'https://sso.ui.ac.id/cas2',
};

const backend: { [key: string]: string } = {
  prod: 'https://shiba-be-staging.herokuapp.com',
  staging: 'https://shiba-be-staging.herokuapp.com',
  local: 'http://localhost:8000',
  local_test: 'http://localhost:8001',
  preview: 'https://shiba-be-dev.herokuapp.com',
};

const config = {
  ...base,
  BASE_API_URL: backend[process.env.NEXT_PUBLIC_NODE_ENV || 'local'],
};

export default config;
