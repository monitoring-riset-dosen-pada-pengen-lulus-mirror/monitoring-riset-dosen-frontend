import axios from 'axios';
import paths from '@/api/paths';

const dosen = {
  getAll: (params: Record<string, any>) => {
    return () => axios.get(paths.listPembuat, { params });
  },
  getAllOpsi: (query: string) => {
    const params = { search: query };
    return axios.get(paths.listOpsiPembuat, { params });
  },
  getDetail: (id: string) => {
    return () => axios.get(paths.detailDosen(id));
  },
  getRisetList: (id: string, params: Record<string, any>) => {
    return () => axios.get(paths.detailDosenRiset(id), { params });
  },
  getPengmasList: (id: string, params: Record<string, any>) => {
    return () => axios.get(paths.detailDosenPengmas(id), { params });
  },
  getHkiList: (id: string, params: Record<string, any>) => {
    return () => axios.get(paths.detailDosenHki(id), { params });
  },
  getPatenList: (id: string, params: Record<string, any>) => {
    return () => axios.get(paths.detailDosenPaten(id), { params });
  },
};

export default dosen;
