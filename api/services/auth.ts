import { TicketPayload } from '@/components/common/Layout';
import axios from 'axios';
import paths from '../paths';

const loginService = async (payload: TicketPayload) => {
  return axios.post(paths.login, payload);
};

const logoutService = () => {
  window.localStorage.clear();
};

const auth = { loginService, logoutService };
export default auth;
