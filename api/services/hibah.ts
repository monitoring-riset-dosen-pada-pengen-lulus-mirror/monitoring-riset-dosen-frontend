import axios from 'axios';
import paths from '../paths';

const hibah = {
  getAll: (params: Record<string, any>) => {
    return () => axios.get(paths.listKegiatan('hibah'), { params });
  },
  getYears: () => {
    return axios.get(paths.listOpsiTahun('hibah'));
  },
  getLabs: () => {
    return () => axios.get(paths.listLaboratorium);
  },
  getById: (id: string) => {
    return () => axios.get(paths.detailKegiatan('hibah', id));
  },
  postKegiatanExcel: (formData: FormData) => {
    return axios.post(paths.uploadExcelKegiatan, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  },
};

export default hibah;
