import axios from 'axios';
import paths from '@/api/paths';

const topikPenelitian = {
  getAllOpsi: () => {
    return () => axios.get(paths.listOpsiTopikPenelitian);
  },
};

export default topikPenelitian;
