import axios from 'axios';
import paths from '@/api/paths';

const dashboard = {
  getAll: () => {
    return () => axios.get(paths.listDashboard);
  },
};

export default dashboard;
