import axios from 'axios';
import paths from '@/api/paths';

const lab = {
  getAllOpsi: () => {
    return () => axios.get(paths.listOpsiLaboratorium);
  },
};

export default lab;
