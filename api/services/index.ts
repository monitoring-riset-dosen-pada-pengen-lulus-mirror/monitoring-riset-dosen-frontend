import axios from 'axios';
import sso from './sso';
import auth from './auth';
import infoHibah from './infoHibah';
import config from '../config';
import paten from './paten';
import hki from './hki';
import pengmas from './pengmas';
import riset from './riset';
import hibah from './hibah';
import dosen from './dosen';
import topikPenelitian from './topikPenelitian';
import lab from './lab';
import pemberiDana from './pemberiDana';
import api from '@/api/services';
import storage from '@/utils/storage';
import paths from '../paths';
import dashboard from '@/api/services/dashboard';

export const BASE_API = config.BASE_API_URL + '/api';

const services = {
  sso,
  auth,
  infoHibah,
  paten,
  hki,
  pengmas,
  riset,
  hibah,
  dashboard,
  dosen,
  topikPenelitian,
  lab,
  pemberiDana,
};

axios.defaults.baseURL = BASE_API;
axios.interceptors.request.use(
  (axiosConfig: any) => {
    const token = storage.get('token');
    if (token) axiosConfig.headers.Authorization = `Bearer ${token}`;

    return axiosConfig;
  },
  (error) => {
    return Promise.reject(error);
  },
);

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  async function (error) {
    const originalRequest = error.config;

    // if refresh token expired -> logout user and redirect it to login page
    if (error.response?.status === 401 && originalRequest.url === BASE_API + '/token/refresh/') {
      if (typeof window !== 'undefined') {
        api.auth.logoutService();
        window.location.href = `${config.HOST_URL()}/`;
      }
    }

    // if refresh token available -> post to get new refresh-access token
    if (error.response?.status === 403 && !originalRequest._retry && typeof window !== 'undefined') {
      originalRequest._retry = true;
      const refreshToken = localStorage.getItem('refresh');

      try {
        const response = await axios.post(BASE_API + paths.refreshToken, {
          refresh: refreshToken,
        });

        localStorage.setItem('token', response.data.access);

        // @ts-ignore
        axios.defaults.headers['Authorization'] = 'Bearer ' + response.data.access;
        originalRequest.headers['Authorization'] = 'Bearer ' + response.data.access;
        return await axios(originalRequest);
      } catch (err) {
        return Promise.reject(error);
      }
    }

    // specific error handling done elsewhere
    return Promise.reject(error);
  },
);

export default services;
