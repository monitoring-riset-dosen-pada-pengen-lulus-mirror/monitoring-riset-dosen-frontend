import axios from 'axios';
import paths from '../paths';

const infoHibah = {
  getAll: (params: Record<string, string | number>) => {
    return () => axios.get(paths.listInfoHibah, { params });
  },
};

export default infoHibah;
