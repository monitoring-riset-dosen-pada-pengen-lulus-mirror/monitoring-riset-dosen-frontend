import config from '../config';

const { SSO_UI_URL, HOST_URL } = config;
export const SSO_LOGIN_URL = `${SSO_UI_URL}/login?service=${HOST_URL()}/auth/redirect`;
export const SSO_LOGOUT_URL = `${SSO_UI_URL}/logout?url=${HOST_URL()}/`;

const redirectToSSOLogin = () => {
  if (typeof window !== 'undefined') {
    window.location.replace(SSO_LOGIN_URL);
  }
};

const redirectToSSOLogout = () => {
  if (typeof window !== 'undefined') {
    window.location.replace(SSO_LOGOUT_URL);
  }
};

const sso = { redirectToSSOLogin, redirectToSSOLogout };
export default sso;
