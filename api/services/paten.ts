import axios from 'axios';
import paths from '../paths';

const paten = {
  getAll: (params: Record<string, any>) => {
    return () => axios.get(paths.listProduk('paten'), { params });
  },
  getYears: () => {
    return axios.get(paths.listOpsiTahun('paten'));
  },
  getById: (id: string) => {
    return () => axios.get(paths.detailProduk('paten', id));
  },
  deleteById: (id: string) => {
    return axios.delete(paths.detailProduk('paten', id));
  },
  postPatenExcel: (formData: FormData) => {
    return axios.post(paths.uploadExcelProduk, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  },
};

export default paten;
