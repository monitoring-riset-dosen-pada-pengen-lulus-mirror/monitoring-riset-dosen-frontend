import axios from 'axios';
import paths from '../paths';

const riset = {
  getAll: (params: Record<string, any>) => {
    return () => axios.get(paths.listKegiatan('riset'), { params });
  },
  getYears: () => {
    return axios.get(paths.listOpsiTahun('hibah'));
  },
  getLabs: () => {
    return () => axios.get(paths.listLaboratorium);
  },
  getById: (id: string) => {
    return () => axios.get(paths.detailKegiatan('riset', id));
  },
  postRisetHibah: async (payload: HibahPayload) => {
    return await axios.post(paths.tambahRisetHibah, payload);
  },
  postRisetNonHibah: async (payload: KegiatanPayload) => {
    return await axios.post(paths.tambahRisetNonHibah, payload);
  },
  deleteById: (id: string) => {
    return axios.delete(paths.detailKegiatan('riset', id));
  },
};

export default riset;
