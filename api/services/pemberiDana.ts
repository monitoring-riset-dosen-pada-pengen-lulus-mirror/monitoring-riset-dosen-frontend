import axios from 'axios';
import paths from '@/api/paths';

const lab = {
  getAllOpsi: () => {
    return () => axios.get(paths.listOpsiPemberiDana);
  },
};

export default lab;
