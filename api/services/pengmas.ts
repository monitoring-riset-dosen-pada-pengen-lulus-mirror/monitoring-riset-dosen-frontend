import axios from 'axios';
import paths from '../paths';

const pengmas = {
  getAll: (params: Record<string, any>) => {
    return () => axios.get(paths.listKegiatan('pengabdian-masyarakat'), { params });
  },
  getYears: () => {
    return axios.get(paths.listOpsiTahun('hibah'));
  },
  getLabs: () => {
    return () => axios.get(paths.listLaboratorium);
  },
  getById: (id: string) => {
    return () => axios.get(paths.detailKegiatan('pengabdian-masyarakat', id));
  },
  postPengmasHibah: async (payload: HibahPayload) => {
    return await axios.post(paths.tambahPengmasHibah, payload);
  },
  postPengmasNonHibah: async (payload: KegiatanPayload) => {
    return await axios.post(paths.tambahPengmasNonHibah, payload);
  },
  deleteById: (id: string) => {
    return axios.delete(paths.detailKegiatan('pengabdian-masyarakat', id));
  },
};

export default pengmas;
