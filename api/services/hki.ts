import axios from 'axios';
import paths from '../paths';

const hki = {
  getAll: (params: Record<string, any>) => {
    return () => axios.get(paths.listProduk('hki'), { params });
  },
  getYears: () => {
    return axios.get(paths.listOpsiTahun('hki'));
  },
  getById: (id: string) => {
    return () => axios.get(paths.detailProduk('hki', id));
  },
  deleteById: (id: string) => {
    return axios.delete(paths.detailProduk('hki', id));
  },
  postHkiExcel: (formData: FormData) => {
    return axios.post(paths.uploadExcelProduk, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  },
};

export default hki;
