# Install dependencies only when needed
FROM node:alpine3.15

# Set /app as the working directory
WORKDIR /root

# Copy package.json and package-lock.json
# to the /app working directory
COPY package*.json /root
COPY yarn.lock /root

# Install dependencies in /app
RUN yarn install

# Copy codebase to docker
COPY . /root

# Run nothing to make the container still run
CMD tail -f /dev/null
