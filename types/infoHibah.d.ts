interface Infohibah {
  id: number;
  judul: string;
  deskripsi: string;
  sumber: string;
  tanggal_buat: string;
  tanggal_ubah: string;
  poster: string;
}
