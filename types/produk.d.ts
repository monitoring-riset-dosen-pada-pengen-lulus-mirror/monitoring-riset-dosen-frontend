interface IProduk {
  id: string;
  pencipta: string[];
  tahun: string;
  judul_ciptaan: string;
  keterangan: string;
  jenis?: string;
  tanggal_permohonan?: string;
  link?: string;
}

interface IHKI extends IProduk {
  jenis_hki: string;
}
interface IPaten extends IProduk {
  jenis_paten: string;
}

type ProductType = 'hki' | 'paten';
