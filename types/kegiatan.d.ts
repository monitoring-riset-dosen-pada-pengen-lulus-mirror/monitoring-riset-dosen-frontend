interface IKegiatan {
  id: string;
  peneliti_utama: string;
  anggota_peneliti: string[];
  lab: ILab | null;
  output?: IOutput[];
  judul_penelitian: string;
  tahun: number;
  tipe_kegiatan?: string;
  is_non_hibah?: boolean;
  topik_penelitian?: ITopikPenelitian[];
}
interface IHibah extends IKegiatan {
  nilai_hibah_internasional?: number;
  nilai_hibah_nasional?: number;
  skema_pembiayaan?: string;
  no_kontrak_hibah?: string;
  pemberi_dana?: IPemberiDana | null;
  status_usulan?: string;
  no_nota_dinas?: string;
}
interface KegiatanPayload {
  tahun: number;
  judul_penelitian: string;
  lab?: string;
  peneliti_utama: string;
  anggota_peneliti?: string[];
  topik_penelitian?: string[];
  keterangan?: string;
  is_non_hibah: boolean;
}
interface HibahPayload extends KegiatanPayload {
  nilai_hibah_internasional?: number;
  nilai_hibah_nasional?: number;
  skema_pembiayaan?: string;
  no_kontrak_hibah?: string;
  pemberi_dana?: string;
  status_usulan?: string;
  no_nota_dinas?: string;
}
interface ILab {
  id: string | number;
  name: string;
  deskripsi: string;
}
interface IPemberiDana {
  id: string | number;
  nama: string;
  tipe_pendanaan: string;
}
interface ITopikPenelitian {
  id: string;
  nama_topik: string;
  deskripsi: string;
}
interface IOutput {
  id: string;
  judul_ciptaan: string;
  jenis: string;
}
type KegiatanType = 'pengabdian-masyarakat' | 'hibah' | 'riset';
type KegiatanAPIType = 'pengmas' | 'hibah' | 'riset';
