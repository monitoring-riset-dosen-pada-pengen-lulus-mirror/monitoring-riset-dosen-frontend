interface IDosen {
  uuid: string;
  nama_lengkap: string;
  jumlah_hibah: string;
  jumlah_hki: string;
  jumlah_paten: string;
  jumlah_riset: string;
  jumlah_pengmas: string;
  jumlah_pendanaan_nasional?: string;
  jumlah_pendanaan_internasional?: string;
}
