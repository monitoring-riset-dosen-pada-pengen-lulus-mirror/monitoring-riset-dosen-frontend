declare module 'redux-persist/lib/storage/createWebStorage';

interface ListResponse<T> {
  next: number | null;
  previous: number | null;
  count: number;
  results: T[];
}

interface ListResponseDashboard<T> {
  results: T;
}
