interface AccountType {
  id: number;
  user: string;
  nama_lengkap: string;
  email: string;
  nip?: string;
  status_kepegawaian?: string;
  npm?: string;
}

interface LoginDataType {
  account: AccountType;
  token: string;
  role: 'tenaga_kependidikan' | 'dosen' | 'mahasiswa';
  refresh: string;
  is_admin: boolean;
}
