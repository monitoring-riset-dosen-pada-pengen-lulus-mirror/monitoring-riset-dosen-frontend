import { ComponentStyleConfig } from '@chakra-ui/react';

const heading: ComponentStyleConfig = {
  baseStyle: {
    fontWeight: { base: 600, md: 700 },
  },
  sizes: {},
  variants: {
    h1: {
      fontSize: '72px',
      lineHeight: '100%',
    },
    h2: {
      fontSize: '60px',
      lineHeight: '100%',
    },
    h3: {
      fontSize: '48px',
      lineHeight: '100%',
    },
    h4: {
      fontSize: '36px',
      lineHeight: '120%',
    },
    h5: {
      fontSize: '30px',
      lineHeight: '120%',
    },
    h6: {
      lineHeight: '120%',
      fontSize: '20px',
    },
  },
};

export default heading;
