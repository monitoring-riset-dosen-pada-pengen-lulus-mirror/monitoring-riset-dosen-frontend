import { ComponentStyleConfig } from '@chakra-ui/react';

const text: ComponentStyleConfig = {
  baseStyle: {
    fontWeight: 400,
  },
  sizes: {},
  variants: {
    '3x-large': {
      fontSize: { base: '2xl', md: '3xl' },
    },
    '2x-large': {
      fontSize: { base: 'xl', md: '2xl' },
    },
    'x-large': {
      fontSize: { base: 'lg', md: 'xl' },
    },
    large: {
      fontSize: { base: 'md', md: 'lg' },
    },
    medium: {
      fontSize: { base: 'sm', md: 'md' },
    },
    small: {
      fontSize: { base: 'xs', md: 'sm' },
    },
  },
};

export default text;
