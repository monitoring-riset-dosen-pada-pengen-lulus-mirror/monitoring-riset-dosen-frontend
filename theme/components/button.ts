import { ComponentStyleConfig } from '@chakra-ui/react';

const button: ComponentStyleConfig = {
  baseStyle: {
    fontWeight: 'bold', // Normally, it is "semibold"
    p: '8px 24px',
  },

  defaultProps: {
    _active: {
      bg: 'none',
    },
    _hover: {
      bg: 'none',
    },
  },

  sizes: {
    xl: {
      h: '56px',
      fontSize: 'lg',
      px: '32px',
    },
  },

  variants: {},
};

export default button;
