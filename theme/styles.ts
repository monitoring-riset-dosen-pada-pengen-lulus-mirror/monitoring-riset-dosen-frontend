const style = {
  styles: {
    global: {
      // styles for the `body`
      body: {
        color: 'black',
        bg: 'gray.400',
      },

      a: {
        _hover: {
          textDecoration: 'none',
        },
      },
    },
  },
};

export const colors = {
  blue: {
    50: '#e7f0ff',
    100: '#c1d2f1',
    200: '#9bb4e3',
    300: '#7596d7',
    400: '#4f78cb',
    500: '#355fb1',
    600: '#294a8a',
    700: '#1c3564',
    800: '#0f203e',
    900: '#020b1a',
  },
  red: {
    50: '#ffe4e6',
    100: '#fbb9bd',
    200: '#f38d91',
    300: '#ec5f66',
    400: '#e5333b',
    500: '#cc1a21',
    600: '#a01219',
    700: '#720a12',
    800: '#470508',
    900: '#1f0000',
  },
  gray: {
    '50': '#F2F2F2',
    '100': '#DBDBDB',
    '200': '#C4C4C4',
    '300': '#ADADAD',
    '400': '#969696',
    '500': '#808080',
    '600': '#666666',
    '700': '#4D4D4D',
    '800': '#333333',
    '900': '#1A1A1A',
  },
};

export const fontSizes = {
  xs: '12px',
  sm: '14px',
  md: '16px',
  lg: '18px',
  xl: '20px',
  '2xl': '24px',
  '3xl': '30px',
  '4xl': '36px',
  '5xl': '48px',
  '6xl': '60px',
  '7xl': '72px',
  '8xl': '96px',
  '9xl': '128px',
};

export default style;
