// theme/index.js
import { extendTheme } from '@chakra-ui/react';

// Global style overrides
import styles, { colors, fontSizes } from './styles';

// Component style overrides
import Button from './components/button';
import Heading from './components/heading';
import Text from './components/text';

const overrides = {
  fonts: {
    heading: 'Inter, sans-serif',
    body: 'Inter, sans-serif',
  },
  styles,
  colors,
  fontSizes,

  // Other foundational style overrides go here
  components: {
    Button,
    Heading,
    Text,
    // Other components go here
  },
};

export default extendTheme(overrides);
