import { ChakraProvider } from '@chakra-ui/react';
import { render } from '@testing-library/react';
import makeStore, { wrapper } from '@/redux/store';
import { Provider } from 'react-redux';

export const renderWithTheme = (ui, { store } = { store: makeStore() }) => {

  const Wrapper = ({ children }) => (
    <Provider store={store}>
      <ChakraProvider>{children}</ChakraProvider>
    </Provider>
  );

  // @ts-ignore
  return render(ui, { wrapper: wrapper.withRedux(Wrapper) });
};

export const sleep = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};
