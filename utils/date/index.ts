import { ISelectOption } from '@/components/form/form';

const START_YEAR = 2007;

export const getCurrentYear = (): number => new Date().getFullYear();

export const getAllPossibleYearChoices = (): ISelectOption[] => {
  const arr: ISelectOption[] = [];
  for (let i = START_YEAR; i < getCurrentYear() + 2; i++) {
    arr.push({
      value: i,
      label: i,
    });
  }
  return arr;
};
