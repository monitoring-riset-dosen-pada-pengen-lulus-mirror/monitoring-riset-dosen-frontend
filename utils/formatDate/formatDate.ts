const formatDate = (dateISO: string) => {
  return new Date(dateISO).toLocaleDateString('id-ID', {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  });
};

export default formatDate;
