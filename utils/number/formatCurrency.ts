const formatCurrency = (amount: number) =>
  amount.toLocaleString('id-ID', { style: 'currency', currency: 'IDR' });
export default formatCurrency;
