import { AxiosResponse } from 'axios';

const resolve = async (promise: Promise<AxiosResponse<any, any>>) => {
  try {
    const result = await promise;
    return { result: result.data, error: null };
  } catch (error) {
    return { result: null, error };
  }
};

export default resolve;
