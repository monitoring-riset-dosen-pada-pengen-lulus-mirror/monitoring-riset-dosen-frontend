const addUrlExtension = (sumber: string) => {
  const startIndexHttps = sumber.search('https://');
  const startIndexHttp = sumber.search('http://');
  let sumberWithWebsiteExtension = sumber;
  const NOT_EXIST = -1;
  if (startIndexHttp == NOT_EXIST && startIndexHttps == NOT_EXIST) {
    sumberWithWebsiteExtension = 'https://' + sumberWithWebsiteExtension;
  }
  return sumberWithWebsiteExtension;
};

export default addUrlExtension;
