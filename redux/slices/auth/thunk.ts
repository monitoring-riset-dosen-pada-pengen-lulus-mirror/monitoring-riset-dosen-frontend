import { createAsyncThunk } from '@reduxjs/toolkit';
import api from '@/api/services';
import { TicketPayload } from '@/components/common/Layout';
import { ErrorToast, SuccessToast } from '@/components/Toast';

export const loginThunk = createAsyncThunk<LoginDataType, TicketPayload>(
  'auth/login',
  async (ticketService, { rejectWithValue }) => {
    try {
      const response = await api.auth.loginService(ticketService);
      SuccessToast({ title: 'Login Berhasil' });
      return response.data;
    } catch (err: any) {
      ErrorToast({ title: 'Login Gagal', description: `${err.response.data.message}` });
      return rejectWithValue(err.response.data);
    }
  },
);
