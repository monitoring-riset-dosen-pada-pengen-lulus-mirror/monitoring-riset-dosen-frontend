import { createSelector } from '@reduxjs/toolkit';

const getAuthAccount = (state: { auth: { account: AccountType } }) => state.auth.account;
const getAdminState = (state: { auth: { is_admin: boolean } }) => state.auth.is_admin;

export const getAccount = createSelector(getAuthAccount, (account: AccountType) => account);
export const isAdmin = createSelector(getAdminState, (is_admin: boolean) => is_admin);
