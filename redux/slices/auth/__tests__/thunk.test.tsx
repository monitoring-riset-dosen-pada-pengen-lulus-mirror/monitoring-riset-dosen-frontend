import config from '@/api/config';
import makeStore from '@/redux/store';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { initialState } from '..';
import { loginThunk } from '../thunk';

export const mockPayload = {
  ticket: 'ST-123-abcdefg12345-sso.ui.ac.id',
  service_url: config.HOST_URL(),
};

export const mockResponse = {
  account: {
    id: 1,
    nama_lengkap: 'User 1',
    user: 'user1',
    email: 'user1@ui.ac.id',
    npm: '1234567890',
  },
  token: null,
  loading: false,
  role: 'mahasiswa',
};

describe('Auth login asyncThunk tests', () => {
  it('posts ticket and then successfully return and store account data', async () => {
    const mock = new MockAdapter(axios);
    mock.onPost('/account/login', mockPayload).reply(200, mockResponse);

    const store = makeStore();
    const result = await store.dispatch(loginThunk(mockPayload));

    expect(result.type).toBe('auth/login/fulfilled');

    const state = store.getState().auth;
    expect(state.account).toEqual(mockResponse.account);
    expect(state.token).toEqual(mockResponse.token);
    expect(state.role).toEqual(mockResponse.role);
    expect(state.loading).toEqual(mockResponse.loading);
  });

  it('posts wrong ticket and then fails to return and store account data', async () => {
    const mock = new MockAdapter(axios);
    mock.onPost('/account/login', mockPayload).reply(400);

    const store = makeStore();
    const result = await store.dispatch(loginThunk(mockPayload));

    expect(result.type).toBe('auth/login/rejected');

    const state = store.getState().auth;
    expect(state.account).toEqual(initialState.account);
    expect(state.token).toEqual(initialState.token);
    expect(state.role).toEqual(initialState.role);
    expect(state.loading).toEqual(initialState.loading);
  });
});
