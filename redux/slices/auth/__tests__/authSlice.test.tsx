import config from '@/api/config';
import makeStore from '@/redux/store';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';
import { clearAccount, initialState, setLoading } from '..';
import { loginThunk } from '../thunk';
import api from '@/api/services';

const mockPayload = {
  ticket: 'ST-123-abcdefg12345-sso.ui.ac.id',
  service_url: config.HOST_URL(),
};

const mockResponse = {
  account: {
    id: 1,
    nama_lengkap: 'User 1',
    user: 'user1',
    email: 'user1@ui.ac.id',
    npm: '1234567890',
  },
  token: null,
  loading: false,
  role: 'mahasiswa',
};

describe('Auth login asyncThunk tests', () => {
  const store = makeStore();
  const authState = store.getState().auth;

  it('successfully sets loading state', async () => {
    await store.dispatch(setLoading(true));
    setTimeout(() => {
      expect(authState.loading).toBe(true);
    }, 1000);
  });

  it('successfully clear user data', async () => {
    const mock = new MockAdapter(axios);
    mock.onPost('/account/login', mockPayload).reply(200, mockResponse);

    await store.dispatch(loginThunk(mockPayload));
    expect(authState.account).toEqual(initialState.account);

    await store.dispatch(clearAccount());
    expect(authState.account).toBe(null);
  });

  it('logout and then clear localstorage', async () => {
    window.localStorage.setItem('persist:auth', JSON.stringify(mockResponse));
    expect(window.localStorage['persist:auth']).toEqual(JSON.stringify(mockResponse));

    const { logoutService } = api.auth;
    logoutService();
    expect(window.localStorage['persist:auth']).toBeUndefined;
    expect(window.localStorage.length).toEqual(0);
  });
});
