import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { loginThunk } from './thunk';

export interface AuthState {
  account: AccountType | null;
  token: string | null;
  loading: boolean;
  role: 'tenaga_kependidikan' | 'dosen' | 'mahasiswa' | null;
  is_admin: boolean;
}

export const initialState: AuthState = {
  account: null,
  token: null,
  loading: false,
  role: null,
  is_admin: false,
};

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    setLoading: (state, { payload }) => {
      state.loading = payload.loading;
    },
    clearAccount: (state) => {
      state.account = null;
      state.token = null;
      state.loading = false;
      state.role = null;
      state.is_admin = false;
    },
    setAccount: (state, { payload }: PayloadAction<LoginDataType>) => {
      const { account, token, role, refresh, is_admin } = payload;
      state.account = account;
      state.token = token;
      state.role = role;
      state.is_admin = is_admin;
      window.localStorage.setItem('token', token);
      window.localStorage.setItem('refresh', refresh);
      window.localStorage.setItem('is_admin', is_admin?.toString());
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loginThunk.pending, (state) => {
        state.loading = true;
      })
      .addCase(loginThunk.fulfilled, (state, action) => {
        authSlice.caseReducers.setAccount(state, action);
        state.loading = false;
      })
      .addCase(loginThunk.rejected, (state) => {
        state.loading = false;
      });
  },
});

export const { setLoading, clearAccount, setAccount } = authSlice.actions;

export default authSlice.reducer;
