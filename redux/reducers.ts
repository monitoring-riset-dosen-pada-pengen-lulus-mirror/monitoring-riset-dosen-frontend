import { combineReducers } from '@reduxjs/toolkit';
import storage from './_storage';
import { persistReducer } from 'redux-persist';

import authReducer, { authSlice } from '@/redux/slices/auth';

const authPersistConfig = {
  key: authSlice.name,
  storage,
};

const reducers = combineReducers({
  auth: persistReducer(authPersistConfig, authReducer),
});

declare global {
  type DefaultRootState = ReturnType<typeof reducers>;
}

export default reducers;
