import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import { fireEvent, screen } from '@testing-library/react';
import { act } from 'react-test-renderer';
import { act as actUtils } from 'react-dom/test-utils';
import DosenPage from '@/pages/dosen';

jest.mock('next/dist/client/router', () => require('next-router-mock'));

describe('Daftar Dosen Container', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });

    // to fully reset the state between tests, clear the storage
    localStorage.clear();
    // and reset all mocks
    jest.clearAllMocks();

    localStorage.setItem('token', 'dummytoken');
    act(() => {
      renderWithTheme(<DosenPage />);
    });
  });

  const isDaftarDosenLoading = () => {
    expect(screen.queryAllByTestId('list-dosen-skeleton')).toBeVisible;
    expect(screen.queryAllByTestId('list-dosen')).not.toBeVisible;
    expect(screen.queryAllByText('terdapat kesalahan')).not.toBeVisible;
  };

  const loadDaftarDosenSuccess = () => {
    expect(screen.queryAllByTestId('list-dosen')).toBeVisible;
    expect(screen.queryAllByTestId('skeleton')).not.toBeVisible;
    expect(screen.queryAllByText('terdapat kesalahan')).not.toBeVisible;
  };

  it('Success on next page clicked', async () => {
    await actUtils(() => sleep(100));

    const NextBtn = screen.getByText('Selanjutnya');
    actUtils(() => {
      fireEvent.click(NextBtn);
    });

    expect(screen.queryAllByTestId('skeleton')).toBeVisible;

    await actUtils(() => sleep(100));

    expect(screen.queryAllByTestId('skeleton')).not.toBeVisible;
  });

  it('Success get info hibah list', async () => {
    await actUtils(() => sleep(100));

    isDaftarDosenLoading();

    await actUtils(() => sleep(100));

    loadDaftarDosenSuccess;
  });

  it('searchbar should work', async () => {
    await actUtils(() => sleep(100));

    const InputSearchBar = screen.getByTestId('searchbar');
    const resultPaten = screen.queryAllByText('tes1');

    fireEvent.click(InputSearchBar);
    fireEvent.change(InputSearchBar, { target: { value: 'tes1' } });
    fireEvent.submit(InputSearchBar);

    await actUtils(() => sleep(100));

    isDaftarDosenLoading();

    await actUtils(() => sleep(100));

    loadDaftarDosenSuccess();
    expect(resultPaten).toBeVisible;
  });

  it('pagination should work', async () => {
    await actUtils(() => sleep(100));

    const ReactPaginator = screen.getByTestId('react-paginator');
    const NextPageBtn = ReactPaginator.getElementsByTagName('a')[3];

    expect(NextPageBtn.text).toEqual('Selanjutnya');
    fireEvent.click(NextPageBtn);

    await actUtils(() => sleep(100));

    isDaftarDosenLoading();

    await actUtils(() => sleep(100));

    loadDaftarDosenSuccess();
    expect(screen.queryByText('tes page 2')).toBeVisible;
  });
});
