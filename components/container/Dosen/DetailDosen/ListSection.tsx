import { Box, Heading, Text } from '@chakra-ui/react';
import { FC } from 'react';

interface IListSection {
  type: string;
  title: string;
}

const ListSection: FC<IListSection> = ({ title, type, children }) => {
  return (
    <Box py={8}>
      <Text fontSize="lg">{type}</Text>
      <Heading mb="14px" variant="h5">
        {title}
      </Heading>
      {children}
    </Box>
  );
};

export default ListSection;
