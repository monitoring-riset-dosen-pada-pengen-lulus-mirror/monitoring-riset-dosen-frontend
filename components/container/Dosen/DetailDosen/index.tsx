import { Divider } from '@chakra-ui/react';
import OverviewSection from './OverviewSection';
import RisetListDosen from './RisetListDosen';
import PengmasListDosen from './PengmasListDosen';
import HkiListDosen from './HkiListDosen';

const DetailDosen = () => {
  return (
    <>
      <OverviewSection />
      <Divider />
      <RisetListDosen />
      <Divider />
      <PengmasListDosen />
      <Divider />
      <HkiListDosen />
      <Divider />
    </>
  );
};

export default DetailDosen;
