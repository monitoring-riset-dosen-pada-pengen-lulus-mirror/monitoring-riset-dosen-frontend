import dosen from '@/api/services/dosen';
import useFetchList from '@/hooks/useFetchList';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';

import { initialPatenFilter, PatenFilterValues } from '../../Produk/Paten';
import ListPaten from '../../Produk/Paten/ListPaten';
import ListSection from './ListSection';

const PAGE_SIZE = 5;

const HkiListDosen = () => {
  const router = useRouter();
  const { id } = router.query as { id: string };

  const patenForm = useForm<PatenFilterValues>({
    defaultValues: { ...initialPatenFilter, page_size: PAGE_SIZE },
  });
  const { watch: watchPaten, setValue: setValuePaten } = patenForm;
  const patenData = useFetchList<IPaten>(dosen.getPatenList(id, { ...watchPaten() }), [
    watchPaten().page,
    id,
  ]);
  const handlePageChangePaten = (event: { selected: number }) => {
    setValuePaten('page', event.selected + 1);
  };

  return (
    <ListSection title="Paten" type="Output">
      <ListPaten data={patenData} form={patenForm} handlePageChange={handlePageChangePaten} />
    </ListSection>
  );
};

export default HkiListDosen;
