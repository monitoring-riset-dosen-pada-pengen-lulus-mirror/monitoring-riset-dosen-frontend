import DetailDosen from '@/components/container/Dosen/DetailDosen';
import { screen } from '@testing-library/react';
import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import mockRouter from 'next-router-mock';
import 'next-router-mock/dynamic-routes';
import { act } from 'react-test-renderer';
import { act as actUtils } from 'react-dom/test-utils';
import { server } from '@/mocks/servers';
import { getDetailDosenError } from '@/mocks/handlers/dosen';

jest.mock('next/dist/client/router', () => require('next-router-mock'));
mockRouter.registerPaths(['/dosen/[id]']);

describe('Detail Dosen', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });
  });

  it('successful renders dosen detail', async () => {
    mockRouter.setCurrentUrl('/dosen/1');

    act(() => {
      renderWithTheme(<DetailDosen />);
    });

    expect(screen.queryByTestId('detail-dosen-skeleton')).not.toEqual(null);
    expect(screen.queryByTestId('detail-dosen-error')).toEqual(null);
    expect(screen.queryByTestId('detail-dosen-data')).toEqual(null);

    await actUtils(() => sleep(100));
  });

  it('failed renders dosen detail', async () => {
    server.use(getDetailDosenError);
    mockRouter.setCurrentUrl('/dosen/1');
    act(() => {
      renderWithTheme(<DetailDosen />);
    });
    expect(screen.queryByTestId('detail-dosen-skeleton')).not.toEqual(null);
    expect(screen.queryByTestId('detail-dosen-error')).toEqual(null);
    expect(screen.queryByTestId('detail-dosen-data')).toEqual(null);

    await actUtils(() => sleep(100));
  });
});
