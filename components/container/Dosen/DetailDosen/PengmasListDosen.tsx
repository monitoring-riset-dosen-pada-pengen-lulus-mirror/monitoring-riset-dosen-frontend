import dosen from '@/api/services/dosen';
import useFetchList from '@/hooks/useFetchList';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { initialPengmasFilter, PengmasFilterValues } from '../../Kegiatan/Pengmas';
import PengmasList from '../../Kegiatan/Pengmas/PengmasList';
import ListSection from './ListSection';

const PAGE_SIZE = 5;

const PengmasListDosen = () => {
  const router = useRouter();
  const { id } = router.query as { id: string };

  const pengmasForm = useForm<PengmasFilterValues>({
    defaultValues: { ...initialPengmasFilter, page_size: PAGE_SIZE },
  });
  const { watch: watchPengmas, setValue: setValuePengmas } = pengmasForm;
  const pengmasData = useFetchList<IHibah>(dosen.getPengmasList(id, { ...watchPengmas() }), [
    watchPengmas().page,
    id,
  ]);
  const handlePageChangePengmas = (event: { selected: number }) => {
    setValuePengmas('page', event.selected + 1);
  };

  return (
    <ListSection title="Pengabdian Masyarakat" type="Kegiatan">
      <PengmasList data={pengmasData} form={pengmasForm} handlePageChange={handlePageChangePengmas} />
    </ListSection>
  );
};

export default PengmasListDosen;
