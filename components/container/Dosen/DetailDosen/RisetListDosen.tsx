import dosen from '@/api/services/dosen';
import useFetchList from '@/hooks/useFetchList';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { initialRisetFilter, RisetFilterValues } from '../../Kegiatan/Riset';
import RisetList from '../../Kegiatan/Riset/RisetList';
import ListSection from './ListSection';

const PAGE_SIZE = 5;

const RisetListDosen = () => {
  const router = useRouter();
  const { id } = router.query as { id: string };

  const risetForm = useForm<RisetFilterValues>({
    defaultValues: { ...initialRisetFilter, page_size: PAGE_SIZE },
  });
  const { watch: watchRiset, setValue: setValueRiset } = risetForm;
  const risetData = useFetchList<IHibah>(dosen.getRisetList(id, { ...watchRiset() }), [
    watchRiset().page,
    id,
  ]);
  const handlePageChangeRiset = (event: { selected: number }) => {
    setValueRiset('page', event.selected + 1);
  };
  return (
    <ListSection title="Riset" type="Kegiatan">
      <RisetList data={risetData} form={risetForm} handlePageChange={handlePageChangeRiset} />
    </ListSection>
  );
};

export default RisetListDosen;
