import services from '@/api/services';
import useFetchDetail from '@/hooks/useFetchDetail';
import formatCurrency from '@/utils/number/formatCurrency';
import { Box, Flex, Heading, Skeleton, StackDivider, Text, VStack } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { ErrorState } from '../../InfoHibah';

const { dosen } = services;

const OverviewSection = () => {
  const router = useRouter();
  const { id } = router.query as { id: string };
  const { isError, isLoading, result } = useFetchDetail<IDosen>(dosen.getDetail(id), id);

  const {
    nama_lengkap,
    jumlah_hibah,
    jumlah_hki,
    jumlah_paten,
    jumlah_pengmas,
    jumlah_riset,
    jumlah_pendanaan_internasional,
    jumlah_pendanaan_nasional,
  } = result as IDosen;

  if (isError) {
    return (
      <span data-testid="detail-dosen-error">
        <ErrorState />
      </span>
    );
  }
  if (isLoading) {
    return (
      <Flex direction="column" gap={4} data-testid="detail-dosen-skeleton">
        <Skeleton h="100px" minW="xs" borderRadius="10">
          <Box minW="xs" minH="md"></Box>
        </Skeleton>
      </Flex>
    );
  }

  return (
    <Flex justify="space-between" align="center" mb="20px" data-testid="detail-dosen-data">
      <Heading variant="h5">{nama_lengkap}</Heading>
      <Box>
        <Heading mb="14px" variant="h6">
          Overview
        </Heading>
        <VStack divider={<StackDivider borderColor="gray.200" />}>
          <Flex justify="space-between" minW="265px">
            <Text fontSize="md">Jumlah Hibah</Text>
            <Text fontSize="md">{jumlah_hibah}</Text>
          </Flex>
          <Flex justify="space-between" minW="265px">
            <Text fontSize="md">Jumlah HKI</Text>
            <Text fontSize="md">{jumlah_hki}</Text>
          </Flex>
          <Flex justify="space-between" minW="265px">
            <Text fontSize="md">Jumlah Paten</Text>
            <Text fontSize="md">{jumlah_paten}</Text>
          </Flex>
          <Flex align="center" justify="space-between" minW="265px">
            <Text maxW="150px" fontSize="md">
              Jumlah Pengabdian Masyarakat
            </Text>
            <Text fontSize="md">{jumlah_pengmas}</Text>
          </Flex>
          <Flex justify="space-between" minW="265px">
            <Text fontSize="md">Jumlah Riset</Text>
            <Text fontSize="md">{jumlah_riset}</Text>
          </Flex>
          <Flex align="center" justify="space-between" minW="265px">
            <Text maxW="150px" fontSize="md">
              Jumlah Pendanaan Nasional Tahun Ini
            </Text>
            <Text fontSize="md">{formatCurrency(+(jumlah_pendanaan_nasional || 0))}</Text>
          </Flex>
          <Flex align="center" justify="space-between" minW="265px">
            <Text maxW="180px" fontSize="md">
              Jumlah Pendanaan Internasional Tahun Ini
            </Text>
            <Text fontSize="md">{formatCurrency(+(jumlah_pendanaan_internasional || 0))}</Text>
          </Flex>
        </VStack>
      </Box>
    </Flex>
  );
};

export default OverviewSection;
