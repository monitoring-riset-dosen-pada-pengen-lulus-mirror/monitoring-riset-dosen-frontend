import dosen from '@/api/services/dosen';
import useFetchList from '@/hooks/useFetchList';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';

import { HKIFilterValues, initialHKIFilter } from '../../Produk/HKI';
import ListHKI from '../../Produk/HKI/ListHKI';
import ListSection from './ListSection';

const PAGE_SIZE = 5;

const HkiListDosen = () => {
  const router = useRouter();
  const { id } = router.query as { id: string };

  const hkiForm = useForm<HKIFilterValues>({
    defaultValues: { ...initialHKIFilter, page_size: PAGE_SIZE },
  });
  const { watch: watchHki, setValue: setValueHki } = hkiForm;
  const hkiData = useFetchList<IHKI>(dosen.getHkiList(id, { ...watchHki() }), [watchHki().page, id]);
  const handlePageChangeHki = (event: { selected: number }) => {
    setValueHki('page', event.selected + 1);
  };

  return (
    <ListSection title="Hak Kekayan Intelektual" type="Output">
      <ListHKI data={hkiData} form={hkiForm} handlePageChange={handlePageChangeHki} />
    </ListSection>
  );
};

export default HkiListDosen;
