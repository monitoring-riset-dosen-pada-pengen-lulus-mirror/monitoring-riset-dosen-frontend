import { useRouter } from 'next/router';
import { getAllUrlParams } from '@/utils/api/url';
import { useState } from 'react';
import useFetchList from '@/hooks/useFetchList';
import { Heading } from '@chakra-ui/react';
import Searchbar from '@/components/Searchbar';
import dosen from '@/api/services/dosen';
import ListDosen from '@/components/container/Dosen/ListDosen';

const Dosen = () => {
  const router = useRouter();
  const FIRST_PAGE = 1;
  const page = getAllUrlParams(router.asPath)['page'] || FIRST_PAGE;
  const [params, setParams] = useState({ search: '', page });
  const data = useFetchList<IDosen>(dosen.getAll(params), [params]);
  const handlePageChange = (event: { selected: number }) => {
    router.push(`${router.pathname}?page=${event.selected + 1}`);
    setParams({ ...params, page: event.selected + 1 });
  };
  const onSearchKeyword = async (query: string) => {
    router.push(`${router.pathname}?page=1`);
    setParams({ ...params, search: query, page: FIRST_PAGE });
  };
  return (
    <>
      <Heading variant="h4" py="4">
        Daftar Dosen
      </Heading>
      <Searchbar placeholder="Cari Nama Dosen..." width="full" onSubmit={onSearchKeyword} />
      <ListDosen data={data} handlePageChange={handlePageChange} />
    </>
  );
};

export default Dosen;
