import { FC } from 'react';
import { UseFetchListHook } from '@/hooks/useFetchList';
import { Box, Flex, Skeleton } from '@chakra-ui/react';
import CardListDosen from '@/components/common/CardListDosen';
import Paginator from '@/components/common/Paginator';
import { getAllUrlParams } from '@/utils/api/url';
import { useRouter } from 'next/router';
import { range } from 'lodash';
import { ErrorState } from '@/components/container/InfoHibah';

interface Props {
  data: UseFetchListHook<IDosen>;
  handlePageChange: (event: { selected: number }) => void;
}

const SkeletonListDosen: FC<any> = () => {
  return (
    <Flex mt="24px" gap={8} direction={{ base: 'column', md: 'row' }}>
      <Box w="full">
        <Flex direction="column" gap={4} mb={8} data-testid="list-dosen-skeleton">
          {range(5).map((item) => (
            <Skeleton borderRadius="8px" key={item} h="130px" />
          ))}
        </Flex>
      </Box>
    </Flex>
  );
};

const ListDosen: FC<Props> = (props) => {
  const router = useRouter();
  const { data, handlePageChange } = props;
  const {
    isError,
    isLoading,
    result: { count, results },
  } = data;
  const FIRST_PAGE = 1;
  const page = getAllUrlParams(router.asPath)['page'] || FIRST_PAGE;
  if (isLoading) {
    return <SkeletonListDosen />;
  }
  if (isError || !results.length) {
    return (
      <span data-testid="daftar-dosen-error">
        <ErrorState />
      </span>
    );
  }
  return (
    <Flex mt="24px" gap={8} direction={{ base: 'column', md: 'row' }}>
      <Box w="full">
        <Flex direction="column" gap={4} mb={8} data-testid="list-dosen">
          {results.map((item) => {
            const onClick = () => router.push(`/dosen/${item.uuid}`);
            return <CardListDosen onClick={onClick} data={item} key={item.uuid} />;
          })}
        </Flex>
        <Paginator pageCount={Math.ceil(count / 10)} onPageChange={handlePageChange} page={page} />
      </Box>
    </Flex>
  );
};

export default ListDosen;
