interface IDatasets {
  label: string;
  data: number[];
  backgroundColor: string;
}

const createGraphFormatData = (
  labels: number[],
  datasetsLabels: string[],
  listData: number[][],
  backgroundColor: string[],
) => {
  const datasets: IDatasets[] = [];
  datasetsLabels.forEach((value: string, index: number) => {
    datasets.push({
      label: value,
      data: listData[index],
      backgroundColor: backgroundColor[index],
    });
  });
  return { labels, datasets: datasets };
};

export default createGraphFormatData;
