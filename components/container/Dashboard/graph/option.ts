const createGraphOption = (graphName: string, yLabel = 'Rupiah', stepSize = 0) => {
  return {
    responsive: true,
    plugins: {
      legend: {
        position: 'top' as const,
      },
      title: {
        display: true,
        text: graphName,
        font: {
          size: 25,
        },
      },
    },
    maintainAspectRatio: true,
    scales: {
      r: {
        ticks: {
          stepSize: 10,
        },
      },
      y: {
        title: {
          display: true,
          text: yLabel,
        },
        ticks: {
          stepSize: stepSize,
        },
      },
      x: {
        title: {
          display: true,
          text: 'Tahun',
        },
      },
    },
  };
};

export default createGraphOption;
