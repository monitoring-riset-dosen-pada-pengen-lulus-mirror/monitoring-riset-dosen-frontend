import { BarElement, CategoryScale, Chart as ChartJS, Legend, LinearScale, Title, Tooltip } from 'chart.js';
import { Bar } from 'react-chartjs-2';
import dashboard from '@/api/services/dashboard';
import useFetchDashboard from '@/hooks/useFetchDashboard';
import { Box, Center, Skeleton, Text } from '@chakra-ui/react';
import { Heading } from '@chakra-ui/layout';
import createGraphOption from '@/components/container/Dashboard/graph/option';
import createGraphFormatData from '@/components/container/Dashboard/graph/format-data';

const Dashboard = () => {
  const data = useFetchDashboard(dashboard.getAll());
  const currentYear = new Date().getFullYear();
  const years = [];
  for (let year = currentYear - 9; year <= currentYear; year++) {
    years.push(year);
  }
  const pendanaanRiset: number[] = [];
  const pendanaanPengmas: number[] = [];
  const pendanaanHibahTotal: number[] = [];
  const pendanaanHibahInternalUI: number[] = [];
  const pendanaanHibahEksternalNasional: number[] = [];
  const pendanaanHibahEksternalInternasional: number[] = [];
  const jumlahHKIBasdat: number[] = [];
  const jumlahHKIPaten: number[] = [];
  const jumlahHKIKaryaTulis: number[] = [];
  const jumlahHKIProgramKomputer: number[] = [];
  const jumlahHKIBuku: number[] = [];
  const jumlahPaten: number[] = [];
  const jumlahPatenSederhana: number[] = [];
  const {
    isLoading,
    result: { pendanaan_riset, pendanaan_hibah, pendanaan_pengmas, jumlah_hki, jumlah_paten },
  } = data;

  if (pendanaan_riset !== null) {
    years.forEach((year) => {
      pendanaanRiset.push(pendanaan_riset[year]);
      pendanaanHibahTotal.push(pendanaan_hibah['total'][year]);
      pendanaanHibahInternalUI.push(pendanaan_hibah['internal_ui'][year]);
      pendanaanHibahEksternalNasional.push(pendanaan_hibah['eksternal_nasional'][year]);
      pendanaanHibahEksternalInternasional.push(pendanaan_hibah['eksternal_internasional'][year]);
      pendanaanPengmas.push(pendanaan_pengmas[year]);
      jumlahHKIBasdat.push(jumlah_hki['basis_data'][year]);
      jumlahHKIPaten.push(jumlah_hki['paten'][year]);
      jumlahHKIKaryaTulis.push(jumlah_hki['karya_tulis'][year]);
      jumlahHKIProgramKomputer.push(jumlah_hki['program_komputer'][year]);
      jumlahHKIBuku.push(jumlah_hki['buku'][year]);
      jumlahPaten.push(jumlah_paten['paten'][year]);
      jumlahPatenSederhana.push(jumlah_paten['paten_sederhana'][year]);
    });
  }

  ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip, Legend);

  const graphOptions = [
    createGraphOption('Statistik Total Pendanaan Hibah per Tahun'),
    createGraphOption('Statistik Total Pendanaan Hibah Riset per Tahun'),
    createGraphOption('Statistik Pendanaan Hibah Pengabdian Masyarakat per Tahun'),
    createGraphOption('Statistik Jumlah Hak Kekayaan Intelektual per Tahun', 'Jumlah', 1),
    createGraphOption('Statistik Jumlah Paten per Tahun', 'Jumlah', 1),
  ];

  const pendanaanHibahGraphFormat = createGraphFormatData(
    years,
    ['Total Hibah', 'Internal UI', 'Eksternal Nasional', 'Eksternal Internasional'],
    [
      pendanaanHibahTotal,
      pendanaanHibahInternalUI,
      pendanaanHibahEksternalNasional,
      pendanaanHibahEksternalInternasional,
    ],
    ['#FE632D', '#83D049', 'rgb(143, 140, 242)', 'rgb(60, 60, 216)'],
  );

  const pendanaanRisetGraphFormat = createGraphFormatData(
    years,
    ['Hibah Riset'],
    [pendanaanRiset],
    ['rgb(143, 140, 242)'],
  );

  const pendanaanPengmasGraphFormat = createGraphFormatData(
    years,
    ['Hibah Pengabdian Masyarakat'],
    [pendanaanPengmas],
    ['#83D049'],
  );

  const jumlahHKIGraphFormat = createGraphFormatData(
    years,
    ['Buku', 'Basis Data', 'Paten', 'Karya Tulis', 'Program Komputer'],
    [jumlahHKIBuku, jumlahHKIBasdat, jumlahHKIPaten, jumlahHKIKaryaTulis, jumlahHKIProgramKomputer],
    ['#FE632D', '#83D049', 'rgb(143, 140, 242)', 'rgb(115,225,166)', 'rgb(60,81,216)'],
  );

  const jumlahPatenGraphFormat = createGraphFormatData(
    years,
    ['Paten', 'Paten Sederhana'],
    [jumlahPaten, jumlahPatenSederhana],
    ['#FE632D', '#83D049'],
  );

  const graphData: any[] = [
    pendanaanHibahGraphFormat,
    pendanaanRisetGraphFormat,
    pendanaanPengmasGraphFormat,
    jumlahHKIGraphFormat,
    jumlahPatenGraphFormat,
  ];
  const graph_width = '80%';
  const graph_margin_bot = '50px';
  return (
    <>
      <Box textAlign="center">
        <Heading variant="h4">
          Sistem Informasi <br /> Hibah dan Pengabdian Masyarakat
        </Heading>
        <Text mb={'50px'} mt="30px" variant="x-large">
          Visualisasi Kondisi Riset dan Pengabdian Masyarakat <br />
          di Fakultas Ilmu Komputer Universitas Indonesia
        </Text>
      </Box>
      {graphData.map((item, index) => (
        <Skeleton key={index} isLoaded={!isLoading}>
          <Center mb={graph_margin_bot}>
            <div style={{ width: graph_width }}>
              <Bar data-testid="dashboard-graph" options={graphOptions[index]} data={item} />
            </div>
          </Center>
        </Skeleton>
      ))}
    </>
  );
};

export default Dashboard;
