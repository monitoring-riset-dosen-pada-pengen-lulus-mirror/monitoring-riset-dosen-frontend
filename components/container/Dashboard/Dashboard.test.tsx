import formatData from '@/components/container/Dashboard/graph/format-data';
import createGraphOption from '@/components/container/Dashboard/graph/option';

jest.mock('next/dist/client/router', () => require('next-router-mock'));

describe('Dashboard Container', () => {
  it('format data should work', async () => {
    const labels = [123];
    const datasetLabels = ['tes'];
    const listData = [[132]];
    const backgroundColor = ['rgb(41, 41, 41)'];
    const result = formatData(labels, datasetLabels, listData, backgroundColor);
    expect(result['labels']).toEqual(labels);
    expect(result['datasets'][0]['label']).toEqual(datasetLabels[0]);
    expect(result['datasets'][0]['data']).toEqual(listData[0]);
    expect(result['datasets'][0]['backgroundColor']).toEqual(backgroundColor[0]);
  });

  it('groph option should work', async () => {
    const graphName = 'Testing';
    const result = createGraphOption(graphName);
    expect(result['plugins']['title']['text']).toEqual(graphName);
  });
});
