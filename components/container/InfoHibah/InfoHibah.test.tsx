import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import HibahPage from '.';
import { fireEvent, screen } from '@testing-library/react';
import { act } from 'react-test-renderer';
import { act as actUtils } from 'react-dom/test-utils';

jest.mock('next/dist/client/router', () => require('next-router-mock'));

describe('Info Hibah Container', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });

    act(() => {
      renderWithTheme(<HibahPage />);
    });
  });

  const isInfoHibahLoading = () => {
    expect(screen.queryAllByTestId('skeleton')).toBeVisible;
    expect(screen.queryAllByTestId('list-info-hibah')).not.toBeVisible;
    expect(screen.queryAllByText('terdapat kesalahan')).not.toBeVisible;
  };

  const loadInfoHibahSuccess = () => {
    expect(screen.queryAllByTestId('list-info-hibah')).toBeVisible;
    expect(screen.queryAllByTestId('skeleton')).not.toBeVisible;
    expect(screen.queryAllByText('terdapat kesalahan')).not.toBeVisible;
  };

  it('Success on next page clicked', async () => {
    await actUtils(() => sleep(100));

    const NextBtn = screen.getByText(/Selanjutnya/);
    actUtils(() => {
      fireEvent.click(NextBtn);
    });

    expect(screen.queryAllByTestId('skeleton')).toBeVisible;

    await actUtils(() => sleep(100));

    expect(screen.queryAllByTestId('skeleton')).not.toBeVisible;
  });

  it('Success get info hibah list', async () => {
    await actUtils(() => sleep(100));

    isInfoHibahLoading();

    await actUtils(() => sleep(100));

    loadInfoHibahSuccess;
  });

  it('Success get info hibah list by newest updates', async () => {
    await actUtils(() => sleep(100));

    const BtnTerbaru = screen.getByTestId('btn-terbaru');

    actUtils(() => {
      fireEvent.click(BtnTerbaru);
    });

    isInfoHibahLoading;

    await actUtils(() => sleep(100));

    loadInfoHibahSuccess;
  });

  it('Success get info hibah list by oldest updates', async () => {
    await actUtils(() => sleep(100));

    const BtnTerlawas = screen.getByTestId('btn-terlawas');
    actUtils(() => {
      fireEvent.click(BtnTerlawas);
    });

    isInfoHibahLoading;

    await actUtils(() => sleep(100));

    loadInfoHibahSuccess;
  });

  it('searchbar should work', async () => {
    await actUtils(() => sleep(100));

    const InputSearchBar = screen.getByTestId('searchbar');
    const resultPaten = screen.queryAllByText('tes1');

    fireEvent.click(InputSearchBar);
    fireEvent.change(InputSearchBar, { target: { value: 'tes1' } });
    fireEvent.submit(InputSearchBar);

    await actUtils(() => sleep(100));

    isInfoHibahLoading();

    await actUtils(() => sleep(100));

    loadInfoHibahSuccess();
    expect(resultPaten).toBeVisible;
  });

  it('pagination should work', async () => {
    await actUtils(() => sleep(100));

    const ReactPaginator = screen.getByTestId('react-paginator');
    const NextPageBtn = ReactPaginator.getElementsByTagName('a')[3];

    expect(NextPageBtn.text).toEqual('Selanjutnya');
    fireEvent.click(NextPageBtn);

    await actUtils(() => sleep(100));

    isInfoHibahLoading();

    await actUtils(() => sleep(100));

    loadInfoHibahSuccess();
    expect(screen.queryByText('tes page 2')).toBeVisible;
  });
});
