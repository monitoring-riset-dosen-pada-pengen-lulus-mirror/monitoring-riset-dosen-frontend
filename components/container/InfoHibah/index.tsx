import { Box, Heading, HStack } from '@chakra-ui/layout';
import { Flex, SkeletonCircle, SkeletonText } from '@chakra-ui/react';
import { BlueButton } from '@/components/button';
import { Searchbar } from '@/components/Searchbar';
import { useState } from 'react';
import CardInfoHibah from '@/components/common/CardInfoHibah';
import { range } from 'lodash';
import { useRouter } from 'next/router';
import { getAllUrlParams } from '@/utils/api/url';
import useFetchList from '@/hooks/useFetchList';
import services from '@/api/services';
import Paginator from '@/components/common/Paginator';
const { infoHibah } = services;

const InfoHibahPage = () => {
  const router = useRouter();

  const page = getAllUrlParams(router.asPath)['page'] || 1;

  const [params, setParams] = useState({ search: '', ordering: '', page });

  const { isError, isLoading, result } = useFetchList<Infohibah>(infoHibah.getAll(params), [params]);
  const { results } = result;

  const setBtnOrdering = (type: string) => () => {
    setParams({ ...params, ordering: type });
  };

  const handlePageChange = (event: { selected: number }) => {
    router.push(`${router.pathname}?page=${event.selected + 1}`);
    setParams({ ...params, page: event.selected + 1 });
  };

  const onSearchKeyword = async (query: string) => {
    router.push(`${router.pathname}?page=1`);
    setParams({ ...params, search: query, page: 1 });
  };

  return (
    <>
      <Heading variant="h4" padding="4" textAlign="center">
        Informasi Hibah
      </Heading>

      <Box maxW="800px" mx="auto">
        <Searchbar placeholder="Hibah apa yang anda cari?" width="full" onSubmit={onSearchKeyword} />
      </Box>

      <HStack justify="center" padding="4" spacing="24px">
        <BlueButton data-testid="btn-terbaru" onClick={setBtnOrdering('-tanggal_ubah')}>
          Terbaru
        </BlueButton>
        <BlueButton data-testid="btn-terlawas" onClick={setBtnOrdering('tanggal_ubah')}>
          Terlawas
        </BlueButton>
      </HStack>

      <Flex textAlign="left" padding="4" gap={8} w="full" maxW="1000px" flexDir="column" mx="auto" mb={10}>
        {isLoading ? (
          <Skeleton />
        ) : isError || !results.length ? (
          <ErrorState />
        ) : (
          <Flex gap={8} flexDir="column" data-testid="list-info-hibah">
            {results.map((item) => (
              <CardInfoHibah data={item} key={item.id} />
            ))}
          </Flex>
        )}
      </Flex>

      <Paginator pageCount={Math.ceil(result.count / 10)} onPageChange={handlePageChange} page={page} />
    </>
  );
};

export default InfoHibahPage;

export const ErrorState = () => {
  return (
    <Box textAlign="center" py={10} bg="gray.50" w="full">
      Data tidak ditemukan
    </Box>
  );
};

export const Skeleton = () => (
  <>
    {range(5).map((item) => (
      <Flex
        data-testid="skeleton"
        key={item}
        padding="16px"
        boxShadow="sm"
        bg="white"
        align="center"
        gap={4}
      >
        <SkeletonCircle minW="134px" size="134px" />
        <SkeletonText w="full" noOfLines={4} spacing="4" />
      </Flex>
    ))}
  </>
);
