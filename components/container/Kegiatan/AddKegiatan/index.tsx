import services from '@/api/services';
import { BlueButton } from '@/components/button';
import { Box, Flex, Heading, List, ListIcon, ListItem, Spinner, Text } from '@chakra-ui/react';
import { Token } from '@chakra-ui/styled-system/dist/declarations/src/utils/types';
import { useRef, useState } from 'react';
import { MdCheckCircle, MdOutlineAttachFile, MdUploadFile } from 'react-icons/md';
import * as CSS from 'csstype';
import { CheckCircleIcon, CloseIcon } from '@chakra-ui/icons';

type TUploadState = 'success' | 'error' | 'loading' | 'stale';

const AddKegiatan = () => {
  const hiddenFileInput = useRef<any>(null);
  const [uploadState, setUploadState] = useState<TUploadState>('stale');
  const [file, setFile] = useState<File>();

  const handleClick = () => {
    if (hiddenFileInput.current) {
      hiddenFileInput.current.click();
    }
  };

  const mapUploadState: Record<TUploadState, Token<CSS.Property.Color, 'colors'>> = {
    error: 'red.50',
    loading: 'white.50',
    success: 'green.50',
    stale: 'gray.50',
  };

  const handleUploadFile = async (file: File | undefined) => {
    if (file) {
      const formData = new FormData();
      formData.append('uploaded_file', file, file.name);

      setUploadState('loading');
      try {
        await services.hibah.postKegiatanExcel(formData);
        setUploadState('success');
      } catch (error) {
        setUploadState('error');
      }
    }
  };

  const handleChange = (event: { target: { files: FileList | null } }) => {
    const fileUploaded = event.target.files?.[0];
    setUploadState('stale');

    setFile(fileUploaded);
  };

  return (
    <Box w="fit-content" mx="auto" p="16px" boxShadow="lg" borderRadius="8px">
      <Heading variant="h5" py="4" textAlign="center">
        Upload Data Kegiatan
      </Heading>
      <List color="gray.500" mb="36px">
        <ListItem>
          <ListIcon as={MdCheckCircle} color="green.500" />
          Berlaku untuk upload data Riset, Pengabdian Masyarakat, dan Hibah
        </ListItem>
        <ListItem>
          <ListIcon as={MdCheckCircle} color="green.500" />
          File yang di kirim harus memiliki format xlsx/xls
        </ListItem>
      </List>

      {file && (
        <Box textAlign="center" p="16px" mb="16px" bgColor={mapUploadState[uploadState]} borderRadius={8}>
          {uploadState == 'success' ? (
            <CheckCircleIcon boxSize={'50px'} color={'green.500'} mb={2} />
          ) : uploadState == 'error' ? (
            <Flex
              mx="auto"
              mb={2}
              flexDirection="column"
              justifyContent="center"
              alignItems="center"
              bg={'red.500'}
              rounded={'50px'}
              w={'50px'}
              h={'50px'}
              textAlign="center"
            >
              <CloseIcon boxSize={'20px'} color={'white'} />
            </Flex>
          ) : (
            uploadState == 'loading' && (
              <Spinner
                mb={2}
                thickness="4px"
                speed="0.65s"
                emptyColor="gray.200"
                color="blue.500"
                size="xl"
              />
            )
          )}
          <Text textAlign="center">{file.name}</Text>
        </Box>
      )}

      <Flex justify="center" gridGap="28px">
        <input
          ref={hiddenFileInput}
          type="file"
          accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
          style={{ display: 'none' }}
          onChange={handleChange}
        />

        <BlueButton
          isLoading={uploadState == 'loading'}
          onClick={handleClick}
          leftIcon={<MdOutlineAttachFile />}
        >
          Pilih Berkas
        </BlueButton>

        <BlueButton
          isDisabled={!file}
          isLoading={uploadState == 'loading'}
          onClick={() => handleUploadFile(file)}
          leftIcon={<MdUploadFile />}
        >
          Upload Berkas
        </BlueButton>
      </Flex>
    </Box>
  );
};

export default AddKegiatan;
