import {
  Box,
  Button,
  Flex,
  Heading,
  ListItem,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalOverlay,
  Skeleton,
  Spacer,
  StackDivider,
  Tag,
  Text,
  UnorderedList,
  useDisclosure,
  VStack,
} from '@chakra-ui/react';
import NextLink from 'next/link';
import { FilterBoxContainer } from '@/components/common/Product/styles';
import { MdArrowBack, MdDelete } from 'react-icons/md';
import { GrayButton, RedButton } from '@/components/button';
import { useRouter } from 'next/router';
import useFetchDetail from '@/hooks/useFetchDetail';
import { ErrorState } from '../../InfoHibah';
import services from '@/api/services';
import { SuccessToast, ErrorToast } from '@/components/Toast';
import { useState } from 'react';
import { useSelector } from 'react-redux';
const { pengmas } = services;

const DetailInfo = ({ title, data }: any) => {
  return (
    <Box my="2">
      <Text variant="small" fontWeight="bold" mb="4px">
        {title}
      </Text>
      <Box fontSize="md">{data ? data : '-'}</Box>
    </Box>
  );
};

const PengmasDetail = () => {
  const router = useRouter();
  const { id } = router.query as { id: string };
  const { isError, isLoading, result } = useFetchDetail<IHibah>(pengmas.getById(id), id);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [isDeleteLoading, setIsDeleteLoading] = useState(false);
  const is_admin = useSelector<DefaultRootState>((state) => state.auth.is_admin);

  const fetchDeletePengmas = (id: string) => async () => {
    if (id) {
      try {
        setIsDeleteLoading(true);
        await pengmas.deleteById(id);
        SuccessToast({ title: `berhasil menghapus pengabdian masyarakat "${judul_penelitian}"` });
        router.replace('/kegiatan/pengabdian-masyarakat');
      } catch (error) {
        ErrorToast({ title: `gagal menghapus pengabdian masyarakat "${judul_penelitian}"` });
      } finally {
        setIsDeleteLoading(false);
      }
    }
  };

  const {
    peneliti_utama,
    anggota_peneliti,
    lab,
    pemberi_dana,
    output,
    judul_penelitian,
    tahun,
    no_kontrak_hibah,
    no_nota_dinas,
    skema_pembiayaan,
    status_usulan,
    tipe_kegiatan,
    is_non_hibah,
    topik_penelitian,
    nilai_hibah_nasional,
    nilai_hibah_internasional,
  } = result as IHibah;

  const getProductAlias = (jenis: string) => {
    if (jenis == 'Paten') {
      return 'paten';
    } else {
      return 'hki';
    }
  };

  if (isError) {
    return (
      <span data-testid="detail-pengmas-error">
        <ErrorState />
      </span>
    );
  }

  return (
    <>
      <Flex justify="space-between">
        <GrayButton mb="4" leftIcon={<MdArrowBack />} onClick={() => router.back()} data-testid="back-btn">
          Kembali
        </GrayButton>
        {is_admin && (
          <RedButton onClick={onOpen} leftIcon={<MdDelete />} data-testid="btn-delete-pengmas">
            Hapus
          </RedButton>
        )}
      </Flex>

      <Modal isOpen={isOpen} onClose={onClose} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalBody>
            Anda yakin ingin menghapus Pengabdian Masyarakat dengan judul &quot;{judul_penelitian}&quot;?
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={onClose}>
              Tidak
            </Button>
            <Button
              data-testid="btn-confirm-delete-pengmas"
              variant="ghost"
              onClick={fetchDeletePengmas(id)}
              isLoading={isDeleteLoading}
            >
              Ya
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
      {isLoading ? (
        <Flex
          direction={{ base: 'column-reverse', md: 'row' }}
          gap="10"
          data-testid="detail-pengmas-skeleton"
        >
          <Skeleton minW="xs" borderRadius="10">
            <Box minW="xs" minH="md"></Box>
          </Skeleton>
          <Skeleton minW="4xl" borderRadius="10">
            <Box minH="md"></Box>
          </Skeleton>
        </Flex>
      ) : (
        <div data-testid="detail-pengmas-data">
          <Flex direction={{ base: 'column-reverse', md: 'row' }} gap="10">
            <FilterBoxContainer as="datalist" data-testid="filter-container" minW="xs">
              <Heading variant="h6">Informasi</Heading>
              <Box>
                <DetailInfo title="Tipe Kegiatan" data={tipe_kegiatan} />
                <DetailInfo title="Model Kegiatan" data={is_non_hibah ? 'Non Hibah' : 'Hibah'} />
                <DetailInfo title="Tahun" data={tahun} />
                <DetailInfo title="Peneliti Utama" data={peneliti_utama} />
                <DetailInfo title="Lab" data={lab?.name} />
                <DetailInfo
                  title="Anggota Peneliti"
                  data={
                    anggota_peneliti.length > 0 ? (
                      <UnorderedList>
                        {anggota_peneliti?.map((item: any) => (
                          <ListItem key={item}>{item}</ListItem>
                        ))}
                      </UnorderedList>
                    ) : (
                      <>-</>
                    )
                  }
                />
                <DetailInfo
                  title="Topik Penelitian"
                  data={
                    (topik_penelitian as ITopikPenelitian[])?.length > 0 ? (
                      <UnorderedList>
                        {topik_penelitian?.map((item: ITopikPenelitian) => (
                          <ListItem key={item.id}>{item.nama_topik}</ListItem>
                        ))}
                      </UnorderedList>
                    ) : (
                      <>-</>
                    )
                  }
                />
                {!is_non_hibah && (
                  <>
                    <DetailInfo title="Nilai Hibah Nasional" data={nilai_hibah_nasional} />
                    <DetailInfo title="Nilai Hibah Internasional" data={nilai_hibah_internasional} />
                    <DetailInfo title="No. Kontrak Hibah" data={no_kontrak_hibah} />
                    <DetailInfo title="No. Nota Dinas" data={no_nota_dinas} />
                    <DetailInfo title="Skema Pembiayaan" data={skema_pembiayaan} />
                    <DetailInfo title="Tipe Pendanaan" data={pemberi_dana?.tipe_pendanaan} />
                    <DetailInfo title="Pemberi Dana" data={pemberi_dana?.nama} />
                    <DetailInfo title="Status Usulan" data={status_usulan} />
                  </>
                )}
              </Box>
            </FilterBoxContainer>
            <Box width="100%">
              <Text fontSize="lg" data-testid="detail-pengmas-text">
                Kegiatan Pengabdian Masyarakat
              </Text>
              <Heading variant="h5" py="4" mb="2">
                {judul_penelitian}
              </Heading>
              <Text fontSize="2xl" fontWeight="semibold" mb="4">
                Output
              </Text>
              <VStack spacing="4" align="stretch" divider={<StackDivider borderColor="gray.200" />}>
                {(output as IOutput[])?.length > 0 ? (
                  output?.map((item: any) => (
                    <div key={item.id}>
                      <Flex>
                        <NextLink href={`/produk/${getProductAlias(item.jenis)}/${item.id}`}>
                          <Text color="blue.600" fontWeight="bold" fontSize="lg" cursor="pointer">
                            {item.judul_ciptaan}
                          </Text>
                        </NextLink>
                        <Spacer />
                        <Tag colorScheme="blue" borderRadius="full" variant="outline" size="sm">
                          {item.jenis}
                        </Tag>
                      </Flex>
                    </div>
                  ))
                ) : (
                  <Text>Belum ada output</Text>
                )}
              </VStack>
            </Box>
          </Flex>
        </div>
      )}
    </>
  );
};

export default PengmasDetail;
