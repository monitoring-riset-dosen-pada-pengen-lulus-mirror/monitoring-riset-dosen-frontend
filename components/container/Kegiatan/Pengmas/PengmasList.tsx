import KegiatanCard from '@/components/common/Kegiatan/KegiatanCard';
import Paginator from '@/components/common/Paginator';
import ProductCardSkeleton from '@/components/common/Product/ProductCardSkeleton';
import { UseFetchListHook } from '@/hooks/useFetchList';
import { Box, Flex } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { FC } from 'react';
import { UseFormReturn, useWatch } from 'react-hook-form';
import { PengmasFilterValues } from '.';
import { ErrorState } from '../../InfoHibah';

interface Props {
  data: UseFetchListHook<IHibah>;
  form: UseFormReturn<PengmasFilterValues, any>;
  handlePageChange: (event: { selected: number }) => void;
}

const PengmasList: FC<Props> = (props) => {
  const router = useRouter();
  const { data, form, handlePageChange } = props;

  const { control, watch } = form;
  const {
    isError,
    isLoading,
    result: { count, results: pengmas },
  } = data;

  const page = useWatch({ control, name: 'page' });

  const ListDataPengmas = () => {
    if (isLoading) {
      return <ProductCardSkeleton data-testid="pengmas-skeleton" />;
    }

    if (isError || !pengmas.length) {
      return (
        <span data-testid="pengmas-error">
          <ErrorState />
        </span>
      );
    }

    return (
      <Flex direction="column" gap={4} mb={8} data-testid="pengmas-list">
        {pengmas.map((item) => {
          const onClick = () => router.push(`/kegiatan/pengabdian-masyarakat/${item.id}`);
          return <KegiatanCard key={item.id} onClick={onClick} data={item} data-testid="pengmas-card" />;
        })}
      </Flex>
    );
  };

  return (
    <Box w="full">
      <ListDataPengmas />
      <Paginator
        pageCount={Math.ceil(count / watch().page_size)}
        onPageChange={handlePageChange}
        page={page}
      />
    </Box>
  );
};

export default PengmasList;
