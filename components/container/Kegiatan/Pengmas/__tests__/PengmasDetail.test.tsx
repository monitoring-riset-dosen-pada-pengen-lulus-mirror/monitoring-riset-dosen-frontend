import PengmasDetail from '@/components/container/Kegiatan/Pengmas/PengmasDetail';
import { screen } from '@testing-library/react';
import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import mockRouter from 'next-router-mock';
import 'next-router-mock/dynamic-routes';
import { act } from 'react-test-renderer';
import { act as actUtils } from 'react-dom/test-utils';
import { server } from '@/mocks/servers';
import { getPengmasDetailError } from '@/mocks/handlers/pengmas';

jest.mock('next/dist/client/router', () => require('next-router-mock'));
mockRouter.registerPaths(['/kegiatan/pengabdian-masyarakat/[id]']);
describe('Detail Pengmas', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });
  });

  const isDetailPengmasLoading = () => {
    const DetailPengmasSkeleton = screen.queryByTestId('detail-pengmas-skeleton');
    const DetailPengmasError = screen.queryByTestId('detail-pengmas-error');
    const DetailPengmasData = screen.queryByTestId('detail-pengmas-data');
    expect(DetailPengmasSkeleton).not.toEqual(null);
    expect(DetailPengmasError).toEqual(null);
    expect(DetailPengmasData).toEqual(null);
  };

  const loadDetailPengmasSuccess = () => {
    const DetailPengmasSkeleton = screen.queryByTestId('detail-pengmas-skeleton');
    const DetailPengmasError = screen.queryByTestId('detail-pengmas-error');
    const DetailPengmasData = screen.queryByTestId('detail-pengmas-data');
    expect(DetailPengmasSkeleton).toEqual(null);
    expect(DetailPengmasError).toEqual(null);
    expect(DetailPengmasData).not.toEqual(null);
  };

  const loadDetailPengmasFailed = () => {
    const DetailPengmasSkeleton = screen.queryByTestId('detail-pengmas-skeleton');
    const DetailPengmasError = screen.queryByTestId('detail-pengmas-error');
    const DetailPengmasData = screen.queryByTestId('detail-pengmas-data');
    expect(DetailPengmasSkeleton).toEqual(null);
    expect(DetailPengmasError).not.toEqual(null);
    expect(DetailPengmasData).toEqual(null);
  };

  it('successful renders Pengmas detail', async () => {
    mockRouter.setCurrentUrl('/kegiatan/pengabdian-masyarakat/1');
    act(() => {
      renderWithTheme(<PengmasDetail />);
    });

    isDetailPengmasLoading();

    await actUtils(() => sleep(100));

    loadDetailPengmasSuccess();
  });

  it('failed renders Pengmas detail', async () => {
    server.use(getPengmasDetailError);
    mockRouter.setCurrentUrl('/kegiatan/pengabdian-masyarakat/1');
    act(() => {
      renderWithTheme(<PengmasDetail />);
    });

    isDetailPengmasLoading();

    await actUtils(() => sleep(100));

    loadDetailPengmasFailed();
  });
});
