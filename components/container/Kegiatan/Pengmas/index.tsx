import services from '@/api/services';
import { BlueButton } from '@/components/button';
import Searchbar from '@/components/Searchbar';
import useFetchList from '@/hooks/useFetchList';
import { getAllUrlParams } from '@/utils/api/url';
import { Flex, Heading, HStack } from '@chakra-ui/react';
import { Item } from 'chakra-ui-autocomplete';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { MdAdd, MdAttachFile } from 'react-icons/md';
import { useSelector } from 'react-redux';
import PengmasFilterbox from './PengmasFilterbox';
import PengmasList from './PengmasList';
const { pengmas } = services;

export type PengmasFilterValues = {
  search: string;
  lab: string;
  tahun: string[];
  status_usulan: string;
  tipe_pendanaan: string;
  page: number;
  page_size: number;
  peneliti?: Item[];
};

export const initialPengmasFilter = {
  search: '',
  lab: '',
  tahun: [],
  peneliti: [],
  status_usulan: '',
  tipe_pendanaan: '',
  page: 1,
  page_size: 10,
};

const FIRST_PAGE = 1;
const Pengmas = () => {
  const router = useRouter();

  const page = getAllUrlParams(router.asPath)['page'] || FIRST_PAGE;

  const form = useForm<PengmasFilterValues>({ defaultValues: { ...initialPengmasFilter, page } });
  const { setValue, getValues, reset } = form;

  const [params, setParams] = useState<PengmasFilterValues>({ ...initialPengmasFilter, page });
  const is_admin = useSelector<DefaultRootState>((state) => state.auth.is_admin);
  const data = useFetchList<IHibah>(pengmas.getAll(params), [params]);

  const setPageParams = (page: number) => {
    router.push({ href: router.pathname, query: { page } });
    setValue('page', page);
  };

  const onSubmitFilter: SubmitHandler<PengmasFilterValues> = (params) => {
    setPageParams(1);
    setParams({ ...params, page: getValues().page });
  };

  const onClearFilter = () => {
    reset(initialPengmasFilter);
    setPageParams(1);
    setParams(getValues());
  };

  const onSearch = (search: string) => {
    setPageParams(FIRST_PAGE);
    setValue('search', search);
    setParams(getValues());
  };

  const onSearchChange = (search: string) => {
    setValue('search', search);
  };

  const handlePageChange = (event: { selected: number }) => {
    setPageParams(event.selected + 1);
    setParams(getValues());
  };

  return (
    <>
      <Flex justify="space-between" align="center">
        <Heading variant="h4" py="4">
          Daftar Pengabdian Masyarakat
        </Heading>
        {is_admin && (
          <HStack spacing="1rem">
            <BlueButton onClick={() => router.push('/kegiatan/add-kegiatan')} leftIcon={<MdAttachFile />}>
              Upload Data
            </BlueButton>
            <BlueButton
              onClick={() => router.push('/kegiatan/pengabdian-masyarakat/tambah')}
              leftIcon={<MdAdd />}
            >
              Tambah Data
            </BlueButton>
          </HStack>
        )}
      </Flex>
      <Searchbar
        placeholder="Cari Judul Kegiatan..."
        width="full"
        onChange={onSearchChange}
        onSubmit={onSearch}
      />
      <Flex mt="24px" gap={8} direction={{ base: 'column', md: 'row' }}>
        <div data-testid="box-filter">
          <PengmasFilterbox
            onSubmit={onSubmitFilter}
            onClear={onClearFilter}
            form={form}
            isLoading={data.isLoading}
          />
        </div>
        <PengmasList data={data} form={form} handlePageChange={handlePageChange} />
      </Flex>
    </>
  );
};

export default Pengmas;
