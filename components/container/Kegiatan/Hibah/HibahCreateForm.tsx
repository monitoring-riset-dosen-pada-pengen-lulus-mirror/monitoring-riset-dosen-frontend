import KegiatanFormset, { KegiatanForm } from '@/components/common/Kegiatan/KegiatanFormset';
import { RadioInput } from '@/components/form';
import { Box, Button, Heading } from '@chakra-ui/react';
import { useForm, useWatch } from 'react-hook-form';
import OPSI_TIPE_KEGIATAN from '@/constants/hibah/tipe_kegiatan.json';
import HibahFormset, { HibahForm } from '@/components/common/Kegiatan/HibahFormset';
import services from '@/api/services';
import { AxiosResponse } from 'axios';
import { ErrorToast, SuccessToast } from '@/components/Toast';
import { useEffect, useState } from 'react';

interface IHibahForm extends HibahForm, KegiatanForm {
  tipe_kegiatan: 'Riset' | 'Pengabdian Masyarakat';
}

export const hibahFormInitialState: IHibahForm = {
  tipe_kegiatan: 'Riset',
  tahun: { label: '', value: '' },
  judul_penelitian: '',
  lab: undefined,
  peneliti_utama: { label: '', value: '' },
  anggota_peneliti: [],
  topik_penelitian: [],
  keterangan: '',
  nilai_hibah_internasional: '',
  nilai_hibah_nasional: '',
  skema_pembiayaan: '',
  no_kontrak_hibah: '',
  pemberi_dana: { label: '', value: '' },
  status_usulan: { label: '', value: '' },
  no_nota_dinas: '',
};

const HibahCreateForm = () => {
  const {
    control,
    handleSubmit,
    register,
    reset,
    formState: { errors, isSubmitting },
  } = useForm<IHibahForm>({ shouldUnregister: true });

  const tipeKegiatanForm = useWatch({ control, name: 'tipe_kegiatan' });
  const [tipeKegiatan, setTipeKegiatan] = useState<'Riset' | 'Pengabdian Masyarakat'>(tipeKegiatanForm);
  useEffect(() => {
    setTipeKegiatan(tipeKegiatanForm);
  }, [tipeKegiatanForm]);

  const { riset, pengmas } = services;

  const onSubmit = (formData: IHibahForm) => {
    const {
      tahun,
      judul_penelitian,
      lab,
      peneliti_utama,
      anggota_peneliti,
      topik_penelitian,
      keterangan,
      nilai_hibah_internasional,
      nilai_hibah_nasional,
      skema_pembiayaan,
      no_kontrak_hibah,
      pemberi_dana,
      status_usulan,
      no_nota_dinas,
    } = formData;

    const hibahPayload = {
      tahun: tahun.value,
      judul_penelitian: judul_penelitian,
      lab: lab?.value || null,
      peneliti_utama: peneliti_utama.value,
      anggota_peneliti: anggota_peneliti?.map((el) => el.value) || [],
      topik_penelitian: topik_penelitian?.map((el) => el.value) || [],
      keterangan: keterangan || '',
      nilai_hibah_internasional: nilai_hibah_internasional || null,
      nilai_hibah_nasional: nilai_hibah_nasional || null,
      skema_pembiayaan: skema_pembiayaan || '',
      no_kontrak_hibah: no_kontrak_hibah || '',
      pemberi_dana: pemberi_dana?.value || null,
      status_usulan: status_usulan?.value || '',
      no_nota_dinas: no_nota_dinas || '',
    };

    let post: (payload: any) => Promise<AxiosResponse<any, any>>;

    if (tipeKegiatan === 'Riset') {
      post = riset.postRisetHibah;
    } else {
      post = pengmas.postPengmasHibah;
    }

    return new Promise<void>((resolve) => {
      setTimeout(() => {
        try {
          post(hibahPayload);
          SuccessToast({ title: 'Pengabdian Masyarakat berhasil ditambahkan' });
        } catch (err: any) {
          ErrorToast({
            title: 'Terdapat kesalahan ketika menambahkan Pengabdian Masyarakat',
            description: `${err.response.data.message}`,
          });
        }
        resolve();
        reset(hibahFormInitialState);
      }, 500);
    });
  };

  return (
    <Box
      w="90%"
      mx="auto"
      minH="90vh"
      background="white"
      boxShadow="0px 0px 50px rgba(187, 187, 187, 0.25);"
      borderRadius="25px"
      p="3.5rem"
    >
      <Heading variant="h4" py="4" textAlign="center">
        Tambah Hibah
      </Heading>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Box my="1rem">
          <RadioInput
            title="Tipe Kegiatan"
            direction="row"
            control={control}
            id="tipe_kegiatan"
            options={OPSI_TIPE_KEGIATAN}
            defaultValue="Riset"
          />
        </Box>
        <KegiatanFormset control={control} errors={errors} register={register} />
        <HibahFormset control={control} errors={errors} register={register} />
        <Button mt={6} mx="auto" d="block" colorScheme="blue" isLoading={isSubmitting} type="submit">
          Submit
        </Button>
      </form>
    </Box>
  );
};

export default HibahCreateForm;
