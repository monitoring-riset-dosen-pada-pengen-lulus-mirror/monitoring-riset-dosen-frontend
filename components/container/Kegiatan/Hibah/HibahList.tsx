import KegiatanCard from '@/components/common/Kegiatan/KegiatanCard';
import Paginator from '@/components/common/Paginator';
import ProductCardSkeleton from '@/components/common/Product/ProductCardSkeleton';
import { UseFetchListHook } from '@/hooks/useFetchList';
import { Box, Flex } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { FC } from 'react';
import { UseFormReturn, useWatch } from 'react-hook-form';
import { HibahFilterValues } from '.';
import { ErrorState } from '../../InfoHibah';

interface Props {
  data: UseFetchListHook<IHibah>;
  form: UseFormReturn<HibahFilterValues, any>;
  handlePageChange: (event: { selected: number }) => void;
}

const HibahList: FC<Props> = (props) => {
  const router = useRouter();
  const { data, form, handlePageChange } = props;

  const { control } = form;
  const {
    isError,
    isLoading,
    result: { count, results: hibah },
  } = data;

  const page = useWatch({ control, name: 'page' });

  const ListDataHibah = () => {
    if (isLoading) {
      return <ProductCardSkeleton data-testid="hibah-skeleton" />;
    }

    if (isError || !hibah.length) {
      return (
        <span data-testid="hibah-error">
          <ErrorState />
        </span>
      );
    }

    return (
      <Flex direction="column" gap={4} mb={8} data-testid="hibah-list">
        {hibah.map((item) => {
          const onClick = () => router.push(`/kegiatan/hibah/${item.id}`);
          return <KegiatanCard key={item.id} onClick={onClick} data={item} data-testid="hibah-card" />;
        })}
      </Flex>
    );
  };

  return (
    <Box w="full">
      <ListDataHibah />
      <Paginator pageCount={Math.ceil(count / 10)} onPageChange={handlePageChange} page={page} />
    </Box>
  );
};

export default HibahList;
