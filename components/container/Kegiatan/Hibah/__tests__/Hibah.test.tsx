import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import { fireEvent, screen } from '@testing-library/react';
import { act } from 'react-test-renderer';
import { act as actUtils } from 'react-dom/test-utils';
import Hibah from '..';

jest.mock('next/dist/client/router', () => require('next-router-mock'));
describe('Hibah', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });

    act(() => {
      renderWithTheme(<Hibah />);
    });
  });

  const HibahSkeleton = screen.queryByTestId('hibah-skeleton');
  const HibahError = screen.queryByTestId('hibah-error');
  const HibahList = screen.queryByTestId('hibah-list');

  const isHibahLoading = () => {
    expect(HibahSkeleton).toBeVisible;
    expect(HibahError).not.toBeVisible;
    expect(HibahList).not.toBeVisible;
  };

  const loadHibahSuccess = () => {
    expect(HibahSkeleton).not.toBeVisible;
    expect(HibahError).not.toBeVisible;
    expect(HibahList).toBeVisible;
  };

  it('should add new item in CUIAutoComplete tags', async () => {
    await actUtils(() => sleep(100));
    const CUIInput = screen.getByTestId('CUIAutoComplete');
    const InputDropdown = CUIInput.getElementsByTagName('input')[0];

    fireEvent.click(InputDropdown);

    const dropdownItem = CUIInput.getElementsByTagName('li')[0];
    fireEvent.click(dropdownItem);

    const selectedItem = CUIInput.getElementsByTagName('button')[0];

    expect(selectedItem).toBeVisible;
  });

  it('should search Hibah list', async () => {
    await actUtils(() => sleep(100));

    const ApplyFilterBtn = screen.getByTestId('apply-filter');
    fireEvent.click(ApplyFilterBtn);

    await actUtils(() => sleep(100));

    isHibahLoading();

    await actUtils(() => sleep(100));

    loadHibahSuccess();
  });

  it('searchbar should work', async () => {
    await actUtils(() => sleep(100));

    const InputSearchBar = screen.getByTestId('searchbar');
    const resultHKI = screen.queryByText('judul 2');

    fireEvent.click(InputSearchBar);
    fireEvent.change(InputSearchBar, { target: { value: 'judul 2' } });
    fireEvent.submit(InputSearchBar);

    await actUtils(() => sleep(100));

    isHibahLoading();

    await actUtils(() => sleep(100));

    loadHibahSuccess();
    expect(resultHKI).toBeVisible;
  });

  it('pagination should work', async () => {
    await actUtils(() => sleep(100));

    const ReactPaginator = screen.getByTestId('react-paginator');
    const NextPageBtn = ReactPaginator.getElementsByTagName('a')[3];

    expect(NextPageBtn.text).toEqual('Selanjutnya');
    fireEvent.click(NextPageBtn);

    await actUtils(() => sleep(100));

    isHibahLoading();

    await actUtils(() => sleep(100));

    loadHibahSuccess();
    expect(screen.queryByText('judul 11')).toBeVisible;
  });

  it('should clear Hibah filter', async () => {
    await actUtils(() => sleep(100));

    const CUIInput = screen.getByTestId('CUIAutoComplete');
    const InputDropdown = CUIInput.getElementsByTagName('input')[0];

    fireEvent.click(InputDropdown);

    const dropdownItem = CUIInput.getElementsByTagName('li')[0];
    fireEvent.click(dropdownItem);

    const ClearFilterBtn = screen.getByTestId('clear-filter');
    fireEvent.click(ClearFilterBtn);

    await actUtils(() => sleep(100));

    isHibahLoading();

    await actUtils(() => sleep(100));

    loadHibahSuccess();
  });
});
