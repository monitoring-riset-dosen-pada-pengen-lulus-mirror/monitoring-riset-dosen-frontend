import HibahDetail from '@/components/container/Kegiatan/Hibah/HibahDetail';
import { screen } from '@testing-library/react';
import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import mockRouter from 'next-router-mock';
import 'next-router-mock/dynamic-routes';
import { act } from 'react-test-renderer';
import { act as actUtils } from 'react-dom/test-utils';
import { server } from '@/mocks/servers';
import { getHibahDetailError } from '@/mocks/handlers/hibah';

jest.mock('next/dist/client/router', () => require('next-router-mock'));
mockRouter.registerPaths(['/kegiatan/hibah/[id]']);
describe('Detail Hibah', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });
  });

  const isDetailHibahLoading = () => {
    const DetailHibahSkeleton = screen.queryByTestId('detail-hibah-skeleton');
    const DetailHibahError = screen.queryByTestId('detail-hibah-error');
    const DetailHibahData = screen.queryByTestId('detail-hibah-data');
    expect(DetailHibahSkeleton).not.toEqual(null);
    expect(DetailHibahError).toEqual(null);
    expect(DetailHibahData).toEqual(null);
  };

  const loadDetailHibahSuccess = () => {
    const DetailHibahSkeleton = screen.queryByTestId('detail-hibah-skeleton');
    const DetailHibahError = screen.queryByTestId('detail-hibah-error');
    const DetailHibahData = screen.queryByTestId('detail-hibah-data');
    expect(DetailHibahSkeleton).toEqual(null);
    expect(DetailHibahError).toEqual(null);
    expect(DetailHibahData).not.toEqual(null);
  };

  const loadDetailHibahFailed = () => {
    const DetailHibahSkeleton = screen.queryByTestId('detail-hibah-skeleton');
    const DetailHibahError = screen.queryByTestId('detail-hibah-error');
    const DetailHibahData = screen.queryByTestId('detail-hibah-data');
    expect(DetailHibahSkeleton).toEqual(null);
    expect(DetailHibahError).not.toEqual(null);
    expect(DetailHibahData).toEqual(null);
  };

  it('succesful renders hibah detail', async () => {
    mockRouter.setCurrentUrl('/kegiatan/hibah/1');
    act(() => {
      renderWithTheme(<HibahDetail />);
    });

    isDetailHibahLoading();

    await actUtils(() => sleep(100));

    loadDetailHibahSuccess();
  });

  it('failed renders hibah detail', async () => {
    server.use(getHibahDetailError);
    mockRouter.setCurrentUrl('/kegiatan/hibah/1');
    act(() => {
      renderWithTheme(<HibahDetail />);
    });

    isDetailHibahLoading();

    await actUtils(() => sleep(100));

    loadDetailHibahFailed();
  });
});
