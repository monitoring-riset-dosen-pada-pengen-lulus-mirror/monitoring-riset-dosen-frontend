import { BlueButton } from '@/components/button';
import { RadioInput, SelectInput } from '@/components/form';
import {
  Box,
  BoxProps,
  Button,
  Heading,
  Portal,
  Slide,
  Text,
  useDisclosure,
  useMediaQuery,
} from '@chakra-ui/react';
import { FC, useEffect } from 'react';
import { SubmitHandler, UseFormReturn } from 'react-hook-form/dist/types';
import { HibahFilterValues } from '.';
import { CUIAutoComplete, Item } from 'chakra-ui-autocomplete';
import useListYears from '@/hooks/useListYears';
import { IoFilter } from 'react-icons/io5';
import {
  FilterBoxContainer,
  labelStyleProps,
  listStyleProps,
  tagStyleProps,
} from '@/components/common/Product/styles';
import useGetList from '@/hooks/useGetList';
import services from '@/api/services';

import JENIS_USULAN from '@/constants/hibah/jenis_usulan.json';
import TIPE_PENDANAAN from '@/constants/hibah/tipe_pendanaan.json';
import { useWatch } from 'react-hook-form';

const { hibah, dosen } = services;

interface Props {
  form: UseFormReturn<HibahFilterValues, any>;
  onSubmit: SubmitHandler<HibahFilterValues>;
  onClear: () => void;
  boxStyle?: BoxProps;
  isLoading: boolean;
}

const HibahFilterbox: FC<Props> = (props) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [mdDown] = useMediaQuery('(max-width: 768px)');

  useEffect(() => {
    if (!props.isLoading) {
      onClose();
    }
  }, [onClose, props.isLoading]);

  return (
    <>
      {mdDown ? (
        <>
          <BlueButton w="full" onClick={onOpen} leftIcon={<IoFilter />}>
            Filter
          </BlueButton>

          <Portal>
            {isOpen && (
              <Box
                onClick={onClose}
                zIndex="overlay"
                pos="fixed"
                left={0}
                top={0}
                h="100vh"
                w="100vw"
                bg="blackAlpha.600"
              />
            )}
            <Slide direction="bottom" in={isOpen} style={{ zIndex: 1400 }}>
              <Box bg="white">
                <FilterComponent boxStyle={{ maxW: 'full', border: 'none' }} {...props} />
              </Box>
            </Slide>
          </Portal>
        </>
      ) : (
        <FilterComponent {...props} />
      )}
    </>
  );
};

const FilterComponent: FC<Props> = (props) => {
  const { form, onSubmit, isLoading, boxStyle, onClear } = props;

  const {
    setValue,
    handleSubmit,
    control,
    register,
    formState: { isDirty },
  } = form;
  const formValue = useWatch({ control });
  const isFormChanged = isDirty || !!formValue.tahun?.length || !!formValue.peneliti?.length;

  const { result } = useListYears({ model: 'hibah', asItem: true });
  const { result: labs } = useGetList<ILab>(hibah.getLabs());
  const { result: listPeneliti } = useGetList<Item>(() => dosen.getAllOpsi(''));

  const convertLabToItem = labs.map((item) => ({ name: item.name, value: item.id as string }));

  const handleSelectedItemsChange = (selectedItems?: Item[]) => {
    if (selectedItems) {
      const listOfTahun = selectedItems.map((item) => item.value);
      setValue('tahun', listOfTahun);
    }
  };

  return (
    <FilterBoxContainer data-testid="filter-container" {...boxStyle} onSubmit={handleSubmit(onSubmit)}>
      <Heading variant="h6">Filter</Heading>
      <Box py="4">
        <Text variant="small" fontWeight="bold">
          Jenis Lab
        </Text>
        <SelectInput
          direction="column"
          register={register}
          placeholder="Pilih Jenis Lab..."
          id="lab"
          options={convertLabToItem}
          variant="outline"
        />
      </Box>
      <Box position="relative" maxW="full" data-testid="CUIAutoComplete">
        <CUIAutoComplete
          disableCreateItem
          label="Tahun"
          placeholder="Pilih tahun..."
          listStyleProps={listStyleProps}
          tagStyleProps={tagStyleProps}
          labelStyleProps={labelStyleProps}
          items={result as Item[]}
          selectedItems={formValue.tahun?.map((item) => ({ label: item, value: item }))}
          hideToggleButton
          onSelectedItemsChange={(changes) => handleSelectedItemsChange(changes.selectedItems)}
        />
      </Box>
      <Box position="relative" maxW="full" data-testid="CUIAutoComplete-penulis">
        <CUIAutoComplete
          disableCreateItem
          label="Peneliti"
          placeholder="Pilih Peneliti..."
          listStyleProps={listStyleProps}
          tagStyleProps={tagStyleProps}
          labelStyleProps={labelStyleProps}
          items={listPeneliti}
          selectedItems={formValue.peneliti as Item[]}
          hideToggleButton
          onSelectedItemsChange={(changes) => setValue('peneliti', changes.selectedItems)}
        />
      </Box>
      <Box pb="4">
        <Text variant="small" fontWeight="bold">
          Status Usulan
        </Text>
        <RadioInput direction="column" control={control} id="status_usulan" options={JENIS_USULAN} />
      </Box>
      <Box>
        <Text variant="small" fontWeight="bold">
          Tipe Pendanaan
        </Text>
        <RadioInput direction="column" control={control} id="tipe_pendanaan" options={TIPE_PENDANAAN} />
      </Box>
      <BlueButton data-testid="apply-filter" type="submit" isLoading={isLoading} mt="20px">
        Terapkan Filter
      </BlueButton>
      {isFormChanged && (
        <Button colorScheme="gray" onClick={onClear} data-testid="clear-filter">
          Hapus Filter
        </Button>
      )}
    </FilterBoxContainer>
  );
};

export default HibahFilterbox;
