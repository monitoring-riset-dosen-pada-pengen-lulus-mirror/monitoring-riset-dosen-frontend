import KegiatanCard from '@/components/common/Kegiatan/KegiatanCard';
import Paginator from '@/components/common/Paginator';
import ProductCardSkeleton from '@/components/common/Product/ProductCardSkeleton';
import { UseFetchListHook } from '@/hooks/useFetchList';
import { Box, Flex } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { FC } from 'react';
import { UseFormReturn, useWatch } from 'react-hook-form';
import { RisetFilterValues } from '.';
import { ErrorState } from '../../InfoHibah';

interface Props {
  data: UseFetchListHook<IHibah>;
  form: UseFormReturn<RisetFilterValues, any>;
  handlePageChange: (event: { selected: number }) => void;
}

const RisetList: FC<Props> = (props) => {
  const router = useRouter();
  const { data, form, handlePageChange } = props;

  const { control, watch } = form;
  const {
    isError,
    isLoading,
    result: { count, results: pengmas },
  } = data;

  const page = useWatch({ control, name: 'page' });

  const ListDataRiset = () => {
    if (isLoading) {
      return <ProductCardSkeleton data-testid="riset-skeleton" />;
    }

    if (isError || !pengmas.length) {
      return (
        <span data-testid="riset-error">
          <ErrorState />
        </span>
      );
    }

    return (
      <Flex direction="column" gap={4} mb={8} data-testid="riset-list">
        {pengmas.map((item) => {
          const onClick = () => router.push(`/kegiatan/riset/${item.id}`);
          return <KegiatanCard key={item.id} onClick={onClick} data={item} data-testid="riset-card" />;
        })}
      </Flex>
    );
  };

  return (
    <Box w="full">
      <ListDataRiset />
      <Paginator
        pageCount={Math.ceil(count / watch().page_size)}
        onPageChange={handlePageChange}
        page={page}
      />
    </Box>
  );
};

export default RisetList;
