import KegiatanFormset, { KegiatanForm } from '@/components/common/Kegiatan/KegiatanFormset';
import { RadioInput } from '@/components/form';
import { Box, Button, Heading } from '@chakra-ui/react';
import { useForm, useWatch } from 'react-hook-form';
import OPSI_IS_NON_HIBAH from '@/constants/riset/is_non_hibah.json';
import { useEffect, useState } from 'react';
import HibahFormset, { HibahForm } from '@/components/common/Kegiatan/HibahFormset';
import services from '@/api/services';
import { AxiosResponse } from 'axios';
import { ErrorToast, SuccessToast } from '@/components/Toast';

export interface IKegiatanForm extends KegiatanForm, HibahForm {
  is_non_hibah: string;
}

export const kegiatanFormInitialState: KegiatanForm = {
  tahun: { label: '', value: '' },
  judul_penelitian: '',
  lab: undefined,
  peneliti_utama: { label: '', value: '' },
  anggota_peneliti: [],
  topik_penelitian: [],
  keterangan: '',
};

const RisetCreateForm = () => {
  const {
    control,
    handleSubmit,
    register,
    reset,
    formState: { errors, isSubmitting },
  } = useForm<IKegiatanForm>({ shouldUnregister: true });

  const [isHibah, setHibah] = useState(false);
  const tipePendanaan = useWatch({ control, name: 'is_non_hibah' });
  useEffect(() => {
    setHibah(tipePendanaan == 'Hibah');
  }, [tipePendanaan]);

  const { riset } = services;

  const onSubmit = (formData: IKegiatanForm) => {
    const {
      tahun,
      judul_penelitian,
      lab,
      peneliti_utama,
      anggota_peneliti,
      topik_penelitian,
      keterangan,
      nilai_hibah_internasional,
      nilai_hibah_nasional,
      skema_pembiayaan,
      no_kontrak_hibah,
      pemberi_dana,
      status_usulan,
      no_nota_dinas,
    } = formData;

    const kegiatanPayload = {
      tahun: tahun.value,
      judul_penelitian: judul_penelitian,
      lab: lab?.value || null,
      peneliti_utama: peneliti_utama.value,
      anggota_peneliti: anggota_peneliti?.map((el) => el.value) || [],
      topik_penelitian: topik_penelitian?.map((el) => el.value) || [],
      keterangan: keterangan || '',
      is_non_hibah: tipePendanaan == 'Non Hibah' || tipePendanaan === undefined,
    };

    let post: (payload: any) => Promise<AxiosResponse<any, any>>;
    let payload: KegiatanPayload | HibahPayload;

    if (isHibah) {
      const hibahPayload = {
        nilai_hibah_internasional: nilai_hibah_internasional || null,
        nilai_hibah_nasional: nilai_hibah_nasional || null,
        skema_pembiayaan: skema_pembiayaan || '',
        no_kontrak_hibah: no_kontrak_hibah || '',
        pemberi_dana: pemberi_dana?.value || null,
        status_usulan: status_usulan?.value || '',
        no_nota_dinas: no_nota_dinas || '',
        ...kegiatanPayload,
      };
      post = riset.postRisetHibah;
      payload = { ...hibahPayload } as HibahPayload;
    } else {
      post = riset.postRisetNonHibah;
      payload = { ...kegiatanPayload } as KegiatanPayload;
    }

    return new Promise<void>((resolve) => {
      setTimeout(() => {
        try {
          post(payload);
          SuccessToast({ title: 'Riset berhasil ditambahkan' });
        } catch (err: any) {
          ErrorToast({
            title: 'Terdapat kesalahan ketika menambahkan Riset',
            description: `${err.response.data.message}`,
          });
        }
        resolve();
        reset(kegiatanFormInitialState);
      }, 500);
    });
  };

  return (
    <Box
      w="90%"
      mx="auto"
      minH="90vh"
      background="white"
      boxShadow="0px 0px 50px rgba(187, 187, 187, 0.25);"
      borderRadius="25px"
      p="3.5rem"
    >
      <Heading variant="h4" py="4" textAlign="center">
        Tambah Riset
      </Heading>
      <form onSubmit={handleSubmit(onSubmit)}>
        <KegiatanFormset control={control} errors={errors} register={register} />
        <Box my="1rem">
          <RadioInput
            title="Model Kegiatan"
            direction="row"
            control={control}
            id="is_non_hibah"
            options={OPSI_IS_NON_HIBAH}
            defaultValue="Non Hibah"
          />
        </Box>
        {isHibah && <HibahFormset control={control} errors={errors} register={register} />}
        <Button mt={6} mx="auto" d="block" colorScheme="blue" isLoading={isSubmitting} type="submit">
          Submit
        </Button>
      </form>
    </Box>
  );
};

export default RisetCreateForm;
