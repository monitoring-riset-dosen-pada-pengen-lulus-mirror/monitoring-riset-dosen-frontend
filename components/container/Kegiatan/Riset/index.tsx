import services from '@/api/services';
import { BlueButton } from '@/components/button';
import Searchbar from '@/components/Searchbar';
import useFetchList from '@/hooks/useFetchList';
import { isAdmin as isAdminSelector } from '@/redux/slices/auth/selector';
import { getAllUrlParams } from '@/utils/api/url';
import { Flex, Heading, HStack } from '@chakra-ui/react';
import { Item } from 'chakra-ui-autocomplete';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { MdAdd, MdAttachFile } from 'react-icons/md';
import { useSelector } from 'react-redux';
import RisetFilterbox from './RisetFilterbox';
import RisetList from './RisetList';
const { riset } = services;

export type RisetFilterValues = {
  search: string;
  lab: string;
  tahun: string[];
  status_usulan: string;
  tipe_pendanaan: string;
  page: number;
  page_size: number;
  peneliti?: Item[];
};

export const initialRisetFilter: RisetFilterValues = {
  search: '',
  lab: '',
  tahun: [],
  peneliti: [],
  status_usulan: '',
  tipe_pendanaan: '',
  page: 1,
  page_size: 10,
};

const FIRST_PAGE = 1;
const Riset = () => {
  const router = useRouter();
  const isAdmin = useSelector(isAdminSelector);

  const page = getAllUrlParams(router.asPath)['page'] || FIRST_PAGE;

  const form = useForm<RisetFilterValues>({ defaultValues: { ...initialRisetFilter, page } });
  const { setValue, getValues, reset } = form;

  const [params, setParams] = useState<RisetFilterValues>({ ...initialRisetFilter, page });
  const data = useFetchList<IHibah>(riset.getAll(params), [params]);

  const setPageParams = (page: number) => {
    router.push({ href: router.pathname, query: { page } });
    setValue('page', page);
  };

  const onSubmitFilter: SubmitHandler<RisetFilterValues> = (params) => {
    setPageParams(1);
    setParams({ ...params, page: getValues().page });
  };

  const onClearFilter = () => {
    reset(initialRisetFilter);
    setPageParams(1);
    setParams(getValues());
  };

  const onSearch = (search: string) => {
    setPageParams(FIRST_PAGE);
    setValue('search', search);
    setParams(getValues());
  };

  const onSearchChange = (search: string) => {
    setValue('search', search);
  };

  const handlePageChange = (event: { selected: number }) => {
    setPageParams(event.selected + 1);
    setParams(getValues());
  };

  return (
    <>
      <Flex justifyContent="space-between" alignItems="center">
        <Heading variant="h4" py="4">
          Daftar Riset
        </Heading>
        {isAdmin && (
          <HStack spacing="1rem">
            <BlueButton onClick={() => router.push('/kegiatan/add-kegiatan')} leftIcon={<MdAttachFile />}>
              Upload Data
            </BlueButton>
            <BlueButton onClick={() => router.push('/kegiatan/riset/tambah')} leftIcon={<MdAdd />}>
              Tambah Data
            </BlueButton>
          </HStack>
        )}
      </Flex>
      <Searchbar
        placeholder="Cari Judul Kegiatan..."
        width="full"
        onChange={onSearchChange}
        onSubmit={onSearch}
      />
      <Flex mt="24px" gap={8} direction={{ base: 'column', md: 'row' }}>
        <div data-testid="box-filter">
          <RisetFilterbox
            onSubmit={onSubmitFilter}
            onClear={onClearFilter}
            form={form}
            isLoading={data.isLoading}
          />
        </div>
        <RisetList data={data} form={form} handlePageChange={handlePageChange} />
      </Flex>
    </>
  );
};

export default Riset;
