import RisetDetail from '@/components/container/Kegiatan/Riset/RisetDetail';
import { screen } from '@testing-library/react';
import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import mockRouter from 'next-router-mock';
import 'next-router-mock/dynamic-routes';
import { act } from 'react-test-renderer';
import { act as actUtils } from 'react-dom/test-utils';
import { server } from '@/mocks/servers';
import { getRisetDetailError } from '@/mocks/handlers/riset';

jest.mock('next/dist/client/router', () => require('next-router-mock'));
mockRouter.registerPaths(['/kegiatan/riset/[id]']);
describe('Detail Riset', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });
  });

  const isDetailRisetLoading = () => {
    const DetailRisetSkeleton = screen.queryByTestId('detail-riset-skeleton');
    const DetailRisetError = screen.queryByTestId('detail-riset-error');
    const DetailRisetData = screen.queryByTestId('detail-riset-data');
    expect(DetailRisetSkeleton).not.toEqual(null);
    expect(DetailRisetError).toEqual(null);
    expect(DetailRisetData).toEqual(null);
  };

  const loadDetailRisetSuccess = () => {
    const DetailRisetSkeleton = screen.queryByTestId('detail-riset-skeleton');
    const DetailRisetError = screen.queryByTestId('detail-riset-error');
    const DetailRisetData = screen.queryByTestId('detail-riset-data');
    expect(DetailRisetSkeleton).toEqual(null);
    expect(DetailRisetError).toEqual(null);
    expect(DetailRisetData).not.toEqual(null);
  };

  const loadDetailRisetFailed = () => {
    const DetailRisetSkeleton = screen.queryByTestId('detail-riset-skeleton');
    const DetailRisetError = screen.queryByTestId('detail-riset-error');
    const DetailRisetData = screen.queryByTestId('detail-riset-data');
    expect(DetailRisetSkeleton).toEqual(null);
    expect(DetailRisetError).not.toEqual(null);
    expect(DetailRisetData).toEqual(null);
  };

  it('successful renders riset detail', async () => {
    mockRouter.setCurrentUrl('/kegiatan/riset/1');
    act(() => {
      renderWithTheme(<RisetDetail />);
    });

    isDetailRisetLoading();

    await actUtils(() => sleep(100));

    loadDetailRisetSuccess();
  });

  it('failed renders riset detail', async () => {
    server.use(getRisetDetailError);
    mockRouter.setCurrentUrl('/kegiatan/riset/1');
    act(() => {
      renderWithTheme(<RisetDetail />);
    });

    isDetailRisetLoading();

    await actUtils(() => sleep(100));

    loadDetailRisetFailed();
  });
});
