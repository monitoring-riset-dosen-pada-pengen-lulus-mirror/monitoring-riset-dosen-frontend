import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import { fireEvent, screen } from '@testing-library/react';
import { act } from 'react-test-renderer';
import { act as actUtils } from 'react-dom/test-utils';
import Riset from '..';

jest.mock('next/dist/client/router', () => require('next-router-mock'));
describe('Riset', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });

    act(() => {
      renderWithTheme(<Riset />);
    });
  });

  const RisetSkeleton = screen.queryByTestId('riset-skeleton');
  const RisetError = screen.queryByTestId('riset-error');
  const RisetList = screen.queryByTestId('riset-list');

  const isRisetLoading = () => {
    expect(RisetSkeleton).toBeVisible;
    expect(RisetError).not.toBeVisible;
    expect(RisetList).not.toBeVisible;
  };

  const loadRisetSuccess = () => {
    expect(RisetSkeleton).not.toBeVisible;
    expect(RisetError).not.toBeVisible;
    expect(RisetList).toBeVisible;
  };

  it('should add new item in CUIAutoComplete tags', async () => {
    await actUtils(() => sleep(100));
    const CUIInput = screen.getByTestId('CUIAutoComplete');
    const InputDropdown = CUIInput.getElementsByTagName('input')[0];

    fireEvent.click(InputDropdown);

    const dropdownItem = CUIInput.getElementsByTagName('li')[0];
    fireEvent.click(dropdownItem);

    const selectedItem = CUIInput.getElementsByTagName('button')[0];

    expect(selectedItem).toBeVisible;
  });

  it('should search Riset list', async () => {
    await actUtils(() => sleep(100));

    const ApplyFilterBtn = screen.getByTestId('apply-filter');
    fireEvent.click(ApplyFilterBtn);

    await actUtils(() => sleep(100));

    isRisetLoading();

    await actUtils(() => sleep(100));

    loadRisetSuccess();
  });

  it('searchbar should work', async () => {
    await actUtils(() => sleep(100));

    const InputSearchBar = screen.getByTestId('searchbar');
    const resultHKI = screen.queryByText('judul 2');

    fireEvent.click(InputSearchBar);
    fireEvent.change(InputSearchBar, { target: { value: 'judul 2' } });
    fireEvent.submit(InputSearchBar);

    await actUtils(() => sleep(100));

    isRisetLoading();

    await actUtils(() => sleep(100));

    loadRisetSuccess();
    expect(resultHKI).toBeVisible;
  });

  it('pagination should work', async () => {
    await actUtils(() => sleep(100));

    const ReactPaginator = screen.getByTestId('react-paginator');
    const NextPageBtn = ReactPaginator.getElementsByTagName('a')[3];

    expect(NextPageBtn.text).toEqual('Selanjutnya');
    fireEvent.click(NextPageBtn);

    await actUtils(() => sleep(100));

    isRisetLoading();

    await actUtils(() => sleep(100));

    loadRisetSuccess();
    expect(screen.queryByText('judul 11')).toBeVisible;
  });

  it('should clear Riset filter', async () => {
    await actUtils(() => sleep(100));

    const CUIInput = screen.getByTestId('CUIAutoComplete');
    const InputDropdown = CUIInput.getElementsByTagName('input')[0];

    fireEvent.click(InputDropdown);

    const dropdownItem = CUIInput.getElementsByTagName('li')[0];
    fireEvent.click(dropdownItem);

    const ClearFilterBtn = screen.getByTestId('clear-filter');
    fireEvent.click(ClearFilterBtn);

    await actUtils(() => sleep(100));

    isRisetLoading();

    await actUtils(() => sleep(100));

    loadRisetSuccess();
  });
});
