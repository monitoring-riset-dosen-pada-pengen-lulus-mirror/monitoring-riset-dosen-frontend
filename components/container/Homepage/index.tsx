import { Box } from '@chakra-ui/layout';
import Description from './Description';
import Hero from './Hero';

const Homepage = () => {
  return (
    <Box minH="100vh" textAlign="center">
      <Hero />
      <Description />
    </Box>
  );
};

export default Homepage;
