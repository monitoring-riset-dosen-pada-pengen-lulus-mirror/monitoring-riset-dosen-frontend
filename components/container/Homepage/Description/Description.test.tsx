import { renderWithTheme } from '@/utils/test-utils/wrapper';
import Description from '.';
import { screen } from '@testing-library/react';

describe('Landing page', () => {
  beforeEach(() => {
    renderWithTheme(<Description />);
  });

  it('renders a "Apa itu SIHIBAH?" text', () => {
    const title = screen.getByText('Apa itu SIHIBAH?');

    expect(title).toBeVisible;
  });

  it('renders the description of SIHIBAH', () => {
    const SSOLogin = screen.getByText(
      /merupakan sebuah sistem yang dapat membantu dalam memantau beban riset dan pengabdian masyarakat/,
    );

    expect(SSOLogin).toBeVisible;
  });
});
