import { Box, Flex, Heading, Text } from '@chakra-ui/react';

const Description = () => {
  return (
    <Box mt="150px">
      <Heading variant="h5" mb="48px">
        Apa itu SIHIBAH?
      </Heading>
      <Flex textAlign="left" flexDir={{ base: 'column', lg: 'row' }} gap="36px">
        <Box w="full">
          <img width="100%" height="auto" src="/landing/gedung-pacil.jpg" alt="gedung pacil" />
        </Box>
        <Box>
          <Text variant="2x-large" fontWeight={500} mb="12px">
            Sistem Informasi Hibah dan Pengabdian Masyarakat{' '}
          </Text>
          <Text variant="x-large">
            merupakan sebuah sistem yang dapat membantu dalam memantau beban riset dan pengabdian masyarakat
            yang sedang dijalankan oleh setiap Dosen di lingkungan Fasilkom UI.
          </Text>
        </Box>
      </Flex>
    </Box>
  );
};

export default Description;
