import { Heading, Text } from '@chakra-ui/layout';
import { Button } from '@chakra-ui/button';
import api from '@/api/services';
import { useSelector } from 'react-redux';
import { getAccount } from '@/redux/slices/auth/selector';

const Hero = () => {
  const { redirectToSSOLogin } = api.sso;
  const account = useSelector(getAccount);

  return (
    <>
      <Heading variant="h4">
        Sistem Informasi <br /> Hibah dan Pengabdian Masyarakat
      </Heading>
      <Text mb="50px" mt="30px" variant="x-large">
        Fakultas Ilmu Komputer <br /> Universitas Indonesia
      </Text>
      <Button
        h="fit-content"
        data-testid="login-btn"
        boxShadow="md"
        _hover={{
          boxShadow: 'lg',
        }}
        colorScheme="blue"
        onClick={() => redirectToSSOLogin()}
        visibility={!!account ? 'hidden' : 'visible'}
      >
        <img src="/icons/logo-ui.svg" alt="logo-ui" />
        <Text variant="large" color="white" ml="16px" fontWeight={700}>
          Masuk Dengan SSO
        </Text>
      </Button>
    </>
  );
};

export default Hero;
