import { renderWithTheme } from '@/utils/test-utils/wrapper';
import Hero from '.';
import { fireEvent, screen } from '@testing-library/react';
import makeStore from '@/redux/store';
import { setAccount } from '@/redux/slices/auth';
import { dummyAcc } from '@/components/common/Navbar/navigations/__tests__/Desktop.test';

describe('Landing page', () => {
  const store = makeStore();
  it('renders a Website Title', () => {
    renderWithTheme(<Hero />);
    const title = screen.getByText('Sistem Informasi Hibah dan Pengabdian Masyarakat');
    screen.getByRole('heading', { name: 'Sistem Informasi Hibah dan Pengabdian Masyarakat' });

    expect(title).toBeVisible;
  });

  it('renders a Login SSO UI Button', () => {
    renderWithTheme(<Hero />);
    const SSOLogin = screen.getByText('Masuk Dengan SSO');
    screen.getByRole('button', { name: 'logo-ui Masuk Dengan SSO' });

    expect(SSOLogin).toBeVisible;
  });

  it('login button should be visible & works', () => {
    renderWithTheme(<Hero />);
    const LoginBtn = screen.getByTestId('login-btn');
    fireEvent.click(LoginBtn);

    const message = screen.getByText('Masuk Dengan SSO');

    expect(message).toBeVisible;
  });

  it('login button should be hidden', () => {
    store.dispatch(setAccount(dummyAcc));
    renderWithTheme(<Hero />, { store });

    const LoginBtn = screen.queryByTestId('login-btn');

    expect(LoginBtn?.className).not.toContain('css-qdcu3w');
    expect(LoginBtn?.className).toContain('css-gbha2z');
  });
});
