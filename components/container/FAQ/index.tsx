import {
  Box,
  Heading,
  Text,
  List,
  ListIcon,
  ListItem,
  Center,
  HStack,
  Accordion,
  AccordionButton,
  AccordionItem,
  AccordionPanel,
} from '@chakra-ui/react';
import { AddIcon, MinusIcon, QuestionIcon } from '@chakra-ui/icons';
import { QandA } from './constants';
import faq from '@/constants/faq.json';

export const FAQ = () => {
  return (
    <>
      <Box minH="100vh" textAlign="center">
        <Heading variant="h4">FAQ</Heading>
        <Text mb="50px" mt="30px" variant="x-large">
          Pertanyaan yang Sering Ditanyakan Seputar <br /> Sistem Informasi Riset dan Pengabdian Masyarakat
        </Text>

        <Center>
          <List textAlign="left" width="75%">
            {faq.map((faqData, id) => (
              <FaqList key={id} {...faqData} />
            ))}
          </List>
        </Center>
      </Box>
    </>
  );
};

const FaqList = ({ question, answer }: QandA) => {
  return (
    <ListItem>
      <Accordion allowMultiple borderColor="white">
        <AccordionItem>
          {({ isExpanded }) => (
            <>
              <h2>
                <AccordionButton>
                  <Box flex="1" textAlign="left" alignItems="baseline">
                    <HStack>
                      <ListIcon as={QuestionIcon} />
                      <Heading as="h6" size="sm">
                        {question}
                      </Heading>
                    </HStack>
                  </Box>
                  {isExpanded ? <MinusIcon fontSize="12px" /> : <AddIcon fontSize="12px" />}
                </AccordionButton>
              </h2>

              <AccordionPanel pb={4}>
                <Text ml="36px">{answer}</Text>
              </AccordionPanel>
            </>
          )}
        </AccordionItem>
      </Accordion>
    </ListItem>
  );
};

export default FAQ;
