import { renderWithTheme } from '@/utils/test-utils/wrapper';
import FAQ from '.';
import { fireEvent, screen } from '@testing-library/react';

describe('FAQ page', () => {
  beforeEach(() => {
    renderWithTheme(<FAQ />);
  });

  it('renders a "FAQ" text', () => {
    const title = screen.getByText('FAQ');

    expect(title).toBeVisible;
  });

  it('renders a list of questions and answers accordion', () => {
    renderWithTheme(<FAQ />);

    const q1 = screen.getAllByText('Question 1');
    const q2 = screen.getAllByText('Question 2');
    const q3 = screen.getAllByText('Question 3');

    expect(q1).toBeVisible;
    expect(q2).toBeVisible;
    expect(q3).toBeVisible;
  });

  it('Opens the answers in accordion', () => {
    const q1 = screen.queryByText('Question 1') as HTMLButtonElement;
    const q2 = screen.queryByText('Question 2') as HTMLButtonElement;
    const q3 = screen.queryByText('Question 3') as HTMLButtonElement;

    fireEvent.click(q1);
    fireEvent.click(q2);
    fireEvent.click(q3);

    const a1 = screen.getAllByText('Answer 1.');
    const a2 = screen.getAllByText('Answer 2.');
    const a3 = screen.getAllByText('Answer 3.');

    expect(a1).toBeVisible;
    expect(a2).toBeVisible;
    expect(a3).toBeVisible;
  });
});
