import Paginator from '@/components/common/Paginator';
import ProductCard from '@/components/common/Product/ProductCard';
import ProductCardSkeleton from '@/components/common/Product/ProductCardSkeleton';
import { UseFetchListHook } from '@/hooks/useFetchList';
import { Box, Flex } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { FC } from 'react';
import { UseFormReturn, useWatch } from 'react-hook-form';
import { HKIFilterValues } from '.';
import { ErrorState } from '../../InfoHibah';

interface Props {
  data: UseFetchListHook<IHKI>;
  form: UseFormReturn<HKIFilterValues, any>;
  handlePageChange: (event: { selected: number }) => void;
}

const ListHKI: FC<Props> = (props) => {
  const router = useRouter();
  const { data, form, handlePageChange } = props;

  const { control, watch } = form;
  const {
    isError,
    isLoading,
    result: { count, results: hkis },
  } = data;

  const page = useWatch({ control, name: 'page' });

  const ListDataHKI = () => {
    if (isLoading) {
      return <ProductCardSkeleton data-testid="hki-skeleton" />;
    }

    if (isError || !hkis.length) {
      return (
        <span data-testid="hki-error">
          <ErrorState />
        </span>
      );
    }

    return (
      <Flex direction="column" gap={4} mb={8} data-testid="hki-list">
        {hkis.map((item) => {
          const data = { ...item, jenis: item.jenis_hki };
          const onClick = () => router.push(`/produk/hki/${item.id}`);
          return <ProductCard key={item.id} onClick={onClick} data={data} data-testid="hki-card" />;
        })}
      </Flex>
    );
  };

  return (
    <Box w="full">
      <ListDataHKI />
      <Paginator
        pageCount={Math.ceil(count / watch().page_size)}
        onPageChange={handlePageChange}
        page={page}
      />
    </Box>
  );
};

export default ListHKI;
