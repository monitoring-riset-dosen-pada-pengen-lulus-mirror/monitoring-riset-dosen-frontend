import DetailHKI from '@/components/container/Produk/HKI/DetailHKI';
import { screen } from '@testing-library/react';
import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import mockRouter from 'next-router-mock';
import 'next-router-mock/dynamic-routes';
import { act } from 'react-test-renderer';
import { act as actUtils } from 'react-dom/test-utils';
import { server } from '@/mocks/servers';
import { getHKIDetailError } from '@/mocks/handlers/hki';

jest.mock('next/dist/client/router', () => require('next-router-mock'));
mockRouter.registerPaths(['/produk/hki/[id]']);
describe('Detail HKI', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });
  });

  const isDetailHKILoading = () => {
    const DetailHKISkeleton = screen.queryByTestId('detail-hki-skeleton');
    const DetailHKIError = screen.queryByTestId('detail-hki-error');
    const DetailHKIData = screen.queryByTestId('detail-hki-data');
    expect(DetailHKISkeleton).not.toEqual(null);
    expect(DetailHKIError).toEqual(null);
    expect(DetailHKIData).toEqual(null);
  };

  const loadDetailHKISuccess = () => {
    const DetailHKISkeleton = screen.queryByTestId('detail-hki-skeleton');
    const DetailHKIError = screen.queryByTestId('detail-hki-error');
    const DetailHKIData = screen.queryByTestId('detail-hki-data');
    expect(DetailHKISkeleton).toEqual(null);
    expect(DetailHKIError).toEqual(null);
    expect(DetailHKIData).not.toEqual(null);
  };

  const loadDetailHKIFailed = () => {
    const DetailHKISkeleton = screen.queryByTestId('detail-hki-skeleton');
    const DetailHKIError = screen.queryByTestId('detail-hki-error');
    const DetailHKIData = screen.queryByTestId('detail-hki-data');
    expect(DetailHKISkeleton).toEqual(null);
    expect(DetailHKIError).not.toEqual(null);
    expect(DetailHKIData).toEqual(null);
  };

  it('successful renders HKI detail', async () => {
    mockRouter.setCurrentUrl('/produk/hki/1');
    act(() => {
      renderWithTheme(<DetailHKI />);
    });

    isDetailHKILoading();

    await actUtils(() => sleep(100));

    loadDetailHKISuccess();
  });

  it('failed renders HKI detail', async () => {
    server.use(getHKIDetailError);
    mockRouter.setCurrentUrl('/produk/hki/1');
    act(() => {
      renderWithTheme(<DetailHKI />);
    });

    isDetailHKILoading();

    await actUtils(() => sleep(100));

    loadDetailHKIFailed();
  });
});
