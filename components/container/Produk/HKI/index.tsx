import services from '@/api/services';
import Searchbar from '@/components/Searchbar';
import useFetchList from '@/hooks/useFetchList';
import { getAllUrlParams } from '@/utils/api/url';
import { Flex, Heading } from '@chakra-ui/react';
import { BlueButton } from '@/components/button';
import { useRouter } from 'next/router';
import { useState } from 'react';
import { MdAdd } from 'react-icons/md';
import { useSelector } from 'react-redux';
import { SubmitHandler, useForm } from 'react-hook-form';
import FilterBox from './FilterBox';
import ListHki from './ListHKI';
import { Item } from 'chakra-ui-autocomplete';
const { hki } = services;

export type HKIFilterValues = {
  search: string;
  jenis_hki: string;
  keterangan: string;
  tahun: string[];
  page: number;
  page_size: number;
  pencipta?: Item[];
};

export const initialHKIFilter = {
  search: '',
  jenis_hki: '',
  keterangan: '',
  tahun: [],
  page: 1,
  pencipta: [],
  page_size: 10,
};

const HKI = () => {
  const router = useRouter();

  const page = getAllUrlParams(router.asPath)['page'] || 1;

  const form = useForm<HKIFilterValues>({ defaultValues: { ...initialHKIFilter, page } });
  const { setValue, getValues, reset } = form;

  const [params, setParams] = useState<HKIFilterValues>({ ...initialHKIFilter, page });

  const is_admin = useSelector<DefaultRootState>((state) => state.auth.is_admin);

  const data = useFetchList<IHKI>(hki.getAll(params), [params]);

  const setPageParams = (page: number) => {
    router.push({ href: router.pathname, query: { page } });
    setValue('page', page);
  };

  const onSubmitFilter: SubmitHandler<HKIFilterValues> = (params) => {
    setPageParams(1);
    setParams({ ...params, page: getValues().page });
  };

  const onClearFilter = () => {
    reset(initialHKIFilter);
    setPageParams(1);
    setParams(getValues());
  };

  const onSearch = (search: string) => {
    setPageParams(1);
    setValue('search', search);
    setParams(getValues());
  };

  const onSearchChange = (search: string) => {
    setValue('search', search);
  };

  const handlePageChange = (event: { selected: number }) => {
    setPageParams(event.selected + 1);
    setParams(getValues());
  };

  return (
    <>
      <Flex justify="space-between" align="center">
        <Heading variant="h4" py="4">
          Daftar HKI
        </Heading>
        {is_admin && (
          <BlueButton onClick={() => router.push('/produk/add-produk')} leftIcon={<MdAdd />}>
            Upload Data
          </BlueButton>
        )}
      </Flex>
      <Searchbar placeholder="Cari Judul..." width="full" onChange={onSearchChange} onSubmit={onSearch} />
      <Flex mt="24px" gap={8} direction={{ base: 'column', md: 'row' }}>
        <div data-testid="box-filter">
          <FilterBox onSubmit={onSubmitFilter} onClear={onClearFilter} form={form} data={data} />
        </div>
        <ListHki data={data} form={form} handlePageChange={handlePageChange} />
      </Flex>
    </>
  );
};

export default HKI;
