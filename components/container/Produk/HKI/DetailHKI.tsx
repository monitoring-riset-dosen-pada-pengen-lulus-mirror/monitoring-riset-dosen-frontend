import {
  Button,
  Flex,
  Heading,
  Link,
  ListItem,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalOverlay,
  SkeletonText,
  StackDivider,
  Text,
  UnorderedList,
  useDisclosure,
  VStack,
} from '@chakra-ui/react';
import { ExternalLinkIcon } from '@chakra-ui/icons';
import { MdArrowBack, MdDelete } from 'react-icons/md';
import { GrayButton, RedButton } from '@/components/button';
import DetailItem from '@/components/common/DetailItem';
import { useRouter } from 'next/router';
import formatDate from '@/utils/formatDate/formatDate';
import useFetchDetail from '@/hooks/useFetchDetail';
import { ErrorState } from '../../InfoHibah';
import services from '@/api/services';
import { useState } from 'react';
import { ErrorToast, SuccessToast } from '@/components/Toast';
import { useSelector } from 'react-redux';
const { hki } = services;

const DetailHKI = () => {
  const router = useRouter();
  const { id } = router.query as { id: string };
  const { isError, isLoading, result } = useFetchDetail<IHKI>(hki.getById(id), id);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [isDeleteLoading, setIsDeleteLoading] = useState(false);
  const is_admin = useSelector<DefaultRootState>((state) => state.auth.is_admin);

  const fetchDeleteHki = (id: string) => async () => {
    if (id) {
      try {
        setIsDeleteLoading(true);
        await hki.deleteById(id);
        SuccessToast({ title: `berhasil menghapus hki "${judul_ciptaan}"` });
        router.replace('/produk/hki');
      } catch (error) {
        ErrorToast({ title: `gagal menghapus hki "${judul_ciptaan}"` });
      } finally {
        setIsDeleteLoading(false);
      }
    }
  };

  const { judul_ciptaan, jenis_hki, tahun, tanggal_permohonan, pencipta, keterangan, link } =
    result as IHKI;

  if (isError) {
    return (
      <span data-testid="detail-hki-error">
        <ErrorState />
      </span>
    );
  }

  return (
    <>
      <Flex justify="space-between">
        <GrayButton mb="4" leftIcon={<MdArrowBack />} onClick={() => router.back()} data-testid="back-btn">
          Kembali
        </GrayButton>
        {is_admin && (
          <RedButton onClick={onOpen} leftIcon={<MdDelete />} data-testid="btn-delete-hki">
            Hapus
          </RedButton>
        )}
      </Flex>

      <Modal isOpen={isOpen} onClose={onClose} isCentered>
        <ModalOverlay />
        <ModalContent>
          <ModalBody>Anda yakin ingin menghapus HKI dengan judul &quot;{judul_ciptaan}&quot;?</ModalBody>

          <ModalFooter>
            <Button colorScheme="blue" mr={3} onClick={onClose}>
              Tidak
            </Button>
            <Button
              data-testid="btn-confirm-delete-hki"
              variant="ghost"
              onClick={fetchDeleteHki(id)}
              isLoading={isDeleteLoading}
            >
              Ya
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>

      <Text fontSize="lg" mt="4">
        Produk HKI
      </Text>
      {isLoading ? (
        <SkeletonText mt="4" noOfLines={20} spacing="4" data-testid="detail-hki-skeleton" />
      ) : (
        <div data-testid="detail-hki-data">
          <Heading variant="h5" py="4" mb="2">
            {judul_ciptaan}
          </Heading>
          <VStack spacing="4" align="stretch" divider={<StackDivider borderColor="gray.200" />}>
            <DetailItem title="Jenis HKI" data={jenis_hki} />
            <DetailItem title="Tahun" data={tahun} />
            <DetailItem title="Tanggal Permohonan" data={formatDate(tanggal_permohonan as string)} />
            <DetailItem
              title="Pencipta"
              data={
                <UnorderedList>
                  {pencipta?.map((item: any) => (
                    <ListItem key={item} listStylePosition="inside">
                      {item}
                    </ListItem>
                  ))}
                </UnorderedList>
              }
            />
            <DetailItem
              title="Tautan"
              data={
                <Link href={link} isExternal>
                  {link} <ExternalLinkIcon mx="2px" />
                </Link>
              }
            />
            <DetailItem title="Keterangan" data={keterangan} />
          </VStack>
        </div>
      )}
    </>
  );
};

export default DetailHKI;
