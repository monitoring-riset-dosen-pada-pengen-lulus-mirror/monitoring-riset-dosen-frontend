import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import { fireEvent, screen } from '@testing-library/react';
import { act } from 'react-test-renderer';
import Paten from '..';
import { act as actUtils } from 'react-dom/test-utils';

jest.mock('next/dist/client/router', () => require('next-router-mock'));
describe('Paten', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });

    act(() => {
      renderWithTheme(<Paten />);
    });
  });

  const PatenSkeleton = screen.queryByTestId('paten-skeleton');
  const PatenError = screen.queryByTestId('paten-error');
  const PatenList = screen.queryByTestId('paten-list');

  const isPatenLoading = () => {
    expect(PatenSkeleton).toBeVisible;
    expect(PatenError).not.toBeVisible;
    expect(PatenList).not.toBeVisible;
  };

  const loadPatenSuccess = () => {
    expect(PatenSkeleton).not.toBeVisible;
    expect(PatenError).not.toBeVisible;
    expect(PatenList).toBeVisible;
  };

  it('should add new item in CUIAutoComplete tags', async () => {
    await actUtils(() => sleep(100));
    const CUIInput = screen.getByTestId('CUIAutoComplete');
    const InputDropdown = CUIInput.getElementsByTagName('input')[0];

    fireEvent.click(InputDropdown);

    const dropdownItem = CUIInput.getElementsByTagName('li')[0];
    fireEvent.click(dropdownItem);

    const selectedItem = CUIInput.getElementsByTagName('button')[0];

    expect(selectedItem).toBeVisible;
  });

  it('should search paten list', async () => {
    await actUtils(() => sleep(100));

    const ApplyFilterBtn = screen.getByTestId('apply-filter');
    fireEvent.click(ApplyFilterBtn);

    await actUtils(() => sleep(100));

    isPatenLoading();

    await actUtils(() => sleep(100));

    loadPatenSuccess();
  });

  it('searchbar should work', async () => {
    await actUtils(() => sleep(100));

    const InputSearchBar = screen.getByTestId('searchbar');
    const resultPaten = screen.queryByText('judul 2');

    fireEvent.click(InputSearchBar);
    fireEvent.change(InputSearchBar, { target: { value: 'judul 2' } });
    fireEvent.submit(InputSearchBar);

    await actUtils(() => sleep(100));

    isPatenLoading();

    await actUtils(() => sleep(100));

    loadPatenSuccess();
    expect(resultPaten).toBeVisible;
  });

  it('pagination should work', async () => {
    await actUtils(() => sleep(100));

    const ReactPaginator = screen.getByTestId('react-paginator');
    const NextPageBtn = ReactPaginator.getElementsByTagName('a')[3];

    expect(NextPageBtn.text).toEqual('Selanjutnya');
    fireEvent.click(NextPageBtn);

    await actUtils(() => sleep(100));

    isPatenLoading();

    await actUtils(() => sleep(100));

    loadPatenSuccess();
    expect(screen.queryByText('judul 11')).toBeVisible;
  });

  it('should clear Paten filter', async () => {
    await actUtils(() => sleep(100));

    const CUIInput = screen.getByTestId('CUIAutoComplete');
    const InputDropdown = CUIInput.getElementsByTagName('input')[0];

    fireEvent.click(InputDropdown);

    const dropdownItem = CUIInput.getElementsByTagName('li')[0];
    fireEvent.click(dropdownItem);

    const ClearFilterBtn = screen.getByTestId('clear-filter');
    fireEvent.click(ClearFilterBtn);

    await actUtils(() => sleep(100));

    isPatenLoading();

    await actUtils(() => sleep(100));

    loadPatenSuccess();
  });
});
