import DetailPaten from '@/components/container/Produk/Paten/DetailPaten';
import { screen } from '@testing-library/react';
import mockRouter from 'next-router-mock';
import 'next-router-mock/dynamic-routes';
import { renderWithTheme, sleep } from '@/utils/test-utils/wrapper';
import { server } from '@/mocks/servers';
import { getPatenDetailError } from '@/mocks/handlers/paten';
import { act } from 'react-test-renderer';
import { act as actUtils } from 'react-dom/test-utils';

jest.mock('next/dist/client/router', () => require('next-router-mock'));
mockRouter.registerPaths(['/produk/paten/[id]']);
describe('Detail Paten', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'matchMedia', {
      writable: true,
      value: jest.fn().mockImplementation((query) => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // Deprecated
        removeListener: jest.fn(), // Deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn(),
      })),
    });
  });

  const isDetailPatenLoading = () => {
    const DetailPatenSkeleton = screen.queryByTestId('detail-paten-skeleton');
    const DetailPatenError = screen.queryByTestId('detail-paten-error');
    const DetailPatenData = screen.queryByTestId('detail-paten-data');
    expect(DetailPatenSkeleton).not.toEqual(null);
    expect(DetailPatenError).toEqual(null);
    expect(DetailPatenData).toEqual(null);
  };

  const loadDetailPatenSuccess = () => {
    const DetailPatenSkeleton = screen.queryByTestId('detail-paten-skeleton');
    const DetailPatenError = screen.queryByTestId('detail-paten-error');
    const DetailPatenData = screen.queryByTestId('detail-paten-data');
    expect(DetailPatenSkeleton).toEqual(null);
    expect(DetailPatenError).toEqual(null);
    expect(DetailPatenData).not.toEqual(null);
  };

  const loadDetailPatenFailed = () => {
    const DetailPatenSkeleton = screen.queryByTestId('detail-paten-skeleton');
    const DetailPatenError = screen.queryByTestId('detail-paten-error');
    const DetailPatenData = screen.queryByTestId('detail-paten-data');
    expect(DetailPatenSkeleton).toEqual(null);
    expect(DetailPatenError).not.toEqual(null);
    expect(DetailPatenData).toEqual(null);
  };

  it('successful renders Paten detail', async () => {
    mockRouter.setCurrentUrl('/produk/paten/1');
    act(() => {
      renderWithTheme(<DetailPaten />);
    });
    isDetailPatenLoading();

    await actUtils(() => sleep(100));

    loadDetailPatenSuccess();
  });

  it('failed renders Paten detail', async () => {
    server.use(getPatenDetailError);
    mockRouter.setCurrentUrl('/produk/paten/1');
    act(() => {
      renderWithTheme(<DetailPaten />);
    });

    isDetailPatenLoading();

    await actUtils(() => sleep(100));

    loadDetailPatenFailed();
  });
});
