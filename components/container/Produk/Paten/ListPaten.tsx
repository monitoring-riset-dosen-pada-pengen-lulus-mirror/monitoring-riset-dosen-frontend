import Paginator from '@/components/common/Paginator';
import ProductCard from '@/components/common/Product/ProductCard';
import ProductCardSkeleton from '@/components/common/Product/ProductCardSkeleton';
import { UseFetchListHook } from '@/hooks/useFetchList';
import { Box, Flex } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { FC } from 'react';
import { UseFormReturn, useWatch } from 'react-hook-form';
import { PatenFilterValues } from '.';
import { ErrorState } from '../../InfoHibah';

interface Props {
  data: UseFetchListHook<IPaten>;
  form: UseFormReturn<PatenFilterValues, any>;
  handlePageChange: (event: { selected: number }) => void;
}

const ListPaten: FC<Props> = (props) => {
  const router = useRouter();
  const { data, form, handlePageChange } = props;

  const { control, watch } = form;
  const {
    isError,
    isLoading,
    result: { count, results: patens },
  } = data;

  const page = useWatch({ control, name: 'page' });

  const ListDataPaten = () => {
    if (isLoading) {
      return <ProductCardSkeleton data-testid="paten-skeleton" />;
    }

    if (isError || !patens.length) {
      return (
        <span data-testid="paten-error">
          <ErrorState />
        </span>
      );
    }

    return (
      <Flex direction="column" gap={4} mb={8} data-testid="paten-list">
        {patens.map((item) => {
          const data = { ...item, jenis: item.jenis_paten };
          const onClick = () => router.push(`/produk/paten/${item.id}`);
          return <ProductCard key={item.id} onClick={onClick} data={data} data-testid="paten-card" />;
        })}
      </Flex>
    );
  };

  return (
    <Box w="full">
      <ListDataPaten />
      <Paginator
        pageCount={Math.ceil(count / watch().page_size)}
        onPageChange={handlePageChange}
        page={page}
      />
    </Box>
  );
};

export default ListPaten;
