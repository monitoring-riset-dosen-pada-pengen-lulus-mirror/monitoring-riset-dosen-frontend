import { BlueButton } from '@/components/button';
import { RadioInput } from '@/components/form';
import {
  Box,
  BoxProps,
  Button,
  Heading,
  Portal,
  Slide,
  Text,
  useDisclosure,
  useMediaQuery,
} from '@chakra-ui/react';
import { FC, useEffect } from 'react';
import { SubmitHandler, UseFormReturn } from 'react-hook-form/dist/types';
import { PatenFilterValues } from '.';
import { CUIAutoComplete, Item } from 'chakra-ui-autocomplete';
import { UseFetchListHook } from '@/hooks/useFetchList';
import useListYears from '@/hooks/useListYears';
import { IoFilter } from 'react-icons/io5';
import jenisPaten from '@/constants/paten/jenis.json';
import keteranganPaten from '@/constants/paten/keterangan.json';
import {
  FilterBoxContainer,
  labelStyleProps,
  listStyleProps,
  tagStyleProps,
} from '@/components/common/Product/styles';
import useGetList from '@/hooks/useGetList';

import { useWatch } from 'react-hook-form';
import dosen from '@/api/services/dosen';

interface Props {
  form: UseFormReturn<PatenFilterValues, any>;
  onSubmit: SubmitHandler<PatenFilterValues>;
  onClear: () => void;
  data: UseFetchListHook<IPaten>;
  boxStyle?: BoxProps;
}

const FilterBox: FC<Props> = (props) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [mdDown] = useMediaQuery('(max-width: 768px)');

  useEffect(() => {
    if (!props.data.isLoading) {
      onClose();
    }
  }, [onClose, props.data.isLoading]);

  return (
    <>
      {mdDown ? (
        <>
          <BlueButton w="full" onClick={onOpen} leftIcon={<IoFilter />}>
            Filter
          </BlueButton>

          <Portal>
            {isOpen && (
              <Box
                onClick={onClose}
                zIndex="overlay"
                pos="fixed"
                left={0}
                top={0}
                h="100vh"
                w="100vw"
                bg="blackAlpha.600"
              />
            )}
            <Slide direction="bottom" in={isOpen} style={{ zIndex: 1400 }}>
              <Box bg="white">
                <FilterComponent boxStyle={{ maxW: 'full', border: 'none' }} {...props} />
              </Box>
            </Slide>
          </Portal>
        </>
      ) : (
        <FilterComponent {...props} />
      )}
    </>
  );
};

const FilterComponent: FC<Props> = (props) => {
  const {
    form,
    onSubmit,
    data: { isLoading },
    boxStyle,
    onClear,
  } = props;

  const {
    setValue,
    control,
    handleSubmit,
    formState: { isDirty },
  } = form;
  const formValue = useWatch({ control });
  const isFormChanged = isDirty || !!formValue.tahun?.length || !!formValue.pencipta?.length;

  const { result } = useListYears({ model: 'paten', asItem: true });
  const { result: listPencipta } = useGetList<Item>(() => dosen.getAllOpsi(''));

  const handleSelectedItemsChange = (selectedItems?: Item[]) => {
    if (selectedItems) {
      const listOfTahun = selectedItems.map((item) => item.value);
      setValue('tahun', listOfTahun);
    }
  };

  return (
    <FilterBoxContainer data-testid="filter-container" {...boxStyle} onSubmit={handleSubmit(onSubmit)}>
      <Heading variant="h6">Filter</Heading>
      <Box py="4">
        <Text variant="small" fontWeight="bold">
          Jenis
        </Text>
        <RadioInput direction="column" control={control} id="jenis_paten" options={jenisPaten} />
      </Box>
      <Box pb="4">
        <Text variant="small" fontWeight="bold">
          Status Keterangan
        </Text>
        <RadioInput direction="column" control={control} id="keterangan" options={keteranganPaten} />
      </Box>
      <Box position="relative" maxW="full" data-testid="CUIAutoComplete">
        <CUIAutoComplete
          disableCreateItem
          label="Tahun"
          placeholder="Pilih tahun..."
          listStyleProps={listStyleProps}
          tagStyleProps={tagStyleProps}
          labelStyleProps={labelStyleProps}
          items={result as Item[]}
          selectedItems={formValue.tahun?.map((item) => ({ label: item, value: item }))}
          hideToggleButton
          onSelectedItemsChange={(changes) => handleSelectedItemsChange(changes.selectedItems)}
        />
      </Box>
      <Box position="relative" maxW="full" data-testid="CUIAutoComplete-penulis">
        <CUIAutoComplete
          disableCreateItem
          label="Pencipta"
          placeholder="Pilih Pencipta..."
          listStyleProps={listStyleProps}
          tagStyleProps={tagStyleProps}
          labelStyleProps={labelStyleProps}
          items={listPencipta}
          selectedItems={formValue.pencipta as Item[]}
          hideToggleButton
          onSelectedItemsChange={(changes) => setValue('pencipta', changes.selectedItems)}
        />
      </Box>
      <BlueButton data-testid="apply-filter" type="submit" isLoading={isLoading} mt="20px">
        Terapkan Filter
      </BlueButton>
      {isFormChanged && (
        <Button colorScheme="gray" onClick={onClear} data-testid="clear-filter">
          Hapus Filter
        </Button>
      )}
    </FilterBoxContainer>
  );
};

export default FilterBox;
