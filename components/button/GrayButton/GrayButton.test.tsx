import { screen } from '@testing-library/react';
import { renderWithTheme } from '@/utils/test-utils/wrapper';
import { GrayButton } from '.';

describe('Gray Button', () => {
  beforeEach(() => {
    renderWithTheme(<GrayButton>Gray</GrayButton>);
  });

  it('renders a button', () => {
    const button = screen.getByRole('button');
    expect(button).toBeVisible;
  });

  it('renders a button with Gray text', () => {
    screen.getByText('Gray', { selector: 'button' });
  });
});
