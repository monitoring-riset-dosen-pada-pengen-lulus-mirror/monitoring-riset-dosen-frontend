import { Button } from '@chakra-ui/button';
import { ButtonProps } from '@chakra-ui/react';
import { FC } from 'react';

export const GrayButton: FC<ButtonProps> = ({ children, ...props }) => (
  <Button
    colorScheme="gray"
    color="black"
    transition=".5s filter"
    _hover={{ filter: 'brightness(0.9)' }}
    {...props}
  >
    {children}
  </Button>
);
