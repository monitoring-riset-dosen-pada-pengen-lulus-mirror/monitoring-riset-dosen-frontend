import { screen } from '@testing-library/react';
import { renderWithTheme } from '@/utils/test-utils/wrapper';
import { RedButton } from '.';

describe('Red Button', () => {
  beforeEach(() => {
    renderWithTheme(<RedButton>Red</RedButton>);
  });

  it('renders a button', () => {
    const button = screen.getByRole('button');
    expect(button).toBeVisible;
  });

  it('renders a button with Red text', () => {
    screen.getByText('Red', { selector: 'button' });
  });
});
