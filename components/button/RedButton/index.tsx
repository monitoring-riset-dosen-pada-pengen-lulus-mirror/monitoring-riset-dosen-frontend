import { Button } from '@chakra-ui/button';
import { ButtonProps } from '@chakra-ui/react';
import { FC } from 'react';

export const RedButton: FC<ButtonProps> = ({ children, ...props }) => (
  <Button
    colorScheme="red"
    color="white"
    transition=".5s filter"
    _hover={{ filter: 'brightness(0.9) contrast(1.25)' }}
    {...props}
  >
    {children}
  </Button>
);
