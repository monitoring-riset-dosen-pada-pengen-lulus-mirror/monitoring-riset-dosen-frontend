import { screen } from '@testing-library/react';
import { renderWithTheme } from '@/utils/test-utils/wrapper';
import { BlueButton } from '.';

describe('Blue Button', () => {
  beforeEach(() => {
    renderWithTheme(<BlueButton>Blue</BlueButton>);
  });

  it('renders a button', () => {
    const button = screen.getByRole('button');
    expect(button).toBeVisible;
  });

  it('renders a button with Blue text', () => {
    screen.getByText('Blue', { selector: 'button' });
  });
});
