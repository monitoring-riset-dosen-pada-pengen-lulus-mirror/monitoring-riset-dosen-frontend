import services from '@/api/services';
import {
  AsyncMultiSelectInput,
  AsyncSearchableSelectInput,
  MultiSelectInput,
  SearchableSelectInput,
  TextareaInput,
  TextInput,
} from '@/components/form';
import { ISelectOption } from '@/components/form/form';
import useGetList from '@/hooks/useGetList';
import { getAllPossibleYearChoices } from '@/utils/date';
import { VStack } from '@chakra-ui/react';
import { Control, UseFormRegister } from 'react-hook-form';

export type KegiatanForm = {
  tahun: ISelectOption;
  judul_penelitian: string;
  lab?: ISelectOption;
  peneliti_utama: ISelectOption;
  anggota_peneliti?: ISelectOption[];
  topik_penelitian?: ISelectOption[];
  keterangan?: string;
};

interface IKegiatanFormsetProps {
  control: Control<any, any>;
  errors: any;
  register: UseFormRegister<any>;
}

const KegiatanFormset = (props: IKegiatanFormsetProps) => {
  const { control, errors, register } = props;
  const { lab, dosen, topikPenelitian } = services;
  const { result: opsiTopikPenelitian } = useGetList<ISelectOption>(topikPenelitian.getAllOpsi());
  const { result: opsiLab } = useGetList<ISelectOption>(lab.getAllOpsi());
  return (
    <>
      <VStack spacing="1rem">
        <SearchableSelectInput
          id="tahun"
          options={getAllPossibleYearChoices().reverse()}
          title="Tahun"
          errors={errors}
          control={control}
          placeholder="Pilih tahun"
          rules={{
            required: 'Masukkan tahun terlebih dahulu',
          }}
        />
        <TextInput
          id="judul_penelitian"
          title="Judul Penelitian"
          errors={errors}
          register={register}
          rules={{
            required: 'Masukkan judul penelitian terlebih dahulu',
          }}
        />
        <SearchableSelectInput<ISelectOption>
          id="lab"
          options={opsiLab}
          title="Lab"
          errors={errors}
          control={control}
          placeholder="Pilih lab"
        />
        <AsyncSearchableSelectInput<ISelectOption>
          id="peneliti_utama"
          getListRequest={dosen.getAllOpsi}
          title="Peneliti Utama"
          errors={errors}
          control={control}
          placeholder="Ketikkan nama peneliti utama di sini"
          rules={{
            required: 'Pilih peneliti utama terlebih dahulu',
          }}
        />
        <AsyncMultiSelectInput<ISelectOption>
          id="anggota_peneliti"
          getListRequest={dosen.getAllOpsi}
          title="Anggota Peneliti"
          errors={errors}
          control={control}
          placeholder="Ketikkan nama-nama anggota peneliti di sini"
        />
        <MultiSelectInput<ISelectOption>
          id="topik_penelitian"
          options={opsiTopikPenelitian}
          title="Topik Penelitian"
          errors={errors}
          control={control}
          placeholder="Pilih topik penelitian"
        />
        <TextareaInput id="keterangan" title="Keterangan" errors={errors} register={register} />
      </VStack>
    </>
  );
};

export default KegiatanFormset;
