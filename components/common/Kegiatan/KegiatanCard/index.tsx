import { CalendarIcon } from '@chakra-ui/icons';
import { Avatar, Flex, Tag, TagLabel, TagLeftIcon, Text, Tooltip } from '@chakra-ui/react';
import { FC } from 'react';
import { FiTag } from 'react-icons/fi';
import { FaWallet } from 'react-icons/fa';
import { MdOutlineAttachMoney, MdPeople, MdScience } from 'react-icons/md';
import { ProductCardContainer } from '../../Product/styles';

export interface KegiatanCardProps {
  onClick?: () => void;
  data: IHibah;
}

const mapTipePendanaanToColor: Record<string, string> = {
  true: 'red',
  false: 'orange',
};

const KegiatanCard: FC<KegiatanCardProps> = (props) => {
  const {
    data: {
      anggota_peneliti,
      judul_penelitian,
      lab,
      pemberi_dana,
      tahun,
      status_usulan,
      peneliti_utama,
      is_non_hibah,
    },
    ...rest
  } = props;
  return (
    <ProductCardContainer {...rest}>
      <Tooltip label={judul_penelitian} placement="bottom" openDelay={500} gutter={0}>
        <Text variant="2x-large" fontWeight="bold" color="blue.500" mb="2px" noOfLines={1} w="fit-content">
          {judul_penelitian}
        </Text>
      </Tooltip>
      <Flex pr={2} mb={2} align="center" gap={1} borderRadius="full" bg="blackAlpha.50" w="fit-content">
        <Avatar size="xs" name={peneliti_utama || 'unknown'} />
        <Text variant="medium" noOfLines={1} textTransform="capitalize">
          {(peneliti_utama || 'unknown').toLowerCase()}
        </Text>
      </Flex>
      <Flex color="gray.500" gap="8px" mb={2} align="center">
        <Tag size="sm" colorScheme="blackAlpha">
          <TagLeftIcon as={CalendarIcon} />
          <TagLabel>{tahun}</TagLabel>
        </Tag>
        <Tag size="sm" colorScheme={mapTipePendanaanToColor[(is_non_hibah as boolean)?.toString()]}>
          <TagLeftIcon as={FaWallet} />
          <TagLabel>{is_non_hibah ? 'Non Hibah' : 'Hibah'}</TagLabel>
        </Tag>
        {pemberi_dana && (
          <Tag size="sm" colorScheme="blue">
            <TagLeftIcon marginInlineEnd={0} as={MdOutlineAttachMoney} />
            <TagLabel>{pemberi_dana.nama}</TagLabel>
          </Tag>
        )}
        {lab && (
          <Tag size="sm" colorScheme="green" fontWeight="semibold">
            <TagLeftIcon marginInlineEnd={0} as={MdScience} />
            <TagLabel>{lab.name}</TagLabel>
          </Tag>
        )}
        {status_usulan && (
          <Tag size="sm" colorScheme="yellow" fontWeight="semibold">
            <TagLeftIcon marginInlineEnd={1} as={FiTag} />
            <TagLabel>{status_usulan}</TagLabel>
          </Tag>
        )}
      </Flex>
      {!!anggota_peneliti.length && (
        <Flex align="center" gap={1} color="gray.500">
          <MdPeople />
          <Text variant="small" noOfLines={1} textTransform="capitalize">
            {anggota_peneliti.toString().replaceAll(',', ', ').toLowerCase()}
          </Text>
        </Flex>
      )}
    </ProductCardContainer>
  );
};

export default KegiatanCard;
