import services from '@/api/services';
import { SearchableSelectInput, NumberInput, TextInput } from '@/components/form';
import { ISelectOption } from '@/components/form/form';
import { VStack } from '@chakra-ui/react';
import { Control, UseFormRegister } from 'react-hook-form';

import OPSI_STATUS_USULAN from '@/constants/hibah/jenis_usulan_opsi.json';
import useGetList from '@/hooks/useGetList';

export type HibahForm = {
  nilai_hibah_internasional?: number | string;
  nilai_hibah_nasional?: number | string;
  skema_pembiayaan?: string;
  no_kontrak_hibah?: string;
  pemberi_dana?: ISelectOption;
  status_usulan?: ISelectOption;
  no_nota_dinas?: string;
};

interface IHibahFormsetProps {
  control: Control<any, any>;
  errors: any;
  register: UseFormRegister<any>;
}

const HibahFormset = (props: IHibahFormsetProps) => {
  const { control, errors, register } = props;
  const { pemberiDana } = services;
  const { result: opsiPemberiDana } = useGetList<ISelectOption>(pemberiDana.getAllOpsi());
  return (
    <>
      <VStack spacing="1rem">
        <NumberInput
          id="nilai_hibah_internasional"
          title="Nilai Hibah Internasional"
          errors={errors}
          control={control}
          helperText="Dalam format Rupiah (IDR)"
        />
        <NumberInput
          id="nilai_hibah_nasional"
          title="Nilai Hibah Nasional"
          errors={errors}
          control={control}
          helperText="Dalam format Rupiah (IDR)"
        />
        <TextInput
          id="skema_pembiayaan"
          title="Skema Pembiayaan"
          errors={errors}
          register={register}
          placeholder='Masukkan skema pembiayaan (misal: "PUTI Q3 2020")'
        />
        <TextInput
          id="no_kontrak_hibah"
          title="No. Kontrak Hibah"
          errors={errors}
          register={register}
          placeholder='Masukkan nomor kontrak hibah (misal: "3695/H2.R12/HKP.05.00/2005")'
        />
        <SearchableSelectInput<ISelectOption>
          id="pemberi_dana"
          options={opsiPemberiDana}
          title="Pemberi Dana"
          errors={errors}
          control={control}
          placeholder="Pilih pemberi dana"
        />
        <SearchableSelectInput<ISelectOption>
          id="status_usulan"
          options={OPSI_STATUS_USULAN}
          title="Status Usulan"
          errors={errors}
          control={control}
          placeholder="Pilih status usulan"
        />
        <TextInput
          id="no_nota_dinas"
          title="No. Nota Dinas"
          errors={errors}
          register={register}
          placeholder='Masukkan nomor nota dinas (misal: "ND-326/UN2.RST/PPM.00/2005")'
        />
      </VStack>
    </>
  );
};

export default HibahFormset;
