import {
  Image,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
  Flex,
  Box,
  Text,
  Stack,
  Link,
  Center,
} from '@chakra-ui/react';

import { ExternalLinkIcon } from '@chakra-ui/icons';
import { useRef, useState } from 'react';
import formatDate from '@/utils/formatDate/formatDate';

const OverlayDetailHibah = (props: {
  deskripsiHibah: string[];
  judulHibah: string;
  posterHibah: string;
  sumber: string;
  terakhirDiperbarui: string;
}) => {
  const [height, setHeight] = useState(0);
  const ref = useRef<HTMLDivElement>(document.createElement('div'));

  return (
    <ModalContent>
      <Flex>
        <Center
          ref={ref}
          flex={'1.5'}
          onLoad={() => {
            setHeight(ref.current.clientHeight);
          }}
          minHeight={'80vh'}
        >
          <Image
            fallbackSrc="https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg"
            src={props.posterHibah}
            alt={props.posterHibah}
            objectFit={'contain'}
          />
        </Center>
        <Box flex="1" overflow={'auto'} maxHeight={height}>
          <ModalHeader marginTop="2">{props.judulHibah}</ModalHeader>
          <ModalCloseButton />
          <ModalBody marginBottom="2">
            <Stack spacing={3}>
              {props.deskripsiHibah.map((paragraph: string) => (
                <Text fontSize="md" key={paragraph}>
                  {paragraph}
                </Text>
              ))}
              <Text fontSize="md">
                Sumber:{' '}
                {props.sumber ? (
                  <Link color={'blue.400'} href={props.sumber} isExternal>
                    {props.sumber}
                    <ExternalLinkIcon mx="2px" />
                  </Link>
                ) : (
                  '-'
                )}
              </Text>
              <Text fontSize="sm" color={'gray.600'}>
                Terakhir diperbarui: {formatDate(props.terakhirDiperbarui)}
              </Text>
            </Stack>
          </ModalBody>
        </Box>
      </Flex>
    </ModalContent>
  );
};
export default OverlayDetailHibah;
