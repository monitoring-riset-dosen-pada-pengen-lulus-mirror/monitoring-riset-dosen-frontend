import OverlayDetailHibah from './index';
import { fireEvent, render, screen } from '@testing-library/react';

import { Modal, ModalOverlay } from '@chakra-ui/react';
import formatDate from '@/utils/formatDate/formatDate';

describe('Overlay Detail Informasi Hibah', () => {
  const judulHibah = 'Testing123';
  const deskripsiHibah = 'Deskripsi123';
  const sumber = 'www.testing.com';
  const terakhirDiperbarui = '2022-03-22T04:12:21.344446Z';
  const posterHibahLink = 'https://www.testing.com';

  function testIfAllValueRendered(
    judul: string,
    sumber: string,
    deskripsi: string,
    poster: string,
    tanggal_ubah: string,
  ) {
    expect(screen.getByText(judul)).toBeVisible;
    expect(screen.getByText(sumber)).toBeVisible;
    expect(screen.getByText(deskripsi)).toBeVisible;
    expect(screen.getByAltText(poster)).toBeVisible;
    expect(screen.queryByText(formatDate(tanggal_ubah))).toBeVisible;
  }

  it('renders opened overlay detail Informasi Hibah' + ' with link and return just the way it is', () => {
    const { getByAltText } = render(
      <Modal onClose={() => null} size={'4xl'} isOpen={true}>
        <ModalOverlay />
        <OverlayDetailHibah
          judulHibah={judulHibah}
          deskripsiHibah={[deskripsiHibah]}
          sumber={sumber}
          terakhirDiperbarui={terakhirDiperbarui}
          posterHibah={posterHibahLink}
        />
      </Modal>,
    );
    fireEvent.load(getByAltText(posterHibahLink));
    testIfAllValueRendered(judulHibah, sumber, deskripsiHibah, posterHibahLink, terakhirDiperbarui);
  });

  it('renders overlay detail Informasi Hibah but still closed', () => {
    render(
      <Modal onClose={() => null} size={'4xl'} isOpen={false}>
        <ModalOverlay />
        <OverlayDetailHibah
          judulHibah={judulHibah}
          deskripsiHibah={[deskripsiHibah]}
          sumber={sumber}
          terakhirDiperbarui={terakhirDiperbarui}
          posterHibah={posterHibahLink}
        />
      </Modal>,
    );
    expect(screen.queryByText(judulHibah)).not.toBeVisible;
    expect(screen.queryByText(deskripsiHibah)).not.toBeVisible;
    expect(screen.queryByText(terakhirDiperbarui)).not.toBeVisible;
    expect(screen.queryByText(sumber)).not.toBeVisible;
    expect(screen.queryByAltText(posterHibahLink)).not.toBeVisible;
  });

  it('renders overlay detail Informasi Hibah' + ' without link and return dash sign', () => {
    const newSumber = '';
    const { getByAltText } = render(
      <Modal onClose={() => null} size={'4xl'} isOpen={true}>
        <ModalOverlay />
        <OverlayDetailHibah
          judulHibah={judulHibah}
          deskripsiHibah={[deskripsiHibah]}
          sumber={newSumber}
          terakhirDiperbarui={terakhirDiperbarui}
          posterHibah={posterHibahLink}
        />
      </Modal>,
    );
    fireEvent.load(getByAltText(posterHibahLink));
    testIfAllValueRendered(judulHibah, 'Sumber: -', deskripsiHibah, posterHibahLink, terakhirDiperbarui);
  });

  it('renders overlay detail Informasi Hibah' + ' without link and return no https:// in text', () => {
    const newSumber = '';
    const { getByAltText } = render(
      <Modal onClose={() => null} size={'4xl'} isOpen={true}>
        <ModalOverlay />
        <OverlayDetailHibah
          judulHibah={judulHibah}
          deskripsiHibah={[deskripsiHibah]}
          sumber={newSumber}
          terakhirDiperbarui={terakhirDiperbarui}
          posterHibah={posterHibahLink}
        />
      </Modal>,
    );
    fireEvent.load(getByAltText(posterHibahLink));
    expect(screen.queryByText('https://')).not.toBeVisible;
  });
});
