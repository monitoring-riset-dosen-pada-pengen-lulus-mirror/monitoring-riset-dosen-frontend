import CardInfoHibah from '@/components/common/CardInfoHibah/index';
import { fireEvent, render, screen } from '@testing-library/react';
import formatDate from '@/utils/formatDate/formatDate';

describe('Card Informasi Hibah', () => {
  const data: Infohibah = {
    judul: 'Testing123',
    sumber: 'www.testing.com',
    poster: 'https://www.testing.com',
    tanggal_ubah: '2022-03-22T04:12:21.344446Z',
    id: 1,
    deskripsi: 'Deskripsi123',
    tanggal_buat: '2022-03-21T04:12:21.344446Z',
  };

  function testIfAllValueRendered(data: Infohibah) {
    expect(screen.getByText(data.judul)).toBeVisible;
    expect(screen.getByText(data.sumber)).toBeVisible;
    expect(screen.getByText(data.deskripsi)).toBeVisible;
    expect(screen.getByAltText(data.poster)).toBeVisible;
    expect(screen.getByText(formatDate(data.tanggal_ubah))).toBeVisible;
  }

  it(
    'renders card informasi hibah ' +
      'with link have substring https://' +
      'and return the link without any extension',
    () => {
      data.sumber = 'https://www.testing.com';
      render(<CardInfoHibah data={data} />);
      testIfAllValueRendered(data);
    },
  );

  it(
    'renders card informasi hibah ' +
      'with link have substring http:// ' +
      'and return the link without any extension',
    () => {
      data.sumber = 'http://www.testing.com';
      render(<CardInfoHibah data={data} />);
      testIfAllValueRendered(data);
    },
  );

  it(
    'renders card informasi hibah ' +
      'without link have substring https:// or http:// ' +
      'and return the link with extension https://',
    () => {
      data.sumber = 'www.testing.com';
      render(<CardInfoHibah data={data} />);
      data.sumber = 'https://' + data.sumber;
      testIfAllValueRendered(data);
    },
  );

  it('renders card informasi hibah ' + 'with blank link value ' + 'and return dash sign', () => {
    data.sumber = '';
    render(<CardInfoHibah data={data} />);
    data.sumber = 'Sumber: -';
    testIfAllValueRendered(data);
  });

  it('renders card informasi hibah ' + 'with error image ' + 'and return the fallback image', () => {
    data.sumber = 'https://www.testing.com';
    data.poster = '';

    const { getByAltText } = render(<CardInfoHibah data={data} />);
    testIfAllValueRendered(data);
    fireEvent.error(getByAltText(data.poster));

    const fallbackImageSrc = 'https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg';
    expect(screen.getByRole('img')).toHaveAttribute('src', fallbackImageSrc);
  });
});
