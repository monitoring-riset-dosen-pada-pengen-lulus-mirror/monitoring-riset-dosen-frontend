import { Image, Box, Text, ModalOverlay, Modal, useDisclosure } from '@chakra-ui/react';

import formatDate from '@/utils/formatDate/formatDate';
import { Flex, Link } from '@chakra-ui/layout';
import OverlayDetailHibah from '@/components/common/OverlayDetailHibah';
import { FC } from 'react';
import { ExternalLinkIcon } from '@chakra-ui/icons';
import addUrlExtension from '@/utils/addUrlExtension';

interface Props {
  data: Infohibah;
}

const CardInfoHibah: FC<Props> = ({ data }) => {
  const { deskripsi, judul, poster, sumber, tanggal_ubah } = data;
  const { isOpen, onOpen, onClose } = useDisclosure();
  const deskripsiHibahParagraphs = deskripsi.split('\n');
  const fallbackImg = 'https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg';
  const sumberAsUrl = addUrlExtension(sumber);

  return (
    <>
      <Modal onClose={onClose} size={'4xl'} isOpen={isOpen}>
        <ModalOverlay />
        <OverlayDetailHibah
          judulHibah={judul}
          deskripsiHibah={deskripsiHibahParagraphs}
          sumber={sumber ? sumberAsUrl : ''}
          terakhirDiperbarui={tanggal_ubah}
          posterHibah={poster}
        />
      </Modal>

      <Flex
        onClick={onOpen}
        padding="4"
        cursor="pointer"
        _hover={{ shadow: 'lg' }}
        shadow="sm"
        w="full"
        borderRadius={8}
        align="center"
      >
        <Image
          onError={({ currentTarget }) => {
            currentTarget.onerror = null; // prevents looping
            currentTarget.src = fallbackImg;
          }}
          src={poster ? poster : fallbackImg}
          alt={poster}
          boxSize="150px"
          padding="4"
        />
        <Box w="calc(100% - 150px)">
          <Text fontSize="3xl" fontWeight="semibold">
            {judul}
          </Text>
          <Text fontSize="md" isTruncated>
            {deskripsi}
          </Text>
          <Text fontSize="md">
            Sumber:{' '}
            {sumber ? (
              <Link color={'blue.400'} href={sumberAsUrl} isExternal>
                {sumberAsUrl}
                <ExternalLinkIcon mx="2px" />
              </Link>
            ) : (
              '-'
            )}
          </Text>
          <Text fontSize="md" color="gray.600">
            {formatDate(tanggal_ubah)}
          </Text>
        </Box>
      </Flex>
    </>
  );
};
export default CardInfoHibah;
