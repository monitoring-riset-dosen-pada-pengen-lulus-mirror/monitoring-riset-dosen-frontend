import { Box, Text } from '@chakra-ui/layout';
import { StyledBigBanner } from './styles';

const BigBanner = () => {
  return (
    <StyledBigBanner>
      <Box w={{ base: '200px', md: 'inherit' }}>
        <img src="/navbar/logo-pacil.jpg" alt="pacil" />
      </Box>
      <Box py={{ base: '12px', md: '16px' }} textAlign={{ base: 'center', md: 'right' }}>
        <Text variant="x-large" fontWeight={600}>
          Sistem Informasi <br /> Hibah dan Pengabdian Masyarakat
        </Text>
        <Text variant="medium" color="#717171">
          Faculty of Computer Science Universitas Indonesia
        </Text>
      </Box>
    </StyledBigBanner>
  );
};

export default BigBanner;
