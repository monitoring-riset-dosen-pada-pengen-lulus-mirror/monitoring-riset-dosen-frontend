import { Flex, Box } from '@chakra-ui/layout';
import styled from '@emotion/styled';

export const NavbarContainer = styled(Flex)();
NavbarContainer.defaultProps = {
  bg: 'blue.700',
  position: 'sticky',
  top: '0',
  color: 'white',
  minH: '60px',
  align: 'center',
  py: { base: 2, md: '30px' },
  px: { base: 4, md: '96px' },
  boxShadow: 'xl',
  zIndex: 10,
};

export const StyledBigBanner = styled(Flex)();
StyledBigBanner.defaultProps = {
  px: { base: 4, md: '96px' },
  justify: 'space-between',
  align: 'center',
  flexDir: { base: 'column', md: 'row' },
};

export const BackDrop = styled(Box)();
BackDrop.defaultProps = {
  backdropFilter: 'auto',
  backdropBlur: 'sm',
  pos: 'absolute',
  bg: 'blackAlpha.500',
  w: '100vw',
  h: '100vh',
};
