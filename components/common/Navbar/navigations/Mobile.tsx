import { Link, Stack, Text } from '@chakra-ui/layout';
import { useDisclosure } from '@chakra-ui/react';
import { Collapse } from '@chakra-ui/transition';
import { NavItem, NAV_ITEMS, SECRET_NAV_ITEMS } from '.';
import NextLink from 'next/link';
import { FC } from 'react';
import { DownArrow, MobileNavText } from './styles';
import api from '@/api/services';
import { useSelector } from 'react-redux';
import { getAccount } from '@/redux/slices/auth/selector';

export const MobileNav = () => {
  const account = useSelector(getAccount);

  return (
    <Stack p={4} display={{ md: 'none' }} boxShadow="lg">
      {NAV_ITEMS.map((navItem) => {
        if (!account && SECRET_NAV_ITEMS.includes(navItem.label)) {
          return null;
        }
        return <MobileNavItem key={navItem.label} {...navItem} />;
      })}
    </Stack>
  );
};

const Wrap: FC<{ href?: string }> = ({ href, children }) => {
  if (!href) {
    return <div>{children}</div>;
  }
  return (
    <NextLink href={href} passHref>
      {children}
    </NextLink>
  );
};

const MobileNavItem = ({ label, children, href, testId }: NavItem) => {
  const { isOpen, onToggle } = useDisclosure();
  const { redirectToSSOLogout } = api.sso;
  const { logoutService } = api.auth;
  const logoutAction = () => {
    logoutService();
    redirectToSSOLogout();
  };

  return (
    <Stack spacing={4} onClick={children && onToggle}>
      <Wrap href={href}>
        <MobileNavText>
          <Text variant="medium" data-testid={`mobile-nav-${testId}`}>
            {label}
          </Text>
          {children && <DownArrow transform={isOpen ? 'rotate(180deg)' : ''} />}
        </MobileNavText>
      </Wrap>

      <Collapse in={isOpen} animateOpacity style={{ marginTop: '0!important' }}>
        <Stack mt={2} pl={4} borderLeft={1} borderStyle={'solid'} borderColor={'gray.200'} align={'start'}>
          {children &&
            children.map((child) => (
              <Text
                data-testid={`mobile-nav-${child.testId}`}
                variant="medium"
                as={Link}
                key={child.label}
                py={2}
                href={child.href}
                onClick={child.label === 'Logout' ? logoutAction : undefined}
              >
                {child.label}
              </Text>
            ))}
        </Stack>
      </Collapse>
    </Stack>
  );
};
