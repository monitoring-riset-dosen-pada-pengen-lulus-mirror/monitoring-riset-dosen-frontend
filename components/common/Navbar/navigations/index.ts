export interface NavItem {
  label: string;
  subLabel?: string;
  children?: Array<NavItem>;
  href?: string;
  testId?: string;
}

export { DesktopNav } from './Desktop';
export { MobileNav } from './Mobile';

/**
 * Untuk menghilangkan tombol dari navbar, masukan nama tombol tersebut di sini
 */
export const SECRET_NAV_ITEMS = ['Produk', 'Akun', 'Daftar Dosen', 'Kegiatan', 'Dashboard'];

export const NAV_ITEMS: Array<NavItem> = [
  {
    label: 'Beranda',
    href: '/',
    testId: 'beranda',
  },
  {
    label: 'Dashboard',
    href: '/dashboard',
    testId: 'dashboard',
  },
  {
    label: 'Daftar Dosen',
    href: '/dosen',
    testId: 'daftar-dosen',
  },
  {
    label: 'Kegiatan',
    testId: 'kegiatan',
    children: [
      {
        label: 'Riset',
        subLabel: 'Informasi Riset Fasilkom UI',
        href: '/kegiatan/riset',
        testId: 'riset',
      },
      {
        label: 'Pengabdian Masyarakat',
        subLabel: 'Informasi Pengmas Fasilkom UI',
        href: '/kegiatan/pengabdian-masyarakat',
        testId: 'pengmas',
      },
      {
        label: 'Hibah',
        subLabel: 'Informasi Hibah Fasilkom UI',
        href: '/kegiatan/hibah',
        testId: 'hibah',
      },
    ],
  },
  {
    label: 'Produk',
    testId: 'produk',
    children: [
      {
        label: 'Hak Kekayaan Intelektual',
        subLabel: 'Informasi Produk HKI Fasilkom UI',
        href: '/produk/hki',
        testId: 'hki',
      },
      {
        label: 'Paten',
        subLabel: 'Informasi Produk Paten Fasilkom UI',
        href: '/produk/paten',
        testId: 'paten',
      },
    ],
  },
  {
    label: 'Informasi Hibah',
    href: '/info-hibah',
    testId: 'hibah',
  },
  {
    label: 'FAQ',
    href: '/faq',
    testId: 'faq',
  },
  {
    label: 'Akun',
    testId: 'akun',
    children: [
      {
        label: 'Logout',
        href: '/',
        testId: 'logout',
      },
    ],
  },
];
