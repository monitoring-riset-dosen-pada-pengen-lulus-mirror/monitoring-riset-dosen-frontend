import { ChevronDownIcon, ChevronRightIcon, Icon } from '@chakra-ui/icons';
import { Box, Link, Stack, Text } from '@chakra-ui/layout';
import { Popover, PopoverContent, PopoverTrigger } from '@chakra-ui/popover';
import { NavItem, NAV_ITEMS, SECRET_NAV_ITEMS } from '.';
import NextLink from 'next/link';
import { FC } from 'react';
import { NavText, RightArrow } from './styles';
import api from '@/api/services';
import { useSelector } from 'react-redux';
import { getAccount } from '@/redux/slices/auth/selector';

const LinkWrapper: FC<{ href?: string }> = ({ href, children }) => {
  if (!href) {
    return (
      <PopoverTrigger>
        <Box cursor="pointer">{children}</Box>
      </PopoverTrigger>
    );
  }
  return (
    <PopoverTrigger>
      <div>
        <NextLink href={href} passHref>
          {children}
        </NextLink>
      </div>
    </PopoverTrigger>
  );
};

export const DesktopNav = () => {
  const account = useSelector(getAccount);

  return (
    <Stack direction={'row'} spacing={'30px'}>
      {NAV_ITEMS.map((navItem) => {
        if (!account && SECRET_NAV_ITEMS.includes(navItem.label)) {
          return null;
        }
        return (
          <Box key={navItem.label}>
            <Popover trigger={'hover'} placement={'bottom-end'}>
              <LinkWrapper href={navItem.href}>
                <NavText data-testid={`desktop-nav-${navItem.testId}`}>
                  {navItem.label}
                  {!navItem.href && <Icon color={'white'} ml="3px" w={5} h={5} as={ChevronDownIcon} />}
                </NavText>
              </LinkWrapper>

              {navItem.children && (
                <PopoverContent w="fit-content" border={0} boxShadow={'xl'} p={2} rounded={'md'}>
                  <Stack>
                    {navItem.children.map((child) => (
                      <DesktopSubNav key={child.label} {...child} />
                    ))}
                  </Stack>
                </PopoverContent>
              )}
            </Popover>
          </Box>
        );
      })}
    </Stack>
  );
};

const DesktopSubNav = ({ label, href, subLabel, testId }: NavItem) => {
  const { redirectToSSOLogout } = api.sso;
  const { logoutService } = api.auth;
  const logoutAction = () => {
    logoutService();
    redirectToSSOLogout();
  };
  const SubNavWrapper: FC = ({ children }) => {
    if (label !== 'Logout') {
      return (
        <NextLink href={href as string} passHref>
          {children}
        </NextLink>
      );
    }
    return <div>{children}</div>;
  };

  return (
    <SubNavWrapper>
      <Link
        role={'group'}
        display={'block'}
        p={2}
        rounded={'md'}
        _hover={{ bg: 'blue.50' }}
        onClick={label === 'Logout' ? logoutAction : undefined}
      >
        <Stack direction={'row'} align={'center'}>
          <Box data-testid={`desktop-nav-${testId}`}>
            <Text variant="medium" color="gray.800" _groupHover={{ color: 'blue.500' }} fontWeight={500}>
              {label}
            </Text>
            <Text variant="small" color="gray.500">
              {subLabel}
            </Text>
          </Box>
          <RightArrow>
            <Icon color={'blue.600'} w={5} h={5} as={ChevronRightIcon} />
          </RightArrow>
        </Stack>
      </Link>
    </SubNavWrapper>
  );
};
