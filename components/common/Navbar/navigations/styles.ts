import { ChevronDownIcon, Icon } from '@chakra-ui/icons';
import { Flex, Link, Text } from '@chakra-ui/layout';
import styled from '@emotion/styled';

export const NavText = styled(Text)();
NavText.defaultProps = {
  as: 'a',
  color: 'white',
  _hover: {
    color: 'gray.200',
  },
  variant: 'medium',
};

export const MobileNavText = styled(Flex)();
MobileNavText.defaultProps = {
  py: 2,
  as: Link,
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
  _hover: {
    textDecoration: 'none',
  },
};

export const RightArrow = styled(Flex)();
RightArrow.defaultProps = {
  transition: 'all .3s ease',
  transform: 'translateX(-10px)',
  opacity: 0,
  _groupHover: { opacity: '100%', transform: 'translateX(0)' },
  justify: 'flex-end',
  align: 'center',
  flex: 1,
};

export const DownArrow = styled(Icon)();
DownArrow.defaultProps = {
  as: ChevronDownIcon,
  transition: 'all .25s ease-in-out',
  w: 6,
  h: 6,
};
