import { fireEvent, screen } from '@testing-library/react';
import { renderWithTheme } from '@/utils/test-utils/wrapper';
import { MobileNav } from '../Mobile';
import makeStore from '@/redux/store';
import { setAccount } from '@/redux/slices/auth';
import { dummyAcc } from './Desktop.test';

describe('Authenticated Mobile Navbar', () => {
  const store = makeStore();
  beforeEach(() => {
    store.dispatch(setAccount(dummyAcc));
    renderWithTheme(<MobileNav />, { store });
  });

  it('renders a Daftar Dosen navitem', () => {
    const dosen = screen.getByTestId('mobile-nav-daftar-dosen');
    expect(dosen).toBeVisible;
  });

  it('renders a kegiatan navitem', () => {
    const kegiatan = screen.getByTestId('mobile-nav-kegiatan');
    expect(kegiatan).toBeVisible;
  });

  it('renders a produk navitem', () => {
    const produk = screen.getByTestId('mobile-nav-produk');
    expect(produk).toBeVisible;
  });

  it('renders a akun navitem', () => {
    const akun = screen.getByTestId('mobile-nav-akun');
    expect(akun).toBeVisible;
  });

  it('logout btn should work', () => {
    const LogoutBtn = screen.getByTestId('mobile-nav-logout');

    fireEvent.click(LogoutBtn);
    expect(localStorage.getItem('token')).toEqual(null);
  });
});

describe('Public Mobile Navbar', () => {
  beforeEach(() => {
    renderWithTheme(<MobileNav />);
  });

  it('not renders a Daftar Dosen navitem', () => {
    const dosen = screen.queryByTestId('mobile-nav-daftar-dosen');
    expect(dosen).toEqual(null);
  });

  it('not renders a kegiatan navitem', () => {
    const kegiatan = screen.queryByTestId('mobile-nav-kegiatan');
    expect(kegiatan).toEqual(null);
  });

  it('not renders a produk navitem', () => {
    const produk = screen.queryByTestId('mobile-nav-produk');
    expect(produk).toEqual(null);
  });

  it('not renders a akun navitem', () => {
    const akun = screen.queryByTestId('mobile-nav-akun');
    expect(akun).toEqual(null);
  });
});
