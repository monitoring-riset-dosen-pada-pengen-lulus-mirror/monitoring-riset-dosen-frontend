import { fireEvent, screen } from '@testing-library/react';
import { renderWithTheme } from '@/utils/test-utils/wrapper';

import { DesktopNav } from '../Desktop';
import makeStore from '@/redux/store';
import { setAccount } from '@/redux/slices/auth';

export const dummyAcc: LoginDataType = {
  account: {
    id: 1,
    nama_lengkap: 'User 1',
    user: 'user1',
    email: 'user1@ui.ac.id',
    npm: '1234567890',
  },
  token: 'asdasdd',
  refresh: 'asdasdd',
  role: 'mahasiswa',
  is_admin: false,
};

describe('Authenticated Desktop Navbar', () => {
  beforeEach(() => {
    const store = makeStore();
    store.dispatch(setAccount(dummyAcc));
    renderWithTheme(<DesktopNav />, { store });
  });

  it('renders a Daftar Dosen navitem', () => {
    const dosen = screen.getByTestId('desktop-nav-daftar-dosen');
    expect(dosen).toBeVisible;
  });

  it('renders a kegiatan navitem', () => {
    const kegiatan = screen.getByTestId('desktop-nav-kegiatan');
    expect(kegiatan).toBeVisible;
  });

  it('renders a produk navitem', () => {
    const produk = screen.getByTestId('desktop-nav-produk');
    expect(produk).toBeVisible;
  });

  it('renders a akun navitem', () => {
    const akun = screen.getByTestId('desktop-nav-akun');
    expect(akun).toBeVisible;
  });

  it('logout btn should work', () => {
    const LogoutBtn = screen.getByTestId('desktop-nav-logout');
    fireEvent.click(LogoutBtn);
    expect(localStorage.getItem('token')).toEqual(null);
  });
});

describe('Public Desktop Navbar', () => {
  beforeEach(() => {
    renderWithTheme(<DesktopNav />);
  });

  it('not renders a Daftar Dosen navitem', () => {
    const dosen = screen.queryByTestId('desktop-nav-daftar-dosen');
    expect(dosen).toEqual(null);
  });

  it('not renders a kegiatan navitem', () => {
    const kegiatan = screen.queryByTestId('desktop-nav-kegiatan');
    expect(kegiatan).toEqual(null);
  });

  it('not renders a produk navitem', () => {
    const produk = screen.queryByTestId('desktop-nav-produk');
    expect(produk).toEqual(null);
  });

  it('not renders a akun navitem', () => {
    const akun = screen.queryByTestId('desktop-nav-akun');
    expect(akun).toEqual(null);
  });
});
