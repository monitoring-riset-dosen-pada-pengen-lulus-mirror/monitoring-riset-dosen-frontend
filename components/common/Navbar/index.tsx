import { IconButton } from '@chakra-ui/button';
import { CloseIcon, HamburgerIcon } from '@chakra-ui/icons';
import { Box, Flex, Heading } from '@chakra-ui/layout';
import { useDisclosure } from '@chakra-ui/react';

import { BackDrop, NavbarContainer } from './styles';
import BigBanner from './BigBanner';
import { DesktopNav, MobileNav } from './navigations';

const Navbar = () => {
  const { isOpen, onToggle } = useDisclosure();
  const ToggleIcon = isOpen ? <CloseIcon w={3} h={3} /> : <HamburgerIcon w={5} h={5} />;

  return (
    <>
      <BigBanner />
      <NavbarContainer>
        <Flex flex={{ base: 1 }} justify={{ md: 'space-between' }}>
          <Heading variant="h6">SIHIBAH</Heading>
          <Flex display={{ base: 'none', md: 'flex' }}>
            <DesktopNav />
          </Flex>
        </Flex>
        <Flex justify="end" flex={{ base: 1, md: 'auto' }} display={{ base: 'flex', md: 'none' }}>
          <IconButton
            colorScheme="whiteAlpha"
            color="white"
            onClick={onToggle}
            icon={ToggleIcon}
            variant="ghost"
            aria-label="Toggle Navigation"
          />
        </Flex>
      </NavbarContainer>

      <Box position="sticky" bg="white" top="60px" zIndex="sticky">
        {isOpen && (
          <>
            <MobileNav />
            <BackDrop onClick={onToggle} />
          </>
        )}
      </Box>
    </>
  );
};

export default Navbar;
