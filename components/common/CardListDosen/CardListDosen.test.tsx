import { render, screen } from '@testing-library/react';
import CardListDosen from '@/components/common/CardListDosen/index';

describe('Card List Dosen', () => {
  const data: IDosen = {
    uuid: '123-321-321-3221',
    nama_lengkap: 'Testing Nama Lengkap',
    jumlah_hibah: '10',
    jumlah_hki: '20',
    jumlah_paten: '30',
    jumlah_riset: '4',
    jumlah_pengmas: '2',
  };

  function testIfAllValueRendered(data: IDosen) {
    expect(screen.getByText(data.nama_lengkap)).toBeVisible;
    expect(screen.getByText(data.jumlah_hibah)).toBeVisible;
    expect(screen.getByText(data.jumlah_hki)).toBeVisible;
    expect(screen.getByText(data.jumlah_riset)).toBeVisible;
    expect(screen.getByText(data.jumlah_pengmas)).toBeVisible;
    expect(screen.getByText(data.jumlah_paten)).toBeVisible;
  }

  it('renders card list dosen ' + 'with all values exist in API' + 'and can see all those values', () => {
    render(<CardListDosen data={data} />);
    testIfAllValueRendered(data);
  });

  it(
    'renders card list dosen ' + 'with penelitan value not exist' + 'and can not see penelitian value',
    () => {
      data.jumlah_pengmas = '0';
      render(<CardListDosen data={data} />);
      expect(screen.queryByText(data.jumlah_pengmas)).not.toBeVisible;
    },
  );
});
