import { Flex, Tag, TagLabel, Text, Tooltip } from '@chakra-ui/react';
import { ProductCardContainer } from '@/components/common/Product/styles';
import { FC } from 'react';

export interface CardListDosenProps {
  onClick?: () => void;
  data: IDosen;
}

export interface CardListDosenTagsProps {
  color: string;
  tagLabel: string;
  amount: number;
}

const CardListDosenTag: FC<CardListDosenTagsProps> = (props) => {
  const { color, tagLabel, amount } = props;
  return (
    <Tag size="md" colorScheme={color}>
      <TagLabel>
        <span style={{ fontWeight: '600' }}>{amount}</span> {tagLabel}
      </TagLabel>
    </Tag>
  );
};

const CardListDosen: FC<CardListDosenProps> = (props) => {
  const {
    data: { nama_lengkap, jumlah_hibah, jumlah_hki, jumlah_paten, jumlah_riset, jumlah_pengmas },
    ...rest
  } = props;
  const tagData = [
    { color: 'gray', amount: Number(jumlah_riset), tagLabel: 'Riset' },
    { color: 'green', amount: Number(jumlah_pengmas), tagLabel: 'Pengmas' },
    { color: 'blue', amount: Number(jumlah_hibah), tagLabel: 'Hibah' },
    { color: 'yellow', amount: Number(jumlah_hki), tagLabel: 'HKI' },
    { color: 'orange', amount: Number(jumlah_paten), tagLabel: 'Paten' },
  ];
  return (
    <ProductCardContainer {...rest}>
      <Tooltip label={nama_lengkap} placement="bottom" openDelay={500} gutter={0}>
        <Text variant="2x-large" fontWeight="bold" color="blue.500" mb="2px" noOfLines={1} w="fit-content">
          {nama_lengkap}
        </Text>
      </Tooltip>
      <Flex color="gray.500" gap="8px" mb="10px" align="center">
        {tagData.map((item) => {
          if (item.amount === 0) {
            return '';
          } else {
            return (
              <CardListDosenTag
                color={item.color}
                tagLabel={item.tagLabel}
                amount={Number(item.amount)}
                key={item.tagLabel}
              />
            );
          }
        })}
      </Flex>
    </ProductCardContainer>
  );
};

export default CardListDosen;
