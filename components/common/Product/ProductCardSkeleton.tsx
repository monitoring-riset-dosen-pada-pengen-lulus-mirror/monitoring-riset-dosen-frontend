import { Flex, FlexProps, Skeleton } from '@chakra-ui/react';
import { range } from 'lodash';
import { FC } from 'react';

const ProductCardSkeleton: FC<FlexProps> = (props) => (
  <Flex direction="column" gap={4} mb={8} {...props}>
    {range(5).map((item) => (
      <Skeleton borderRadius="8px" key={item} h="130px" />
    ))}
  </Flex>
);
export default ProductCardSkeleton;
