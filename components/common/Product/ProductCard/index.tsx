import { CalendarIcon } from '@chakra-ui/icons';
import { Box, Flex, Tag, TagLabel, TagLeftIcon, Text, Tooltip } from '@chakra-ui/react';
import { FC } from 'react';
import { FiTag } from 'react-icons/fi';
import { MdPeopleAlt } from 'react-icons/md';
import { ProductCardContainer } from '../styles';

export interface ProductCardProps {
  onClick?: () => void;
  data: IProduk;
}

const mapKeteranganToColor: Record<string, string> = {
  'Belum Terdaftar': 'red',
  Tercatat: 'orange',
};

const ProductCard: FC<ProductCardProps> = (props) => {
  const {
    data: { jenis, judul_ciptaan, keterangan, pencipta, tahun },
    ...rest
  } = props;
  return (
    <ProductCardContainer {...rest}>
      <Tooltip label={judul_ciptaan} placement="bottom" openDelay={500} gutter={0}>
        <Text variant="2x-large" fontWeight="bold" color="blue.500" mb="2px" noOfLines={1} w="fit-content">
          {judul_ciptaan}
        </Text>
      </Tooltip>
      <Flex color="gray.500" gap="8px" mb="10px" align="center">
        <Tag size="sm">
          <TagLeftIcon as={CalendarIcon} />
          <TagLabel>{tahun}</TagLabel>
        </Tag>
        {jenis && (
          <Tag size="sm" colorScheme="blue">
            <TagLeftIcon as={FiTag} />
            <TagLabel>{jenis}</TagLabel>
          </Tag>
        )}
        {keterangan && (
          <Tag size="sm" colorScheme={mapKeteranganToColor[keterangan] ?? 'green'} fontWeight="semibold">
            <TagLabel>{keterangan}</TagLabel>
          </Tag>
        )}
      </Flex>
      <Flex color="gray.500" align="center" gap="4px">
        <Box>
          <MdPeopleAlt />
        </Box>
        <Text variant="medium" noOfLines={1}>
          {pencipta.toString().replaceAll(',', ', ')}
        </Text>
      </Flex>
    </ProductCardContainer>
  );
};

export default ProductCard;
