import { Box, BoxProps, Flex } from '@chakra-ui/react';
import styled from '@emotion/styled';

export const ProductCardContainer = styled(Box)``;
ProductCardContainer.defaultProps = {
  borderRadius: '8px',
  border: '1px solid',
  borderColor: 'gray.100',
  py: '15px',
  px: '24px',
  cursor: 'pointer',
  _hover: { bg: 'gray.50' },
};

export const FilterBoxContainer = styled(Flex)``;
FilterBoxContainer.defaultProps = {
  direction: 'column',
  gap: '12px',
  maxW: '400px',
  borderRadius: '10px',
  border: '2px solid',
  borderColor: 'gray.100',
  p: '28px',
  as: 'form',
  h: 'fit-content',
};

export const listStyleProps: BoxProps = {
  maxH: '150px',
  overflowY: 'auto',
  pos: 'absolute',
  top: '100%',
  w: '100%',
  zIndex: 1500,
  mt: '-16px',
};

export const tagStyleProps = {
  rounded: 'full',
  mx: '0 !important',
  m: '2px !important',
};

export const labelStyleProps = { fontSize: '14px', fontWeight: 'bold', m: 0 };
