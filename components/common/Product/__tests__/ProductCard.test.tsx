import { renderWithTheme } from '@/utils/test-utils/wrapper';
import { fireEvent, screen } from '@testing-library/react';
import { act } from 'react-test-renderer';
import { renderHook } from '@testing-library/react-hooks';
import Router, { useRouter } from 'next/router';
import ProductCard from '../ProductCard';

jest.mock('next/dist/client/router', () => require('next-router-mock'));
describe('HKI Card', () => {
  const hki = {
    id: '1',
    jenis: 'paten',
    judul_ciptaan: 'judul 1',
    keterangan: 'keterangan',
    pencipta: ['andi'],
    tahun: '2000',
  };
  beforeEach(() => {
    renderWithTheme(
      <ProductCard
        data={hki}
        onClick={() => Router.push(`/produk/hki/${hki.id}`)}
        data-testid="hki-card"
      />,
    );
  });

  it('should redirect to detail HKI page', async () => {
    const { result } = renderHook(() => useRouter());
    const Card = screen.getByTestId('hki-card');

    act(() => {
      fireEvent.click(Card);
    });

    expect(result.current).toMatchObject({ asPath: '/produk/hki/1' });
  });
});
