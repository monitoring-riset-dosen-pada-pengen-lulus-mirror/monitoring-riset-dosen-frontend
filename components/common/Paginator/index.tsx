import { FC } from 'react';
import ReactPaginate from 'react-paginate';

const Paginator: FC<{ page: number; onPageChange: any; pageCount: number }> = ({
  page,
  onPageChange,
  pageCount,
}) => {
  const paginateConfig = {
    forcePage: page - 1,
    nextLabel: 'Selanjutnya',
    onPageChange,
    pageRangeDisplayed: 5,
    marginPagesDisplayed: 3,
    pageCount,
    previousLabel: 'Sebelumnya',
    pageClassName: 'page-item',
    pageLinkClassName: 'page-link',
    previousClassName: 'page-item',
    previousLinkClassName: 'page-link',
    nextClassName: 'page-item',
    nextLinkClassName: 'page-link',
    breakLabel: '...',
    breakClassName: 'page-item',
    breakLinkClassName: 'page-link',
    containerClassName: 'pagination',
    activeClassName: 'active',
    renderOnZeroPageCount: () => null,
  };

  return (
    <span data-testid="react-paginator">
      <ReactPaginate {...paginateConfig} />
    </span>
  );
};

export default Paginator;
