/* eslint-disable react/display-name */

import { WarningToast } from '@/components/Toast';
import { useRouter } from 'next/router';

const withAuth = <T,>(WrappedComponent: React.ComponentType<T>) => {
  return (props: T) => {
    const router = useRouter();

    if (typeof window !== 'undefined') {
      const accessToken = localStorage.getItem('token');

      if (!accessToken) {
        WarningToast({ title: 'Mohon login melalui SSO' });
        localStorage.clear();
        router.replace('/');

        return null;
      }

      return <WrappedComponent {...props} />;
    }

    return null;
  };
};

export default withAuth;
