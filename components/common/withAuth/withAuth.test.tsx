//

import { screen } from '@testing-library/react';
import { renderWithTheme } from '@/utils/test-utils/wrapper';
import { Box } from '@chakra-ui/react';
import withAuth from '.';
import { FC } from 'react';

jest.mock('next/dist/client/router', () => require('next-router-mock'));

const MockComponent: FC = () => {
  return <Box>pass withAuth</Box>;
};

describe('Paten Page', () => {
  beforeEach(() => {
    // to fully reset the state between tests, clear the storage
    localStorage.clear();
    // and reset all mocks
    jest.clearAllMocks();
  });

  it('should show warning toast', async () => {
    const ComponentInside = withAuth(MockComponent);
    renderWithTheme(<ComponentInside />);

    const warningMessage = screen.getByText('Mohon login melalui SSO');

    expect(warningMessage).toBeVisible;
  });

  it('should pass to wrapped component', async () => {
    localStorage.setItem('token', 'dummytoken');

    const ComponentInside = withAuth(MockComponent);
    renderWithTheme(<ComponentInside />);

    const warningMessage = screen.getByText('Mohon login melalui SSO');
    const passText = screen.getByText('pass withAuth');

    expect(warningMessage).not.toBeVisible;
    expect(passText).not.toBeVisible;
  });
});
