import { Box } from '@chakra-ui/layout';
import { FC } from 'react';
import Footer from '../Footer';
import Navbar from '../Navbar';

export interface TicketPayload {
  ticket: string | string[] | undefined;
  service_url: string;
}

const Layout: FC = (props) => {
  const { children } = props;

  return (
    <>
      <Navbar />
      <Box minH="100vh" py={{ base: '80px', md: '120px' }} px={{ base: 4, md: '96px' }}>
        {children}
      </Box>
      <Footer />
    </>
  );
};

export default Layout;
