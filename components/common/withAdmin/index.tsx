/* eslint-disable react/display-name */

import { WarningToast } from '@/components/Toast';
import { useRouter } from 'next/router';

const withAdmin = <T,>(WrappedComponent: React.ComponentType<T>) => {
  return (props: T) => {
    const router = useRouter();

    if (typeof window !== 'undefined') {
      const isAdmin = localStorage.getItem('is_admin');

      if (isAdmin !== 'true') {
        WarningToast({ title: 'Anda tidak memiliki akses ke halaman ini' });
        localStorage.clear();
        router.replace('/');

        return null;
      }

      return <WrappedComponent {...props} />;
    }

    return null;
  };
};

export default withAdmin;
