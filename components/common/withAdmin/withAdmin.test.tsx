//

import { screen } from '@testing-library/react';
import { renderWithTheme } from '@/utils/test-utils/wrapper';
import { Box } from '@chakra-ui/react';
import withAdmin from '.';
import { FC } from 'react';

jest.mock('next/dist/client/router', () => require('next-router-mock'));

const MockComponent: FC = () => {
  return <Box>pass withAdmin</Box>;
};

describe('Paten Page', () => {
  beforeEach(() => {
    // to fully reset the state between tests, clear the storage
    localStorage.clear();
    // and reset all mocks
    jest.clearAllMocks();
  });

  it('should show warning toast', async () => {
    const ComponentInside = withAdmin(MockComponent);
    renderWithTheme(<ComponentInside />);

    const warningMessage = screen.getByText('Anda tidak memiliki akses ke halaman ini');

    expect(warningMessage).toBeVisible;
  });

  it('should pass to wrapped component', async () => {
    localStorage.setItem('is_admin', 'true');

    const ComponentInside = withAdmin(MockComponent);
    renderWithTheme(<ComponentInside />);

    const warningMessage = screen.getByText('Anda tidak memiliki akses ke halaman ini');
    const passText = screen.getByText('pass withAdmin');

    expect(warningMessage).not.toBeVisible;
    expect(passText).not.toBeVisible;
  });
});
