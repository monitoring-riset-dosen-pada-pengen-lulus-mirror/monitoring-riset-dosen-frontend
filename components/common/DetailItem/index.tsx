import { Box, Text, VStack } from '@chakra-ui/react';

const DetailItem = ({ title, data }: any) => {
  return (
    <VStack align="stretch" spacing="0">
      <Text fontSize="sm" fontWeight="bold">
        {title}
      </Text>
      <Box fontSize="md">{data}</Box>
    </VStack>
  );
};

export default DetailItem;
