import { Box, Flex, Heading, Text } from '@chakra-ui/layout';
import Link from 'next/link';
import Copyright from './Copyright';
import { MdEmail, MdLocationOn } from 'react-icons/md';
import { Icon } from '@chakra-ui/icons';
import { Contacts, FooterContainer } from './styles';

const LINKS = [
  { name: 'Beranda', href: '/' },
  { name: 'Daftar Pengarang', href: '/dosen' },
  { name: 'Daftar Riset', href: '/riset' },
  { name: 'Daftar Pengabdian Masyarakat', href: '/pengabdian-masyarakat' },
  { name: 'Informasi Hibah', href: '/hibah' },
  { name: 'FAQ', href: '/faq' },
];

const Footer = () => {
  return (
    <>
      <FooterContainer>
        <Box w={{ base: 'full', md: '500px' }} textAlign={{ base: 'center', md: 'left' }}>
          <Heading variant="h6">Hibah dan Pengabdian Masyarakat</Heading>
          <Text color="#717171" variant="large">
            Fakultas Ilmu Komputer Universitas Indonesia
          </Text>
          <Text variant="medium" fontWeight={500} mt="18px" color="#717171">
            {LINKS.map((item, idx) => (
              <span key={item.name}>
                <Link href={item.href}>
                  <a>{item.name}</a>
                </Link>
                {idx !== LINKS.length - 1 && <Box as="span"> - </Box>}
              </span>
            ))}
          </Text>
        </Box>
        <Contacts>
          <Flex gap="12px" align="center" maxW="300px">
            <Icon boxSize={{ base: '20px', md: '24px' }} as={MdLocationOn} />
            <Text align={{ base: 'center', md: 'start' }} variant="medium">
              Faculty of Computer Science, University of Indonesia
            </Text>
          </Flex>
          <Flex gap="12px" align="center" justify={{ base: 'center', md: 'start' }}>
            <Icon boxSize={{ base: '20px', md: '24px' }} as={MdEmail} />
            <Text variant="medium">sihibah@cs.ui.ac.id</Text>
          </Flex>
        </Contacts>
      </FooterContainer>
      <Copyright />
    </>
  );
};

export default Footer;
