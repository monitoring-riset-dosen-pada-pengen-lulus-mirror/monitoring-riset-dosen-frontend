import { Flex } from '@chakra-ui/layout';
import styled from '@emotion/styled';

export const FooterContainer = styled(Flex)();
FooterContainer.defaultProps = {
  flexDir: { base: 'column', md: 'row' },
  bg: '#F5F5F5',
  justify: 'space-between',
  py: '30px',
  px: { base: '16px', md: '96px' },
  align: { base: 'start', md: 'center' },
  gap: '20px',
};

export const Contacts = styled(Flex)();
Contacts.defaultProps = {
  w: 'fit-content',
  m: { base: 'auto', md: '0 0 0 auto' },
  justify: 'center',
  gap: '16px',
  flexDir: 'column',
};
