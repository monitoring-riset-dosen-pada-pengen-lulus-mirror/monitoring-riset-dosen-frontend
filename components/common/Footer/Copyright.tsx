import { Box, Text } from '@chakra-ui/layout';

const Copyright = () => {
  const year = new Date().getFullYear();
  return (
    <Box p="16px" borderTop="10px solid" borderColor="red.500" bg="blue.700">
      <Text fontWeight={400} variant="small" color="white" align="center">
        Copyright © {year} | Fakultas Ilmu Komputer Universitas Indonesia
      </Text>
    </Box>
  );
};

export default Copyright;
