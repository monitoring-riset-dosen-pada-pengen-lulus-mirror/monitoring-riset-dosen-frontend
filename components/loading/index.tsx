import { Flex, Spinner } from '@chakra-ui/react';
import styled from '@emotion/styled';

const SLoadingScreen = styled(Flex)``;
SLoadingScreen.defaultProps = {
  top: '0',
  w: '100vw',
  h: '100vh',
  justifyContent: 'center',
  alignItems: 'center',
  position: 'fixed',
  flexDir: 'column',
  backgroundColor: 'white',
};

const LoadingScreen = () => (
  <SLoadingScreen>
    <img src="/navbar/logo-pacil.jpg" aria-label="logo pacil" alt="logo pacil" />
    <Spinner
      data-testid="spinner"
      marginTop="3rem"
      thickness="2px"
      speed="0.65s"
      emptyColor="blue.50"
      color="blue.500"
      size="xl"
    />
  </SLoadingScreen>
);

export default LoadingScreen;
