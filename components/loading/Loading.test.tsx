import { renderWithTheme } from '@/utils/test-utils/wrapper';
import { screen } from '@testing-library/react';
import LoadingScreen from '.';

describe('Loading screen', () => {
  beforeEach(() => {
    renderWithTheme(<LoadingScreen />);
  });

  it('renders Univ. Indonesia & Fasilkom logo image', () => {
    const logoUI = screen.getByRole('img', { name: 'logo pacil' });
    expect(logoUI).toBeVisible;
  });

  it('renders a spinner', () => {
    const spinner = screen.getByTestId('spinner');
    expect(spinner).toBeVisible;
  });
});
