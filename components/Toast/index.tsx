import { createStandaloneToast, UseToastOptions } from '@chakra-ui/react';
import theme from '@/theme';

const toast = createStandaloneToast({
  defaultOptions: { position: 'top', duration: 5000, isClosable: true },
  theme,
});

type ToastOptions = Pick<UseToastOptions, 'description'> & Record<'title', string>;

export const SuccessToast = (options: ToastOptions) =>
  !toast.isActive(options.title) &&
  toast({
    id: options.title,
    status: 'success',
    ...options,
  });

export const ErrorToast = (options: ToastOptions) =>
  !toast.isActive(options.title) &&
  toast({
    id: options.title,
    status: 'error',
    ...options,
  });

export const WarningToast = (options: ToastOptions) =>
  !toast.isActive(options.title) &&
  toast({
    id: options.title,
    status: 'warning',
    ...options,
  });
