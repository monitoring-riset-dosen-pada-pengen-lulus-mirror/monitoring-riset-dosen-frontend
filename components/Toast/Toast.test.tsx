import { screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { ErrorToast, SuccessToast } from '.';

describe('Toast', () => {
  it('SuccessToast is shown', () => {
    SuccessToast({ title: 'test success', description: 'desc success' });
    const title = screen.getByText('test success');
    const desc = screen.getByText('desc success');
    expect(title).toBeVisible;
    expect(desc).toBeVisible;
  });
  it('ErrorToast is shown', () => {
    ErrorToast({ title: 'test error', description: 'desc error' });
    const title = screen.getByText('test error');
    const desc = screen.getByText('desc error');
    expect(title).toBeVisible;
    expect(desc).toBeVisible;
  });
});
