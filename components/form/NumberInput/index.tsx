import {
  FormControl,
  FormErrorMessage,
  FormHelperText,
  FormLabel,
  NumberInput as ChakraNumberInput,
  NumberInputField,
} from '@chakra-ui/react';
import { FC } from 'react';
import { Controller } from 'react-hook-form';
import { INumberInput } from '../form';

export const NumberInput: FC<INumberInput> = (props) => {
  const {
    errors,
    control,
    id,
    title,
    placeholder = '',
    rules = {},
    variant = 'outline',
    helperText,
    ...rest
  } = props;
  return (
    <FormControl isInvalid={errors?.[id]} isRequired={!!rules.required}>
      <FormLabel fontWeight="normal" htmlFor={id} color="#6F7482">
        {title}
      </FormLabel>
      <Controller
        name={id}
        control={control}
        rules={rules}
        render={({ field: { ref, ...render } }) => (
          <ChakraNumberInput variant={variant} id={id} placeholder={placeholder} {...render} {...rest}>
            <NumberInputField ref={ref} name={id} />
          </ChakraNumberInput>
        )}
      />
      <FormHelperText>{helperText}</FormHelperText>
      <FormErrorMessage role="alert">{errors?.[id] && errors?.[id].message}</FormErrorMessage>
    </FormControl>
  );
};
