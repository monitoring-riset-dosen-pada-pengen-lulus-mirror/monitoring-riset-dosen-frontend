import { renderWithTheme } from '@/utils/test-utils/wrapper';
import { configure, screen } from '@testing-library/react';
import { useForm } from 'react-hook-form';
import { NumberInput } from '.';

describe('Number Input Component', () => {
  beforeEach(() => {
    const NumberInputComponent = () => {
      const { control } = useForm();
      return <NumberInput control={control} id="test_input" title="test Number Input" />;
    };
    renderWithTheme(<NumberInputComponent />);
    configure({
      throwSuggestions: true,
    });
  });

  it('should render Number Input with title "test Number Input"', () => {
    screen.getByText(/test Number Input/i, { selector: 'label' });
  });
});
