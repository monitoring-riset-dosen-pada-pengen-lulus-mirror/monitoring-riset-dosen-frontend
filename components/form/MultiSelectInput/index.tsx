import resolve from '@/utils/api/resolve';
import { FormControl, FormErrorMessage, FormHelperText, FormLabel } from '@chakra-ui/react';
import { Select, GroupBase, AsyncSelect } from 'chakra-react-select';
import { Controller } from 'react-hook-form';
import AsyncSelectLoadingComponent from '../AsyncSelectLoadingIndicator';
import { IAsyncMultiSelectInput, IMultiSelectInput, ISelectOption } from '../form';

export const MultiSelectInput = <T,>(props: IMultiSelectInput<T>) => {
  const { errors, options, control, id, title, placeholder = '', rules = {}, helperText } = props;
  return (
    <Controller
      control={control}
      name={id}
      rules={rules}
      render={({ field: { onChange, onBlur, value, name, ref } }) => (
        <FormControl isInvalid={errors?.[id]} isRequired={!!rules.required}>
          <FormLabel fontWeight="normal" htmlFor={id} color="#6F7482">
            {title}
          </FormLabel>
          <Select<T, true, GroupBase<T>>
            isMulti
            hasStickyGroupHeaders
            name={name}
            ref={ref}
            onChange={onChange}
            onBlur={onBlur}
            value={value}
            options={options}
            placeholder={placeholder}
            closeMenuOnSelect={false}
          />
          <FormHelperText>{helperText}</FormHelperText>
          <FormErrorMessage role="alert">{errors?.[id] && errors?.[id].message}</FormErrorMessage>
        </FormControl>
      )}
    />
  );
};

export const AsyncMultiSelectInput = <T extends ISelectOption>(props: IAsyncMultiSelectInput<T>) => {
  const { errors, getListRequest, control, id, title, placeholder = '', rules = {}, helperText } = props;
  return (
    <Controller
      control={control}
      name={id}
      rules={rules}
      render={({ field: { onChange, onBlur, value, name, ref } }) => (
        <FormControl isInvalid={errors?.[id]} isRequired={!!rules.required}>
          <FormLabel fontWeight="normal" htmlFor={id} color="#6F7482">
            {title}
          </FormLabel>
          <AsyncSelect<T, true, GroupBase<T>>
            isMulti
            hasStickyGroupHeaders
            name={name}
            ref={ref}
            onChange={onChange}
            onBlur={onBlur}
            value={value}
            placeholder={placeholder}
            components={AsyncSelectLoadingComponent}
            loadOptions={(inputValue, callback) => {
              setTimeout(async () => {
                const values = await resolve(getListRequest(inputValue));
                callback(values.result);
              }, 100);
            }}
          />
          <FormHelperText>{helperText}</FormHelperText>
          <FormErrorMessage role="alert">{errors?.[id] && errors?.[id].message}</FormErrorMessage>
        </FormControl>
      )}
    />
  );
};
