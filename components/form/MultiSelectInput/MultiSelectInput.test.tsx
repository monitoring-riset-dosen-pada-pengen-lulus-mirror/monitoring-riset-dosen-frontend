import { renderWithTheme } from '@/utils/test-utils/wrapper';
import { configure, screen } from '@testing-library/react';
import { useForm } from 'react-hook-form';
import { MultiSelectInput } from '.';
import { ISelectOption } from '../form';

describe('Multi Select Input Component', () => {
  beforeEach(() => {
    const Component = () => {
      const { control } = useForm({ defaultValues: { test_multi_select_input: '' } });
      return (
        <MultiSelectInput<ISelectOption>
          control={control}
          id="test_multi_select_input"
          options={[
            { label: 'option 1', value: 'option 1' },
            { label: 'option 2', value: 'option 2' },
            { label: 'option 3', value: 'option 3' },
          ]}
          title="test Multi Select Input"
        />
      );
    };
    renderWithTheme(<Component />);
    configure({
      throwSuggestions: true,
    });
  });

  it('should render Multi Select Input with title "test Multi Select Input"', () => {
    screen.getByText('test Multi Select Input', { selector: 'label' });
  });
});
