import { renderWithTheme } from '@/utils/test-utils/wrapper';
import { configure, screen } from '@testing-library/react';
import { useForm } from 'react-hook-form';
import { SearchableSelectInput } from '.';
import { ISelectOption } from '../form';

describe('Searchable Select Input Component', () => {
  beforeEach(() => {
    const Component = () => {
      const { control } = useForm({ defaultValues: { test_searchable_select_input: '' } });
      return (
        <SearchableSelectInput<ISelectOption>
          control={control}
          id="test_searchable_select_input"
          options={[
            { label: 'option 1', value: 'option 1' },
            { label: 'option 2', value: 'option 2' },
            { label: 'option 3', value: 'option 3' },
          ]}
          title="test Searchable Select Input"
        />
      );
    };
    renderWithTheme(<Component />);
    configure({
      throwSuggestions: true,
    });
  });

  it('should render Searchable Select Input with title "test Searchable Select Input"', () => {
    screen.getByText('test Searchable Select Input', { selector: 'label' });
  });
});
