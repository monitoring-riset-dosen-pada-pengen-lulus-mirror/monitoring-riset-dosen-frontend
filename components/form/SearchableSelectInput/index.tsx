import resolve from '@/utils/api/resolve';
import { FormControl, FormErrorMessage, FormHelperText, FormLabel } from '@chakra-ui/react';
import { Select, GroupBase, AsyncSelect } from 'chakra-react-select';
import { Controller } from 'react-hook-form';
import AsyncSelectLoadingComponent from '../AsyncSelectLoadingIndicator';
import { IAsyncSearchableSelectInput, ISearchableSelectInput, ISelectOption } from '../form';

export const SearchableSelectInput = <T,>(props: ISearchableSelectInput<T>) => {
  const { errors, options, control, id, title, placeholder = '', rules = {}, helperText } = props;
  return (
    <Controller
      control={control}
      name={id}
      rules={rules}
      render={({ field: { onChange, onBlur, value, name, ref } }) => (
        <FormControl isInvalid={errors?.[id]} isRequired={!!rules.required}>
          <FormLabel fontWeight="normal" htmlFor={id} color="#6F7482">
            {title}
          </FormLabel>
          <Select<T, false, GroupBase<T>>
            isClearable
            hasStickyGroupHeaders
            backspaceRemovesValue
            closeMenuOnSelect
            name={name}
            ref={ref}
            onChange={onChange}
            onBlur={onBlur}
            value={value}
            options={options}
            placeholder={placeholder}
          />
          <FormHelperText>{helperText}</FormHelperText>
          <FormErrorMessage role="alert">{errors?.[id] && errors?.[id].message}</FormErrorMessage>
        </FormControl>
      )}
    />
  );
};

export const AsyncSearchableSelectInput = <T extends ISelectOption>(
  props: IAsyncSearchableSelectInput<T>,
) => {
  const { errors, getListRequest, control, id, title, placeholder = '', rules = {}, helperText } = props;
  return (
    <Controller
      control={control}
      name={id}
      rules={rules}
      render={({ field: { onChange, onBlur, value, name, ref } }) => (
        <FormControl isInvalid={errors?.[id]} isRequired={!!rules.required}>
          <FormLabel fontWeight="normal" htmlFor={id} color="#6F7482">
            {title}
          </FormLabel>
          <AsyncSelect<T, false, GroupBase<T>>
            isClearable
            hasStickyGroupHeaders
            backspaceRemovesValue
            closeMenuOnSelect
            name={name}
            ref={ref}
            onChange={onChange}
            onBlur={onBlur}
            value={value}
            placeholder={placeholder}
            components={AsyncSelectLoadingComponent}
            loadOptions={(inputValue, callback) => {
              setTimeout(async () => {
                const values = await resolve(getListRequest(inputValue));
                callback(values.result);
              }, 100);
            }}
          />
          <FormHelperText>{helperText}</FormHelperText>
          <FormErrorMessage role="alert">{errors?.[id] && errors?.[id].message}</FormErrorMessage>
        </FormControl>
      )}
    />
  );
};
