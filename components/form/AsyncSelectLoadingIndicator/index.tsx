import { useColorModeValue } from '@chakra-ui/react';
import { chakraComponents, GroupBase, LoadingIndicatorProps } from 'chakra-react-select';
import { ISelectOption } from '../form';

const AsyncSelectLoadingComponent = {
  LoadingIndicator: <T extends ISelectOption, U extends boolean>(
    props: LoadingIndicatorProps<T, U, GroupBase<T>>,
  ) => {
    const { color, emptyColor } = useColorModeValue(
      {
        color: 'blue.500',
        emptyColor: 'blue.100',
      },
      {
        color: 'blue.300',
        emptyColor: 'blue.900',
      },
    );

    return (
      <chakraComponents.LoadingIndicator
        color={color}
        emptyColor={emptyColor}
        speed="750ms"
        spinnerSize="md"
        thickness="3px"
        {...props}
      />
    );
  },
};

export default AsyncSelectLoadingComponent;
