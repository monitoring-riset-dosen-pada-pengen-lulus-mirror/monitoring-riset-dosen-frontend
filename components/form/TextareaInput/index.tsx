import { FormControl, FormErrorMessage, FormHelperText, FormLabel, Textarea } from '@chakra-ui/react';
import { FC } from 'react';
import { IInput } from '../form';

export const TextareaInput: FC<IInput> = (props) => {
  const {
    errors,
    register,
    id,
    title,
    placeholder = '',
    rules = {},
    variant = 'outline',
    helperText,
    ...rest
  } = props;
  return (
    <FormControl isInvalid={errors?.[id]} isRequired={!!rules.required}>
      <FormLabel fontWeight="normal" htmlFor={id} color="#6F7482">
        {title}
      </FormLabel>
      <Textarea variant={variant} id={id} placeholder={placeholder} {...register?.(id, rules)} {...rest} />
      <FormHelperText>{helperText}</FormHelperText>
      <FormErrorMessage role="alert">{errors?.[id] && errors?.[id].message}</FormErrorMessage>
    </FormControl>
  );
};
