import { renderWithTheme } from '@/utils/test-utils/wrapper';
import { configure, screen } from '@testing-library/react';
import { TextareaInput } from '.';

describe('Textarea Input Component', () => {
  beforeEach(() => {
    renderWithTheme(<TextareaInput id="test_input" title="test Textarea" />);
    configure({
      throwSuggestions: true,
    });
  });

  it('should render Textarea Input with title "test Textarea"', () => {
    screen.getByText(/test Textarea/i, { selector: 'label' });
  });
});
