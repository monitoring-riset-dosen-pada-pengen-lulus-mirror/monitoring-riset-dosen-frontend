import { renderWithTheme } from '@/utils/test-utils/wrapper';
import { configure, screen } from '@testing-library/react';
import { useForm } from 'react-hook-form';
import { RadioInput } from '.';

describe('Radio Input Component', () => {
  beforeEach(() => {
    const Component = () => {
      const { control } = useForm({ defaultValues: { test_radio: '' } });
      return (
        <RadioInput
          control={control}
          id="test_radio"
          options={[
            { name: 'option 1', value: 'option 1' },
            { name: 'option 2', value: 'option 2' },
            { name: 'option 3', value: 'option 3' },
          ]}
          title="test radio"
        />
      );
    };
    renderWithTheme(<Component />);
    configure({
      throwSuggestions: true,
    });
  });

  it('should render Radio Input with title "test radio"', () => {
    screen.getByText('test radio', { selector: 'label' });
  });

  it('should render Radio Input with options', () => {
    screen.getByRole('radio', { name: /option 1/i });
    screen.getByRole('radio', { name: /option 2/i });
    screen.getByRole('radio', { name: /option 3/i });
  });

  it('should has value of selected radio', () => {
    const option1 = screen.getByRole('radio', { name: /option 1/i });

    expect(option1).not.toBeChecked();
    option1.setAttribute('checked', 'true');
    expect(option1).toHaveAttribute('checked', 'true');
  });
});
