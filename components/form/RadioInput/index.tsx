import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  Radio,
  RadioGroup,
  Stack,
  StackDirection,
} from '@chakra-ui/react';
import { FC } from 'react';
import { Control, RegisterOptions, useController } from 'react-hook-form';

interface RadioController {
  control: Control<any, any>;
  id: string;
  options: { name: string; value: string | number }[];
  direction?: StackDirection;
  title?: string;
  rules?: RegisterOptions;
  defaultValue?: string | number;
}

export const RadioInput: FC<RadioController> = (props) => {
  const { control, id, rules = {}, title, options, direction = 'row', defaultValue } = props;

  const {
    field,
    formState: { errors },
  } = useController({ control, name: id, rules });

  return (
    <FormControl isInvalid={errors?.[id]} isRequired={!!rules.required}>
      <FormLabel fontWeight="normal" htmlFor={id} color="#6F7482">
        {title}
      </FormLabel>
      <RadioGroup onChange={field.onChange} value={field.value} name={id} defaultValue={defaultValue}>
        <Stack direction={direction}>
          {options.map((item, idx) => (
            <Radio key={idx} value={item.value}>
              {item.name}
            </Radio>
          ))}
        </Stack>
      </RadioGroup>
      <FormErrorMessage role="alert">{errors?.[id] && errors[id].message}</FormErrorMessage>
    </FormControl>
  );
};
