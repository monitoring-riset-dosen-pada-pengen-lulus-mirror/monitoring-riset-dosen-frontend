import { InputProps, StackDirection } from '@chakra-ui/react';
import { FieldErrors, RegisterOptions, UseFormRegister } from 'react-hook-form';

interface IInput {
  register?: UseFormRegister<T>;
  errors?: FieldErrors;
  placeholder?: string;
  id: string;
  title?: string;
  rules?: RegisterOptions;
  variant?: InputProps['variant'];
  helperText?: string;
}

interface INumberInput extends IInput {
  control?: Control<FieldValues, any>;
}

interface IMultipleInput extends IInput {
  options: { name: string; value: string }[];
  direction?: StackDirection;
}

interface IMultiSelectInput<T> extends IInput {
  options?: T[];
  control?: Control<FieldValues, any>;
}

interface IAsyncMultiSelectInput<T> extends IMultiSelectInput<T> {
  getListRequest: (query: string) => Promise<AxiosResponse<any, any>>;
}

interface ISearchableSelectInput<T> extends IInput {
  options?: T[];
  control?: Control<FieldValues, any>;
}

interface IAsyncSearchableSelectInput<T> extends ISearchableSelectInput<T> {
  getListRequest: (query: string) => Promise<AxiosResponse<any, any>>;
}

interface ISelectOption extends OptionBase {
  label: string | number;
  value: string | number;
}
