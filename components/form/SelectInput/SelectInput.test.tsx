import { renderWithTheme } from '@/utils/test-utils/wrapper';
import { configure, screen } from '@testing-library/react';
import { SelectInput } from '.';

describe('Select Input Component', () => {
  beforeEach(() => {
    renderWithTheme(
      <SelectInput
        id="test_Select"
        options={[
          { name: 'option 1', value: 'option 1' },
          { name: 'option 2', value: 'option 2' },
          { name: 'option 3', value: 'option 3' },
        ]}
        title="test Select"
      />,
    );
    configure({
      throwSuggestions: true,
    });
    // screen.debug();
  });

  it('should render Select Input with title "test Select"', () => {
    screen.getByText(/test Select/i, { selector: 'label' });
  });

  it('should render Select Input with options', () => {
    screen.getByRole('option', { name: /option 1/i });
    screen.getByRole('option', { name: /option 2/i });
    screen.getByRole('option', { name: /option 3/i });
  });

  it('should has value of selected Select', () => {
    const SelectInput = screen.getByRole('combobox', { name: /test Select/i });

    expect(SelectInput).not.toHaveAttribute('selected');
    SelectInput.setAttribute('selected', 'option 1');
    expect(SelectInput).toHaveAttribute('selected', 'option 1');
  });
});
