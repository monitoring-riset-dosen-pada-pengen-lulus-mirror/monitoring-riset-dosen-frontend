import { FormControl, FormErrorMessage, FormHelperText, FormLabel, Select } from '@chakra-ui/react';
import { FC } from 'react';
import { IMultipleInput } from '../form';

export const SelectInput: FC<IMultipleInput> = (props) => {
  const {
    errors,
    options,
    register,
    id,
    title,
    placeholder = '',
    rules = {},
    variant = 'filled',
    helperText,
  } = props;
  return (
    <FormControl isInvalid={errors?.[id]} isRequired={!!rules.required}>
      <FormLabel fontWeight="normal" htmlFor={id} color="#6F7482">
        {title}
      </FormLabel>
      <Select {...register?.(id, rules)} variant={variant} id={id} placeholder={placeholder}>
        {options.map((item) => (
          <option key={item.name} value={item.value}>
            {item.name}
          </option>
        ))}
      </Select>
      <FormHelperText>{helperText}</FormHelperText>
      <FormErrorMessage role="alert">{errors?.[id] && errors?.[id].message}</FormErrorMessage>
    </FormControl>
  );
};
