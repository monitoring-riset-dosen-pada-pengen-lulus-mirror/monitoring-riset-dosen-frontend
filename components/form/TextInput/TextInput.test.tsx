import { renderWithTheme } from '@/utils/test-utils/wrapper';
import { configure, screen } from '@testing-library/react';
import { TextInput } from '.';

describe('Text Input Component', () => {
  beforeEach(() => {
    renderWithTheme(<TextInput id="test_input" title="test Text" />);
    configure({
      throwSuggestions: true,
    });
  });

  it('should render Text Input with title "test Text"', () => {
    screen.getByText(/test Text/i, { selector: 'label' });
  });
});
