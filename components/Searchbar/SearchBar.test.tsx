import { renderWithTheme } from '@/utils/test-utils/wrapper';
import { fireEvent, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import Searchbar from '.';

describe('Search Bar', () => {
  beforeEach(() => {
    renderWithTheme(<Searchbar placeholder="_placeholder" width="40%" />);
  });

  it('is a search bar', () => {
    const bar = screen.getByTestId('searchbar');
    expect(bar).toBeVisible;
  });

  it('has a submit button', () => {
    const button = screen.getByRole('button');
    expect(button).toBeVisible;
  });

  it('it can submit', () => {
    const button = screen.getByRole('button');
    expect(button).toHaveAttribute('type', expect.stringContaining('submit'));
  });

  it('display the correct placeholder as inputed', () => {
    const bar = screen.getByTestId('searchbar');
    expect(bar).toHaveAttribute('placeholder', expect.stringContaining('_placeholder'));
  });

  it('submit an input to a window alert and expect the window alert to be called with the input', () => {
    jest.spyOn(window, 'alert').mockImplementation(() => undefined);
    const placeholderText = 'Hibah apa yang anda cari?';
    const { getByPlaceholderText } = render(
      <Searchbar placeholder={placeholderText} width="full" onSubmit={window.alert} />,
    );
    const output = 'TESTING';
    fireEvent.change(getByPlaceholderText(placeholderText), { target: { value: output } });
    fireEvent.submit(getByPlaceholderText(placeholderText));
    expect(window.alert).toHaveBeenCalledWith(output);
  });
});
