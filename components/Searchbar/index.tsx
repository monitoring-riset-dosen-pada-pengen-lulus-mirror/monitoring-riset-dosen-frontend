import { HStack, Input } from '@chakra-ui/react';
import { FC, useState } from 'react';
import { GrayButton } from '../button';

interface Props {
  placeholder: string;
  width: string;
  onSubmit?: (query: string) => void;
  onChange?: (query: string) => void;
}

export const Searchbar: FC<Props> = ({ placeholder, width, onSubmit, onChange }) => {
  const [formInput, setFormInputs] = useState({
    searchTerm: '',
  });

  const handleInputs = (event: any) => {
    const { name, value } = event.target;
    setFormInputs({ ...formInput, [name]: value });
    onChange?.(value);
  };

  const search = async (event: any) => {
    event.preventDefault();

    onSubmit?.(formInput.searchTerm);
  };

  return (
    <form onSubmit={search}>
      <HStack width={width}>
        <Input
          data-testid="searchbar"
          placeholder={placeholder}
          name="searchTerm"
          value={formInput.searchTerm}
          onChange={handleInputs}
          type="text"
        />
        <GrayButton type="submit">Cari</GrayButton>
      </HStack>
    </form>
  );
};

export default Searchbar;
