import { useEffect, useState } from 'react';
import resolve from '@/utils/api/resolve';
import { ErrorToast } from '@/components/Toast';
import { AxiosResponse } from 'axios';

export interface UseFetchListHook<T> {
  result: ListResponse<T>;
  isLoading: boolean;
  isError: boolean;
}

const initialResult = { count: 0, next: null, previous: null, results: [] };

type UseFetchListType = <T>(
  getListRequest: () => Promise<AxiosResponse<any, any>>,
  deps?: any[],
) => UseFetchListHook<T>;

const useFetchList: UseFetchListType = <T,>(
  getListRequest: () => Promise<AxiosResponse<any, any>>,
  deps: any[] = [],
) => {
  const [response, setResponse] = useState<UseFetchListHook<T>>({
    result: initialResult,
    isLoading: false,
    isError: false,
  });

  const fetchList = async (): Promise<void> => {
    setResponse((prev) => ({ ...prev, isLoading: true }));
    const { result, error } = await resolve(getListRequest());
    if (!error) {
      setResponse({ isError: false, isLoading: false, result });
    } else {
      ErrorToast({ title: 'terdapat kesalahan' });
      setResponse({ isError: true, isLoading: false, result: initialResult });
    }
  };

  useEffect(() => {
    if (!deps.includes(undefined)) {
      fetchList();
    }
  }, deps);

  return response;
};

export default useFetchList;
