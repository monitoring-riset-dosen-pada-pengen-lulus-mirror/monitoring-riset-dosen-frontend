import { useEffect, useState } from 'react';
import resolve from '@/utils/api/resolve';
import { ErrorToast } from '@/components/Toast';
import services from '@/api/services';
import { Item } from 'chakra-ui-autocomplete';

export interface IUseListYear {
  model: ProductType | KegiatanAPIType;
  asItem?: boolean;
}

const useListYears = ({ model, asItem }: IUseListYear) => {
  const [response, setResponse] = useState<{
    result: (number | Item)[];
    isLoading: boolean;
    isError: boolean;
  }>({
    result: [],
    isLoading: false,
    isError: false,
  });

  const processList = (list: number[]) => {
    if (asItem) {
      return list.map((item) => ({ label: `${item}`, value: `${item}` })) as Item[];
    }
    return list;
  };

  const fetchYears = async (): Promise<void> => {
    setResponse((prev) => ({ ...prev, isLoading: true }));
    const { result, error } = await resolve(services[model].getYears());
    if (!error) {
      const resultList = processList(result);
      setResponse({ isError: false, isLoading: false, result: resultList });
    } else {
      ErrorToast({ title: 'terdapat kesalahan' });
      setResponse({ isError: true, isLoading: false, result: [] });
    }
  };

  useEffect(() => {
    fetchYears();
  }, []);

  return response;
};

export default useListYears;
