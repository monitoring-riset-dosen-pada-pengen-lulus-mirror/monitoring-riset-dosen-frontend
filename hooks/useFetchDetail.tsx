import { useEffect, useState } from 'react';
import resolve from '@/utils/api/resolve';
import { ErrorToast } from '@/components/Toast';
import { AxiosResponse } from 'axios';

export interface UseFetchDetailHook<T> {
  result: T | Record<string, unknown>;
  isLoading: boolean;
  isError: boolean;
}

type UseFetchDetailType = <T>(
  getDetailRequest: () => Promise<AxiosResponse<any, any>>,
  id: string,
) => UseFetchDetailHook<T>;

const useFetchDetail: UseFetchDetailType = <T,>(
  getDetailRequest: () => Promise<AxiosResponse<any, any>>,
  id: string,
) => {
  const [response, setResponse] = useState<UseFetchDetailHook<T>>({
    result: {},
    isLoading: true,
    isError: false,
  });

  const fetchDetail = async (): Promise<void> => {
    setResponse((prev) => ({ ...prev, isLoading: true }));
    const { result, error } = await resolve(getDetailRequest());
    if (!error) {
      setResponse({ isError: false, isLoading: false, result });
    } else {
      ErrorToast({ title: 'terdapat kesalahan' });
      setResponse({ isError: true, isLoading: false, result: {} });
    }
  };

  useEffect(() => {
    if (id) {
      fetchDetail();
    }
  }, [id]);

  return response;
};

export default useFetchDetail;
