import services from '@/api/services';
import { getHKIDetailError } from '@/mocks/handlers/hki';
import { server } from '@/mocks/servers';
import { sleep } from '@/utils/test-utils/wrapper';
import { renderHook, RenderResult } from '@testing-library/react-hooks';
import { act } from 'react-test-renderer';
import useFetchDetail, { UseFetchDetailHook } from '../useFetchDetail';
const { hki } = services;

describe('useFetchDetail', () => {
  const loadingFetchDetail = (result: RenderResult<UseFetchDetailHook<IHKI>>) => {
    expect(result.current.isLoading).toEqual(true);
    expect(result.current.result).toEqual({});
    expect(result.current.isError).toEqual(false);
  };

  const successFetchDetail = (result: RenderResult<UseFetchDetailHook<IHKI>>) => {
    expect(result.current.isLoading).toEqual(false);
    expect(result.current.result).not.toEqual({});
    expect(result.current.isError).toEqual(false);
  };

  const failedFetchDetail = (result: RenderResult<UseFetchDetailHook<IHKI>>) => {
    expect(result.current.isLoading).toEqual(false);
    expect(result.current.result).toEqual({});
    expect(result.current.isError).toEqual(true);
  };

  it(`Success with default params`, async () => {
    const { result } = renderHook(() => useFetchDetail<IHKI>(hki.getById('1'), '1'));

    loadingFetchDetail(result);

    await act(() => sleep(200));

    successFetchDetail(result);
  });

  it(`Failed fetch detail HKI`, async () => {
    server.use(getHKIDetailError);
    const { result } = renderHook(() => useFetchDetail<IHKI>(hki.getById('1'), '1'));

    loadingFetchDetail(result);

    await act(() => sleep(200));

    failedFetchDetail(result);
  });
});
