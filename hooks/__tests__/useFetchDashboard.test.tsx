import services from '@/api/services';
import { server } from '@/mocks/servers';
import { sleep } from '@/utils/test-utils/wrapper';
import { renderHook, RenderResult } from '@testing-library/react-hooks';
import { act } from 'react-test-renderer';
import useFetchDashboard, { UseFetchDashboard } from '@/hooks/useFetchDashboard';
import { getDashboardError } from '@/mocks/handlers/dashboard';

const { dashboard } = services;

describe('useFetchDashboard', () => {
  const loadingFetchDashboard = (result: RenderResult<UseFetchDashboard>) => {
    expect(result.current.isLoading).toEqual(true);
    expect(result.current.result).toEqual({
      jumlah_hki: null,
      jumlah_paten: null,
      pendanaan_hibah: null,
      pendanaan_pengmas: null,
      pendanaan_riset: null,
    });
    expect(result.current.isError).toEqual(false);
  };

  const successFetchDashboard = (result: RenderResult<UseFetchDashboard>) => {
    expect(result.current.isLoading).toEqual(false);
    expect(result.current.result).not.toEqual({
      jumlah_hki: null,
      jumlah_paten: null,
      pendanaan_hibah: null,
      pendanaan_pengmas: null,
      pendanaan_riset: null,
    });
    expect(result.current.isError).toEqual(false);
  };

  const failedFetchDashboard = (result: RenderResult<UseFetchDashboard>) => {
    expect(result.current.isLoading).toEqual(false);
    expect(result.current.result).toEqual({
      jumlah_hki: null,
      jumlah_paten: null,
      pendanaan_hibah: null,
      pendanaan_pengmas: null,
      pendanaan_riset: null,
    });
    expect(result.current.isError).toEqual(true);
  };

  it(`Success with default params`, async () => {
    const { result } = renderHook(() => useFetchDashboard(dashboard.getAll()));
    loadingFetchDashboard(result);

    await act(() => sleep(200));

    successFetchDashboard(result);
  });

  it(`Failed fetch dashboard data`, async () => {
    server.use(getDashboardError);
    const { result } = renderHook(() => useFetchDashboard(dashboard.getAll()));
    loadingFetchDashboard(result);

    loadingFetchDashboard(result);

    await act(() => sleep(200));

    failedFetchDashboard(result);
  });
});
