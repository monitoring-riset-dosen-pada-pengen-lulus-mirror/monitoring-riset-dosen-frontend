import services from '@/api/services';
import { getInfoHibahError } from '@/mocks/handlers/info-hibah';
import mockInfoHibah from '@/mocks/response/info-hibah';
import { server } from '@/mocks/servers';
import { sleep } from '@/utils/test-utils/wrapper';
import { renderHook } from '@testing-library/react-hooks';
import { act } from 'react-test-renderer';
import useFetchList from '../useFetchList';

describe('useFetchList', () => {
  const params = {
    search: '',
    ordering: '',
    page: 1,
    page_size: 10,
  };
  const { infoHibah } = services;

  describe('when fetching get list of info hibah', () => {
    it(`Success with default params`, async () => {
      const { result } = renderHook(() => useFetchList<Infohibah>(infoHibah.getAll(params)));

      expect(result.current.isLoading).toEqual(true);
      expect(result.current.result.results).toEqual([]);
      expect(result.current.isError).toEqual(false);

      await act(() => sleep(500));

      expect(result.current.isLoading).toEqual(false);
      expect(result.current.result.results).toEqual(mockInfoHibah.getAll(params).results);
      expect(result.current.isError).toEqual(false);
    });

    it(`Success with params search equals to "tes"`, async () => {
      const { result } = renderHook(() => useFetchList<Infohibah>(infoHibah.getAll({ search: 'tes' })));

      expect(result.current.isLoading).toEqual(true);
      expect(result.current.result.results).toEqual([]);
      expect(result.current.isError).toEqual(false);

      await act(() => sleep(500));

      expect(result.current.isLoading).toEqual(false);
      expect(result.current.result.results.length).toEqual(4);
      expect(result.current.isError).toEqual(false);
    });
  });

  it(`Failed fetch list Info Hibah`, async () => {
    server.use(getInfoHibahError);
    const { result } = renderHook(() => useFetchList<Infohibah>(infoHibah.getAll(params)));

    expect(result.current.isLoading).toEqual(true);
    expect(result.current.result.results).toEqual([]);
    expect(result.current.isError).toEqual(false);

    await act(() => sleep(500));

    expect(result.current.isLoading).toEqual(false);
    expect(result.current.result.results).toEqual([]);
    expect(result.current.isError).toEqual(true);
  });
});
