import { getPatenYearsError } from '@/mocks/handlers/paten';
import { server } from '@/mocks/servers';
import { sleep } from '@/utils/test-utils/wrapper';
import { renderHook, act } from '@testing-library/react-hooks';
import useListYears from '../useListYears';

describe('useListYears', () => {
  it(`Success fetch list years of Paten as list of year`, async () => {
    const { result } = renderHook(() => useListYears({ model: 'paten' }));

    expect(result.current.isLoading).toEqual(true);
    expect(result.current.result.length).toEqual(0);
    expect(result.current.isError).toEqual(false);

    await act(() => sleep(500));

    expect(result.current.isLoading).toEqual(false);
    expect(result.current.result.length).toBeGreaterThan(0);
    expect(typeof result.current.result[0]).toBe('string');
    expect(result.current.isError).toEqual(false);
  });

  it(`Failed fetch list years of Paten`, async () => {
    server.use(getPatenYearsError);
    const { result } = renderHook(() => useListYears({ model: 'paten' }));

    expect(result.current.isLoading).toEqual(true);
    expect(result.current.result).toEqual([]);
    expect(result.current.isError).toEqual(false);

    await act(() => sleep(500));

    expect(result.current.isLoading).toEqual(false);
    expect(result.current.result).toEqual([]);
    expect(result.current.isError).toEqual(true);
  });
});
