import pengmas from '@/api/services/pengmas';
import { getPengmasLabsError } from '@/mocks/handlers/pengmas';
import { server } from '@/mocks/servers';
import { sleep } from '@/utils/test-utils/wrapper';
import { renderHook, act } from '@testing-library/react-hooks';
import useGetList from '../useGetList';

describe('useGetList', () => {
  it(`Success fetch list Lab`, async () => {
    const { result } = renderHook(() => useGetList<ILab>(pengmas.getLabs()));

    expect(result.current.isLoading).toEqual(true);
    expect(result.current.result.length).toEqual(0);
    expect(result.current.isError).toEqual(false);

    await act(() => sleep(500));

    expect(result.current.isLoading).toEqual(false);
    expect(result.current.result.length).toBeGreaterThan(0);
    expect(typeof result.current.result[0]).toBe('string');
    expect(result.current.isError).toEqual(false);
  });

  it(`Failed fetch list Lab`, async () => {
    server.use(getPengmasLabsError);
    const { result } = renderHook(() => useGetList<ILab>(pengmas.getLabs()));

    expect(result.current.isLoading).toEqual(true);
    expect(result.current.result).toEqual([]);
    expect(result.current.isError).toEqual(false);

    await act(() => sleep(500));

    expect(result.current.isLoading).toEqual(false);
    expect(result.current.result).toEqual([]);
    expect(result.current.isError).toEqual(true);
  });
});
