import { AxiosResponse } from 'axios';
import { useEffect, useState } from 'react';
import resolve from '@/utils/api/resolve';
import { ErrorToast } from '@/components/Toast';

export interface UseFetchDashboard {
  result: IDashboard;
  isLoading: boolean;
  isError: boolean;
}

const initialResult: IDashboard = {
  pendanaan_hibah: null,
  pendanaan_riset: null,
  pendanaan_pengmas: null,
  jumlah_hki: null,
  jumlah_paten: null,
};

type UseFetchListTypeDashboard = (
  getListRequest: () => Promise<AxiosResponse<any, any>>,
  deps?: any[],
) => UseFetchDashboard;

const useFetchDashboard: UseFetchListTypeDashboard = (
  getListRequest: () => Promise<AxiosResponse<any, any>>,
  deps: any[] = [],
) => {
  const [response, setResponse] = useState<UseFetchDashboard>({
    result: initialResult,
    isLoading: false,
    isError: false,
  });

  const fetchList = async (): Promise<void> => {
    setResponse((prev) => ({ ...prev, isLoading: true }));
    const { result, error } = await resolve(getListRequest());
    if (!error) {
      setResponse({ isError: false, isLoading: false, result });
    } else {
      ErrorToast({ title: 'terdapat kesalahan' });
      setResponse({ isError: true, isLoading: false, result: initialResult });
    }
  };

  useEffect(() => {
    fetchList();
  }, deps);

  return response;
};

export default useFetchDashboard;
