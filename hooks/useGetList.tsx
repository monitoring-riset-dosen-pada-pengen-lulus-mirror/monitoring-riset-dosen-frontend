import { ErrorToast } from '@/components/Toast';
import resolve from '@/utils/api/resolve';
import { AxiosResponse } from 'axios';
import { useEffect, useState } from 'react';

export interface UseGetListHook<T> {
  result: T[];
  isLoading: boolean;
  isError: boolean;
}

export type useGetListType = <T>(
  getListRequest: () => Promise<AxiosResponse<any, any>>,
  deps?: any[],
) => UseGetListHook<T>;

const useGetList: useGetListType = <T,>(
  getListRequest: () => Promise<AxiosResponse<any, any>>,
  deps: any[] = [],
) => {
  const [response, setResponse] = useState<UseGetListHook<T>>({
    result: [],
    isLoading: false,
    isError: false,
  });

  const fetchList = async (): Promise<void> => {
    setResponse((prev) => ({ ...prev, isLoading: true }));
    const { result, error } = await resolve(getListRequest());
    if (!error) {
      setResponse({ isError: false, isLoading: false, result });
    } else {
      ErrorToast({ title: 'terdapat kesalahan' });
      setResponse({ isError: true, isLoading: false, result: [] });
    }
  };

  useEffect(() => {
    fetchList();
  }, deps);

  return response;
};

export default useGetList;
