# SIHIBAH - FRONTEND
## Link Penting
- link ke Linear -> [https://linear.app/ppl-c05/team/PPL/active](https://linear.app/ppl-c05/team/PPL/active)
- link ke Figma -> [https://www.figma.com/file/ghXRXHJWv9Rhg30zItr8Pw/Topik-3-Wireframe?node-id=88%3A9069](https://www.figma.com/file/ghXRXHJWv9Rhg30zItr8Pw/Topik-3-Wireframe?node-id=88%3A9069)
- link ke Staging -> [https://sihibah-fe.netlify.app/](https://sihibah-fe.netlify.app/)
- link ke Sonarqube -> https://pmpl.cs.ui.ac.id/sonarqube/dashboard?branch=staging&id=ppl-fasilkom-ui_2022_kelas-c_monitoring-riset-dosen_sihibah-fe_AX8YHMCLmTzPxwcer_l5

## Cara Menjalankan development environment
1. pastikan sudah terinstal `nodejs` dan `yarn` pada device anda
2. setelah clone projek, buka terminal dengan direktori dibawah `sihibah-fe/`
3. tulis command `yarn`, untuk install semua package
4. tulis `yarn dev`, untuk menjalankan aplikasi

## Cara Menjalankan Sonarqube di Local

1. Pastikan sudah terinstall Docker
2. Jalankan command ini
```bash
docker run -d --name sonarqube -e SONAR_ES_BOOTSTRAP_CHECKS_DISABLE=true --net host sonarqube:8.9.7-community
```
3. Buka port 9000
4. Login dan buat token
5. Jalankan command ini
```bash
docker run --net host -it -v $(pwd):/home/sonar/workspace addianto/sonar-scanner-cli:latest sonar-scanner -Dsonar.host.url=http://localhost:9000 -Dsonar.login=$GANTI_DENGAN_TOKEN_SONARQUBE
```

## Troubleshooting

1. Jika web app memiliki ketidaksesuaian atau error sesudah menjalankan `docker-compose up -d`, maka jalankan `docker-compose up -d --build` untuk mem-build ulang image yang ada.

## Information

1. Jika ingin menjalankan test, maka jalankan `docker exec -i sihibah-fe yarn test`
2. Jika ingin melihat coverage, maka jalankan `docker exec -i sihibah-fe yarn test --coverage `

## Tutorial _Deployment_

Beberapa informasi yang perlu diketahui sebelum melakukan _deployment_:

- Aplikasi ini menggunakan [Docker Container](https://www.docker.com/) untuk deployment.

Untuk melakukan _deployment_, yang perlu dilakukan hanyalah:

1. Ganti `prod` dengan nama domain dari back end SIHIBAH pada `api/config.ts`

![image](https://i.imgur.com/VRPpLqX.png)

1. Jalankan instruksi ini pada directory utama untuk mem-_build_ Docker _image_: `docker build -t <nama_image> .`
1. Lakukan _container deployment_ sesuai dengan _server provider_

**PENTING**: Ketika menjalankan

```
docker run [OPTIONS] IMAGE [COMMAND] [ARG...]
```

Maka `[COMMAND]` dan `[ARG...]` harus diisi dengan `/bin/sh -c "yarn build && yarn start"` dan salah satu `[OPTIONS]` adalah `-e NEXT_PUBLIC_NODE_ENV=prod`.

**PENTING**: Port untuk mengakses container front end SIHIBAH adalah **3000**. 



