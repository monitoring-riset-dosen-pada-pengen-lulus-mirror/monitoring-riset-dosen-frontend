import Layout from '@/components/common/Layout';
import InfoHibahPage from '@/components/container/InfoHibah';
import type { NextPage } from 'next';

const Hibah: NextPage = () => {
  return (
    <Layout>
      <InfoHibahPage />
    </Layout>
  );
};

export default Hibah;
