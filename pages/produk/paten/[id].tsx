import Layout from '@/components/common/Layout';
import DetailPaten from '@/components/container/Produk/Paten/DetailPaten';
import { Box } from '@chakra-ui/react';

const PatenDetail = () => {
  return (
    <Layout>
      <Box maxW="1200px" w="full" mx="auto">
        <DetailPaten />
      </Box>
    </Layout>
  );
};

export default PatenDetail;
