import Layout from '@/components/common/Layout';
import withAuth from '@/components/common/withAuth';
import Paten from '@/components/container/Produk/Paten';
import { Box } from '@chakra-ui/react';
import { FC } from 'react';

const PatenPage: FC = () => {
  return (
    <Layout>
      <Box maxW="1200px" w="full" mx="auto">
        <Paten />
      </Box>
    </Layout>
  );
};

export default withAuth(PatenPage);
