import Layout from '@/components/common/Layout';
import DetailHKI from '@/components/container/Produk/HKI/DetailHKI';
import { Box } from '@chakra-ui/react';

const HKIDetail = () => {
  return (
    <Layout>
      <Box maxW="1200px" w="full" mx="auto">
        <DetailHKI />
      </Box>
    </Layout>
  );
};

export default HKIDetail;
