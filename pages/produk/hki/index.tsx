import Layout from '@/components/common/Layout';
import withAuth from '@/components/common/withAuth';
import HKI from '@/components/container/Produk/HKI';
import { Box } from '@chakra-ui/react';
import { FC } from 'react';

const HKIPage: FC = () => {
  return (
    <Layout>
      <Box maxW="1200px" w="full" mx="auto">
        <HKI />
      </Box>
    </Layout>
  );
};

export default withAuth(HKIPage);
