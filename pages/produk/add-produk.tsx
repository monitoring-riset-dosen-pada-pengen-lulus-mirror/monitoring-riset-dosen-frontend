import Layout from '@/components/common/Layout';
import withAdmin from '@/components/common/withAdmin';
import AddProduk from '@/components/container/Produk/AddProduk';

const AddProdukPage = () => {
  return (
    <Layout>
      <AddProduk />
    </Layout>
  );
};

export default withAdmin(AddProdukPage);
