import Layout from '@/components/common/Layout';
import Homepage from '@/components/container/Homepage';
import type { NextPage } from 'next';

const Landing: NextPage = () => {
  return (
    <Layout>
      <Homepage />
    </Layout>
  );
};

export default Landing;
