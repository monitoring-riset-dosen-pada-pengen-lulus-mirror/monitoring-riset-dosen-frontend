import config from '@/api/config';
import React, { FC, useCallback, useEffect } from 'react';
import LoadingScreen from '@/components/loading';
import { loginThunk } from '@/redux/slices/auth/thunk';
import { useRouter } from 'next/router';
import { useDispatch } from 'react-redux';

const RedirectAuth: FC = () => {
  const router = useRouter();
  const dispatch = useDispatch();

  const {
    query: { ticket },
  } = router;

  const loginSSO = useCallback(async () => {
    const ticketPayload = {
      ticket,
      service_url: `${config.HOST_URL()}/auth/redirect`,
    };

    await dispatch(loginThunk(ticketPayload));
    router.replace('/dashboard');
  }, [dispatch, router, ticket]);

  useEffect(() => {
    if (ticket) {
      loginSSO();
    }
  }, [loginSSO, ticket]);

  return <LoadingScreen />;
};

export default RedirectAuth;
