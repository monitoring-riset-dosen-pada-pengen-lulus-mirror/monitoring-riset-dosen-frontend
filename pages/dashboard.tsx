import { NextPage } from 'next';
import Layout from '@/components/common/Layout';
import Dashboard from '@/components/container/Dashboard';
import withAuth from '@/components/common/withAuth';

const DashboadPage: NextPage = () => {
  return (
    <Layout>
      <Dashboard />
    </Layout>
  );
};

export default withAuth(DashboadPage);
