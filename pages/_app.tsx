import { wrapper } from '@/redux/store';
import { useStore } from 'react-redux';
import type { AppProps } from 'next/app';
import { ChakraProvider } from '@chakra-ui/react';
import { PersistGate } from 'redux-persist/integration/react';
import theme from '@/theme';
import '@/theme/globals.css';
import Head from 'next/head';

if (process.env.NEXT_PUBLIC_API_MOCKING === 'enabled') {
  require('../mocks');
}

function MyApp({ Component, pageProps }: AppProps) {
  const store: any = useStore();

  return (
    <>
      <Head>
        <title>Sistem Informasi Hibah &amp; Pengabdian Masyarakat</title>
      </Head>
      <ChakraProvider theme={theme}>
        <PersistGate persistor={store.__persistor} loading={<Component {...pageProps} />}>
          <Component {...pageProps} />
        </PersistGate>
      </ChakraProvider>
    </>
  );
}

export default wrapper.withRedux(MyApp);
