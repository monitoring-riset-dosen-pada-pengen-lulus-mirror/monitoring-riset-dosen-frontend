import Layout from '@/components/common/Layout';
import FAQ from '@/components/container/FAQ';
import type { NextPage } from 'next';

const FaqPage: NextPage = () => {
  return (
    <Layout>
      <FAQ />
    </Layout>
  );
};

export default FaqPage;
