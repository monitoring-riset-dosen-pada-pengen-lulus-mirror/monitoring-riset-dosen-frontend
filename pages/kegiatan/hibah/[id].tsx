import Layout from '@/components/common/Layout';
import HibahDetail from '@/components/container/Kegiatan/Hibah/HibahDetail';
import { Box } from '@chakra-ui/react';

const DetailHibah = () => {
  return (
    <Layout>
      <Box maxW="1200px" w="full" mx="auto">
        <HibahDetail />
      </Box>
    </Layout>
  );
};

export default DetailHibah;
