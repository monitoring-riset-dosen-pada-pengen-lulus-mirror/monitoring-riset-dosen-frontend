import Layout from '@/components/common/Layout';
import withAdmin from '@/components/common/withAdmin';
import HibahCreateForm from '@/components/container/Kegiatan/Hibah/HibahCreateForm';
import { Box } from '@chakra-ui/react';

const HibahCreate = () => {
  return (
    <Layout>
      <Box maxW="1200px" w="full" mx="auto">
        <HibahCreateForm />
      </Box>
    </Layout>
  );
};

export default withAdmin(HibahCreate);
