import Layout from '@/components/common/Layout';
import Hibah from '@/components/container/Kegiatan/Hibah';
import { Box } from '@chakra-ui/react';

const HibahList = () => {
  return (
    <Layout>
      <Box maxW="1200px" w="full" mx="auto">
        <Hibah />
      </Box>
    </Layout>
  );
};

export default HibahList;
