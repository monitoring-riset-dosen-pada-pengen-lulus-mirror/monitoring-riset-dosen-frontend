import Layout from '@/components/common/Layout';
import withAdmin from '@/components/common/withAdmin';
import AddKegiatan from '@/components/container/Kegiatan/AddKegiatan';

const AddKegiatanPage = () => {
  return (
    <Layout>
      <AddKegiatan />
    </Layout>
  );
};

export default withAdmin(AddKegiatanPage);
