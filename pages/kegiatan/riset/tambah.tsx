import Layout from '@/components/common/Layout';
import withAdmin from '@/components/common/withAdmin';
import RisetCreateForm from '@/components/container/Kegiatan/Riset/RisetCreateForm';
import { Box } from '@chakra-ui/react';

const RisetCreate = () => {
  return (
    <Layout>
      <Box maxW="1200px" w="full" mx="auto">
        <RisetCreateForm />
      </Box>
    </Layout>
  );
};

export default withAdmin(RisetCreate);
