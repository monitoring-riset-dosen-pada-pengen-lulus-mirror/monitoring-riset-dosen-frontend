import Layout from '@/components/common/Layout';
import RisetDetail from '@/components/container/Kegiatan/Riset/RisetDetail';
import { Box } from '@chakra-ui/react';

const DetailRiset = () => {
  return (
    <Layout>
      <Box maxW="1200px" w="full" mx="auto">
        <RisetDetail />
      </Box>
    </Layout>
  );
};

export default DetailRiset;
