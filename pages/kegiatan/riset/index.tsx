import Layout from '@/components/common/Layout';
import Riset from '@/components/container/Kegiatan/Riset';
import { Box } from '@chakra-ui/react';

const RisetList = () => {
  return (
    <Layout>
      <Box maxW="1200px" w="full" mx="auto">
        <Riset />
      </Box>
    </Layout>
  );
};

export default RisetList;
