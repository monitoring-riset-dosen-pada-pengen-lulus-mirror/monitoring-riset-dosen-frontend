import Layout from '@/components/common/Layout';
import Pengmas from '@/components/container/Kegiatan/Pengmas';
import { Box } from '@chakra-ui/react';

const PengmasList = () => {
  return (
    <Layout>
      <Box maxW="1200px" w="full" mx="auto">
        <Pengmas />
      </Box>
    </Layout>
  );
};

export default PengmasList;
