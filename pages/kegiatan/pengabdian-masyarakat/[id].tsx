import Layout from '@/components/common/Layout';
import PengmasDetail from '@/components/container/Kegiatan/Pengmas/PengmasDetail';
import { Box } from '@chakra-ui/react';

const DetailPengmas = () => {
  return (
    <Layout>
      <Box maxW="1200px" w="full" mx="auto">
        <PengmasDetail />
      </Box>
    </Layout>
  );
};

export default DetailPengmas;
