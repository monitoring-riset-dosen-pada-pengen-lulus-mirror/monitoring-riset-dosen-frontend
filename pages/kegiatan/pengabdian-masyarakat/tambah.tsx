import Layout from '@/components/common/Layout';
import withAdmin from '@/components/common/withAdmin';
import PengmasCreateForm from '@/components/container/Kegiatan/Pengmas/PengmasCreateForm';
import { Box } from '@chakra-ui/react';

const PengmasCreate = () => {
  return (
    <Layout>
      <Box maxW="1200px" w="full" mx="auto">
        <PengmasCreateForm />
      </Box>
    </Layout>
  );
};

export default withAdmin(PengmasCreate);
