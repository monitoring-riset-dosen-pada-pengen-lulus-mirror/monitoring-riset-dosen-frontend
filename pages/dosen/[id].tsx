import Layout from '@/components/common/Layout';
import withAuth from '@/components/common/withAuth';
import DetailDosen from '@/components/container/Dosen/DetailDosen';
import { Container } from '@chakra-ui/react';

const DetailDosenPage = () => {
  return (
    <Layout>
      <Container maxW="1440px" p={0}>
        <DetailDosen />
      </Container>
    </Layout>
  );
};

export default withAuth(DetailDosenPage);
