import { NextPage } from 'next';
import Layout from '@/components/common/Layout';
import Dosen from '@/components/container/Dosen';
import withAuth from '@/components/common/withAuth';

const DosenPage: NextPage = () => {
  return (
    <Layout>
      <Dosen />
    </Layout>
  );
};

export default withAuth(DosenPage);
